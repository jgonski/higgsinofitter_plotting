'''

Welcome to cuts.py

Here we have configure_cuts()
This a cut string added_cuts
and a list of cuts applied l_cuts_nMinus1

Configure the regions in dictionary d_cuts = {}
that is keyed by the region name,
whose value is the nominal list of cuts 
to be applied to this region.

By default, if CR or VR is in the name of the region
all the cuts are applied (to be safe from unblinding).
Otherwise if SR is in the name of the region,
all cuts are applied except those containing 
the inputted variable "var".

'''

#____________________________________________________________________________
def configure_cuts(var, add_cuts, sig_reg, doSRmll=False, print_cuts=True):

  # Putting cuts as lists allows easy construction of N-1
  nbaseLep2 = ['nLep_base == 2']
  nLep2 = ['nLep_signal == 2 && nLep_base == 2']
  nLep3 = ['nLep_signal == 3 && nLep_base == 3']
  # OS = opposite sign, SS = same sign
  l_OS  = ['lep1Charge != lep2Charge']
  l_SS  = ['lep1Charge == lep2Charge']

  # SF = same flavour (ee, mm), DF = different flavour (em, me), AF = all flavour (ee, mm, em, me)
  l_SF  = ['lep1Flavor == lep2Flavor']
  l_DF  = ['lep1Flavor != lep2Flavor']
 
  l_2e  = ['lep1Flavor == 1 && lep2Flavor == 1']
  l_2m  = ['lep1Flavor == 2 && lep2Flavor == 2']
  l_em  = ['lep1Flavor == 1 && lep2Flavor == 2']
  l_me  = ['lep1Flavor == 2 && lep2Flavor == 1']
  l_ee_me = ['( (lep1Flavor == 1 && lep2Flavor == 1) || (lep1Flavor == 2 && lep2Flavor == 1) )']
  l_mm_em = ['( (lep1Flavor == 2 && lep2Flavor == 2) || (lep1Flavor == 1 && lep2Flavor == 2) )']
 
  l_lep2pt = ['lep2Pt > (lep2Flavor==2 ? 3 : 4 )'] #for old SRs and VR-RJR-SS 
  l_slep2pt_mll = ['lep2Pt > min(10, 2+mll/3 )'] #for mll high met
  l_slep2pt_mt2 = ['lep2Pt > (lep2Flavor==2 ? 3 : 4 ) && lep2Pt > min(2.5*(mt2leplsp_100 - 100) + 2.5, 20.0)'] #for mt2 high met
  l_slep2pt_mll_lowMET = ['lep2Pt > (5 + 0.25*mll) ']
  l_slep2pt_mt2_lowMET = ['lep2Pt > min(15.0, 7.5 + 0.75*(mt2leplsp_100-100.0)) ']

  l_common = [
    'met_Et>120',
    'jetPt[0]>100', 
    'DPhiJ1Met > 2.0',
    'minDPhiAllJetsMet > 0.4',
    'FS != 206 && FS != 207', 
    'trigMatch_metTrig',
    'lep1Pt > 5',
    'lep2Pt > (lep2Flavor==2 ? 3 : 4 )',
    'Rll > 0.05',
    'mll > 1 && mll < 60 && (mll < 3 || mll > 3.2)',
    '(lep1Author != 16 && lep2Author != 16)',
    #'((lep1TruthMatched && lep2TruthMatched) || (DatasetNumber >= 407311 && DatasetNumber <= 407315))'
  ]  
  l_common_nolep2pt = [
    'met_Et>120',
    'jetPt[0]>100',
    'DPhiJ1Met > 2.0',
    'minDPhiAllJetsMet > 0.4',
    'FS != 206 && FS != 207', 
    'trigMatch_metTrig',
    'lep1Pt > 5',
    #'Rll > 0.05',
    '(lep1Flavor != lep2Flavor ? Rll > 0.2 : Rll > 0.05)',
    'mll > 1 && mll < 60 && (mll < 3 || mll > 3.2)',
    '(lep1Author != 16 && lep2Author != 16)',
    #'((lep1TruthMatched && lep2TruthMatched) || (DatasetNumber >= 407311 && DatasetNumber <= 407315))'
  ]  

  l_common_OS_baseLep = l_common + nbaseLep2 + l_OS
  l_common_OS = l_common + nLep2 + l_OS
  l_common_SS = l_common + nLep2 + l_SS
  l_common_RJR_OS = l_common_nolep2pt + nLep2 + l_OS
  l_common_RJR_SS = l_common_nolep2pt + nLep2 + l_SS

  # Define regions: 2018 with RISR
  l_CR_RJR_top = [
    'RJR_RISR > 0.7 && RJR_RISR < 1.0',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 >= 1',
  ]

  l_CR_RJR_tau = [
    'RJR_RISR > 0.7 && RJR_RISR < 1.0',
    '(MTauTau > 60 && MTauTau < 120)',
    'nBJet20_MV2c10 == 0',
  ]
  
  l_CR_RJR_VV = [
    'RJR_RISR > 0.7',
    'RJR_RISR < 0.85',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
  ]

  l_VR_RJR_SS = [
    'RJR_RISR > 0.7 && RJR_RISR < 1.0',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
  ]
  l_SR_RJR_MLL = [
    'nBJet20_MV2c10 == 0',
    '(MTauTau < 0 || MTauTau > 160)',
    'RJR_RISR > max(0.85,0.98-2*mll/100)',  
    'RJR_RISR < 1.0',  
    'mt_lep1<60',
  ]
  l_SR_RJR_MT2 = [
    'nBJet20_MV2c10 == 0',
    '(MTauTau < 0 || MTauTau > 160)',
    'RJR_RISR > max(0.85,0.98 - 2*( mt2leplsp_100 - 100)/100.)',  
    'RJR_RISR < 1.0',  
  ]
  # Define regions: 2018 with RISR LowMET
  l_CR_LowMET_top = [
    'RJR_RISR > 0.8 && RJR_RISR < 1.0',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 >= 1',
  ]

  l_CR_LowMET_tau = [
    'RJR_RISR > 0.6 && RJR_RISR < 1.0',
    '(MTauTau > 60 && MTauTau < 120)',
    'nBJet20_MV2c10 == 0',
    'METOverHTLep < 7.5',
  ]

  l_CR_LowMET_tau_low = [
    'RJR_RISR > 0.6 && RJR_RISR < 1.0',
    '(MTauTau > 60 && MTauTau < 120)',
    'nBJet20_MV2c10 == 0',
    'RJR_MS < 50',
    'METOverHTLep > 7.5',
  ]

  l_CR_LowMET_VV = [
    'RJR_RISR > 0.6',
    'RJR_RISR < 0.8',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
    "nJet30 < 3",
    "mt_lep1 > 30"
  ]

  l_VR_LowMET_SS = [
    'RJR_RISR > 0.8 && RJR_RISR < 1.0',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
  ]

  l_VR_LowMET_SS_low = [
    'METOverHTLep > 10',
    'RJR_MS < 50',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
  ]

  l_SR_LowMET_MLL = [
    'nBJet20_MV2c10 == 0',
    '(MTauTau < 0 || MTauTau > 160)',
    'METOverHTLep < 10',
    'RJR_RISR > 0.8 && RJR_RISR < 1.0',
    'mt_lep1<60',
    'mt_lep1>10',
  ]

  l_SR_LowMET_MLL_low = [
    'nBJet20_MV2c10 == 0',
    '(MTauTau < 0 || MTauTau > 160)',
    'METOverHTLep > 10',
    'RJR_MS < 50',
  ]

  l_SR_LowMET_MT2 = [
    'nBJet20_MV2c10 == 0',
    '(MTauTau < 0 || MTauTau > 160)',
    'RJR_RISR > 0.8 && RJR_RISR < 1.0',
  ]

  # Define regions: original 2016
  l_CR_top = [
    'METOverHTLep > 5.0',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 >= 1',
  ]

  l_CR_tau = [
    '( METOverHTLep > 4.0 ) && ( METOverHTLep < 8.0 )',
    '(MTauTau > 60 && MTauTau < 120)',
    'nBJet20_MV2c10 == 0',
  ]
  
  l_VR_VV = [
    'METOverHTLep < 3.0',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
  ]

  l_VR_SS = [
    'METOverHTLep > 5.0',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
  ]

  l_VR_SS_loose = [
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
  ]

  l_SR_MLL = [
    'METOverHTLep > max(5.0, 15 - 2*mll)',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
    'Rll < 2.0',
    'mt_lep1 < 70',
  ]

  l_SR_MT2 = [
    'METOverHTLep > max( 3.0, 15 - 2*(mt2leplsp_100 - 100) )',
    '(MTauTau < 0 || MTauTau > 160)',
    'nBJet20_MV2c10 == 0',
  ]
  
  l_eMLLaa = ['mll >= 1 && mll < 2']
  l_eMLLab = ['mll >= 2 && mll < 3']
  l_eMLLa = ['mll >= 1 && mll < 3']
  l_eMLLb = ['mll >= 3.2 && mll < 5']
  l_eMLLc = ['mll >= 5 && mll < 10']
  l_eMLLc2 = ['mll >= 1 && mll < 10']
  l_eMLLd = ['mll >= 10 && mll < 20']
  l_eMLLe = ['mll >= 20 && mll < 30']
  l_eMLLf = ['mll >= 30 && mll < 40']
  l_eMLLg = ['mll >= 40 && mll < 60']
  
  l_iMLLaa = ['mll < 2']
  l_iMLLab = ['mll < 3']
  l_iMLLa = ['mll < 3']
  l_iMLLb = ['mll < 5']
  l_iMLLc = ['mll < 10']
  l_iMLLd = ['mll < 20']
  l_iMLLe = ['mll < 30']
  l_iMLLf = ['mll < 40']
  l_iMLLg = ['mll < 60']
  
  l_iMT2a = ['mt2leplsp_100 < 100.5']
  l_iMT2b = ['mt2leplsp_100 < 101']
  l_iMT2c = ['mt2leplsp_100 < 102']
  l_iMT2d = ['mt2leplsp_100 < 105']
  l_iMT2e = ['mt2leplsp_100 < 110']
  l_iMT2f = ['mt2leplsp_100 < 120']
  l_iMT2g = ['mt2leplsp_100 < 130']
  l_iMT2h = ['mt2leplsp_100 < 140']
 
  l_common_3L = ['met_Et>150',
                 'nLep_base==3',
                 'nLep_signal==3']
  
  # =============================================

  d_cuts = {

    ####################### 
    ### Old 2016 versions
    ####################### 
    'SR3L-preselect' : l_common_3L,
    'VR3L-1ib'       : l_common_3L + ['nBJet20_MV2c10 >= 1', 'METOverHTLep > 3'],
    'SR3L-METHTLep5' : l_common_3L + ['nBJet20_MV2c10 == 0 && METOverHTLep > 5'],
    'VR3L-mZ'        : l_common_3L + ['nBJet20_MV2c10 == 0 && mSFOS_min > 70 && mSFOS_min < 110'],
    'preselect'     : ['FS != 206 && FS != 207'],
    'CR-topNoIso-AF'  : l_common_OS_baseLep +  l_CR_top,

    'CR-top-AF'     : l_common_OS +  l_CR_top,
    'CR-tau-AF'     : l_common_OS +  l_CR_tau,

    'CR-top-ee'     : l_common_OS +  l_CR_top + l_2e,
    'CR-tau-ee'     : l_common_OS +  l_CR_tau + l_2e,

    'CR-top-mm'     : l_common_OS +  l_CR_top + l_2m,
    'CR-tau-mm'     : l_common_OS +  l_CR_tau + l_2m,

    'CR-top-em'     : l_common_OS +  l_CR_top + l_em,
    'CR-tau-em'     : l_common_OS +  l_CR_tau + l_em,

    'CR-top-me'     : l_common_OS +  l_CR_top + l_me,
    'CR-tau-me'     : l_common_OS +  l_CR_tau + l_me,

    'CR-top-DF'     : l_common_OS +  l_CR_top + l_DF,
    'CR-tau-DF'     : l_common_OS +  l_CR_tau + l_DF,

    'CR-top-SF'     : l_common_OS +  l_CR_top + l_SF,
    'CR-tau-SF'     : l_common_OS +  l_CR_tau + l_SF, 
    
    'VR-top-SS-ee'  : l_common_SS +  l_CR_top + l_2e,
    'VR-top-SS-mm'  : l_common_SS +  l_CR_top + l_2m,

    'VR-top-SS-ee-me'  : l_common_SS +  l_CR_top + l_ee_me,
    'VR-top-SS-mm-em'  : l_common_SS +  l_CR_top + l_mm_em,
    
    'VR-VV-AF'      : l_common_OS +  l_VR_VV,
    'VR-VV-ee'      : l_common_OS +  l_VR_VV  + l_2e,
    'VR-VV-mm'      : l_common_OS +  l_VR_VV  + l_2m,
    'VR-VV-em'      : l_common_OS +  l_VR_VV  + l_em,
    'VR-VV-me'      : l_common_OS +  l_VR_VV  + l_me,
    'VR-VV-DF'      : l_common_OS +  l_VR_VV  + l_DF,
    'VR-VV-SF'      : l_common_OS +  l_VR_VV  + l_SF,
    
    'VR-SS-AF'      : l_common_SS + l_VR_SS,
    'VR-SS-SF'      : l_common_SS + l_VR_SS + l_SF,
    'VR-SS-ee-me'   : l_common_SS + l_VR_SS + l_ee_me,
    'VR-SS-mm-em'   : l_common_SS + l_VR_SS + l_mm_em,
    'VR-SS-me'      : l_common_SS + l_VR_SS + l_me,
    'VR-SS-em'      : l_common_SS + l_VR_SS + l_em,
    'VR-SS-ee'      : l_common_SS + l_VR_SS + l_2e,
    'VR-SS-mm'      : l_common_SS + l_VR_SS + l_2m,
    'VR-SS-AF-loose'   : l_common_SS + l_VR_SS_loose,
    'VR-SS-ee-me-loose'   : l_common_SS + l_VR_SS_loose + l_ee_me,
    'VR-SS-mm-em-loose'   : l_common_SS + l_VR_SS_loose + l_mm_em,
     
    'VRem-iMLLg-em' : l_common_OS + l_SR_MLL + l_iMLLg + l_em,
    'VRme-iMLLg-me' : l_common_OS + l_SR_MLL + l_iMLLg + l_me,
    'VRem-iMT2f-em' : l_common_OS + l_SR_MT2 + l_iMT2f + l_em,
    'VRme-iMT2f-me' : l_common_OS + l_SR_MT2 + l_iMT2f + l_me,
    'VRDF-iMT2f-DF' : l_common_OS + l_SR_MT2 + l_iMT2f + l_DF, 
    'VRDF-iMLLg-DF' : l_common_OS + l_SR_MLL + l_iMLLg + l_DF, 
     
    #Old 2016 SRs
    'SRee-iMLLa-ee' : l_common_OS + l_SR_MLL + l_iMLLa + l_2e,
    'SRmm-iMLLa-mm' : l_common_OS + l_SR_MLL + l_iMLLa + l_2m,
    'SRSF-iMLLa-SF' : l_common_OS + l_SR_MLL + l_iMLLa + l_SF,
    'SRee-eMLLa-ee' : l_common_OS + l_SR_MLL + l_eMLLa + l_2e,
    'SRmm-eMLLa-mm' : l_common_OS + l_SR_MLL + l_eMLLa + l_2m,
    
    'SRee-iMLLb-ee' : l_common_OS + l_SR_MLL + l_iMLLb + l_2e,
    'SRmm-iMLLb-mm' : l_common_OS + l_SR_MLL + l_iMLLb + l_2m,
    'SRSF-iMLLb-SF' : l_common_OS + l_SR_MLL + l_iMLLb + l_SF,
    'SRee-eMLLb-ee' : l_common_OS + l_SR_MLL + l_eMLLb + l_2e,
    'SRmm-eMLLb-mm' : l_common_OS + l_SR_MLL + l_eMLLb + l_2m,
    
    'SRee-iMLLc-ee' : l_common_OS + l_SR_MLL + l_iMLLc + l_2e,
    'SRmm-iMLLc-mm' : l_common_OS + l_SR_MLL + l_iMLLc + l_2m,
    'SRSF-iMLLc-SF' : l_common_OS + l_SR_MLL + l_iMLLc + l_SF,

    'SRee-iMLLd-ee' : l_common_OS + l_SR_MLL + l_iMLLd + l_2e, 
    'SRmm-iMLLd-mm' : l_common_OS + l_SR_MLL + l_iMLLd + l_2m,
    'SRSF-iMLLd-SF' : l_common_OS + l_SR_MLL + l_iMLLd + l_SF,
    'SRee-iMLLe-ee' : l_common_OS + l_SR_MLL + l_iMLLe + l_2e,
    'SRmm-iMLLe-mm' : l_common_OS + l_SR_MLL + l_iMLLe + l_2m,
    'SRSF-iMLLe-SF' : l_common_OS + l_SR_MLL + l_iMLLe + l_SF,
    'SRee-iMLLf-ee' : l_common_OS + l_SR_MLL + l_iMLLf + l_2e,
    'SRmm-iMLLf-mm' : l_common_OS + l_SR_MLL + l_iMLLf + l_2m,
    'SRSF-iMLLf-SF' : l_common_OS + l_SR_MLL + l_iMLLf + l_SF,
    'SRee-iMLLg-ee' : l_common_OS + l_SR_MLL + l_iMLLg + l_2e,
    'SRmm-iMLLg-mm' : l_common_OS + l_SR_MLL + l_iMLLg + l_2m,
    'SRSF-iMLLg-SF' : l_common_OS + l_SR_MLL + l_iMLLg + l_SF, 

    'SRee-iMT2a-ee' : l_common_OS + l_SR_MT2 + l_iMT2a + l_2e,
    'SRmm-iMT2a-mm' : l_common_OS + l_SR_MT2 + l_iMT2a + l_2m,
    'SRSF-iMT2a-SF' : l_common_OS + l_SR_MT2 + l_iMT2a + l_SF,
    'SRee-iMT2b-ee' : l_common_OS + l_SR_MT2 + l_iMT2b + l_2e,
    'SRmm-iMT2b-mm' : l_common_OS + l_SR_MT2 + l_iMT2b + l_2m,
    'SRSF-iMT2b-SF' : l_common_OS + l_SR_MT2 + l_iMT2b + l_SF,
    'SRee-iMT2c-ee' : l_common_OS + l_SR_MT2 + l_iMT2c + l_2e,
    'SRmm-iMT2c-mm' : l_common_OS + l_SR_MT2 + l_iMT2c + l_2m,
    'SRSF-iMT2c-SF' : l_common_OS + l_SR_MT2 + l_iMT2c + l_SF,
    'SRee-iMT2d-ee' : l_common_OS + l_SR_MT2 + l_iMT2d + l_2e,
    'SRmm-iMT2d-mm' : l_common_OS + l_SR_MT2 + l_iMT2d + l_2m,
    'SRSF-iMT2d-SF' : l_common_OS + l_SR_MT2 + l_iMT2d + l_SF,
    'SRee-iMT2e-ee' : l_common_OS + l_SR_MT2 + l_iMT2e + l_2e,
    'SRmm-iMT2e-mm' : l_common_OS + l_SR_MT2 + l_iMT2e + l_2m,
    'SRSF-iMT2e-SF' : l_common_OS + l_SR_MT2 + l_iMT2e + l_SF,
    'SRee-iMT2f-ee' : l_common_OS + l_SR_MT2 + l_iMT2f + l_2e,
    'SRmm-iMT2f-mm' : l_common_OS + l_SR_MT2 + l_iMT2f + l_2m,
    'SRSF-iMT2f-SF' : l_common_OS + l_SR_MT2 + l_iMT2f + l_SF, 
    

    ####################### 
    ### RJR versions
    ####################### 

    #by mll bins
    #CR-top
    'CR-RJRaa-top-AF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLaa,
    'CR-RJRab-top-AF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLab,
    'CR-RJRa-top-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLa,
    'CR-RJRb-top-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLd,
    'CR-RJRc-top-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLc,
    'CR-RJRc2-top-AF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLc2,
    'CR-RJRd-top-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLd,
    'CR-RJRe-top-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLe,
    'CR-RJRf-top-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLf,
    'CR-RJRg-top-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_eMLLg,
    
    #CR-tau
    'CR-RJRaa-tau-AF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLaa,
    'CR-RJRab-tau-AF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLab,
    'CR-RJRa-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLa,
    'CR-RJRb-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLd,
    'CR-RJRc-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLc,
    'CR-RJRc2-tau-AF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLc2,
    'CR-RJRd-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLd,
    'CR-RJRe-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLe,
    'CR-RJRf-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLf,
    'CR-RJRg-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_eMLLg,

    #CR-VV
    'CR-RJRaa-VV-DF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLaa + l_DF,
    'CR-RJRab-VV-DF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLab + l_DF,
    'CR-RJRa-VV-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLa + l_DF,
    'CR-RJRb-VV-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLd + l_DF,
    'CR-RJRc-VV-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLc + l_DF,
    'CR-RJRc2-VV-DF'    : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLc2 + l_DF,
    'CR-RJRd-VV-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLd + l_DF,
    'CR-RJRe-VV-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLe + l_DF,
    'CR-RJRf-VV-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLf + l_DF,
    'CR-RJRg-VV-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_eMLLg + l_DF,

    #VR-SS
    'VR-RJRaa-SS-AF'    : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLaa,
    'VR-RJRab-SS-AF'    : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLab,
    'VR-RJRa-SS-AF'     : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLa,
    'VR-RJRb-SS-AF'     : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLd,
    'VR-RJRc-SS-AF'     : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLc,
    'VR-RJRc2-SS-AF'    : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLc2,
    'VR-RJRd-SS-AF'     : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLd,
    'VR-RJRe-SS-AF'     : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLe,
    'VR-RJRf-SS-AF'     : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLf,
    'VR-RJRg-SS-AF'     : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_eMLLg,
    
    #by flavor
    #CR-top
    'CR-RJR-top-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top,
    'CR-RJR-top-ee'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_2e,
    'CR-RJR-top-mm'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_2m,
    'CR-RJR-top-em'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_em,
    'CR-RJR-top-me'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_me,
    'CR-RJR-top-ee-me'  : l_common_RJR_OS + l_slep2pt_mll +  l_CR_RJR_top + l_ee_me,
    'CR-RJR-top-mm-em'  : l_common_RJR_OS + l_slep2pt_mll +  l_CR_RJR_top + l_mm_em,
    'CR-RJR-top-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_DF,
    'CR-RJR-top-SF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_top + l_SF,
   
    #CR-tau 
    'CR-RJR-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau,
    'CR-RJR-tau-ee'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_2e,
    'CR-RJR-tau-mm'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_2m,
    'CR-RJR-tau-em'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_em,
    'CR-RJR-tau-me'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_me,
    'CR-RJR-tau-ee-me'  : l_common_RJR_OS + l_slep2pt_mll +  l_CR_RJR_tau + l_ee_me,
    'CR-RJR-tau-mm-em'  : l_common_RJR_OS + l_slep2pt_mll +  l_CR_RJR_tau + l_mm_em,
    'CR-RJR-tau-DF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_DF,
    'CR-RJR-tau-SF'     : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_tau + l_SF, 

    #CR-VV
    'CR-RJR-VV-AF'      : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV,
    'CR-RJR-VV-ee'      : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV  + l_2e,
    'CR-RJR-VV-mm'      : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV  + l_2m,
    'CR-RJR-VV-em'      : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV  + l_em,
    'CR-RJR-VV-me'      : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV  + l_me,
    'CR-RJR-VV-ee-me'   : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_ee_me,
    'CR-RJR-VV-mm-em'   : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV + l_mm_em,
    'CR-RJR-VV-DF'      : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV  + l_DF,
    'CR-RJR-VV-SF'      : l_common_RJR_OS + l_slep2pt_mll + l_CR_RJR_VV  + l_SF,
    
    #VR-SS
    'VR-RJR-SS-AF'      : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS,
    'VR-RJR-SS-ee-me'   : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_ee_me,
    'VR-RJR-SS-mm-em'   : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_mm_em,
    'VR-RJR-SS-me'      : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_me,
    'VR-RJR-SS-em'      : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_em,
    'VR-RJR-SS-ee'      : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_2e,
    'VR-RJR-SS-mm'      : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_2m,
    'VR-RJR-SS-DF'      : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_DF,
    'VR-RJR-SS-SF'      : l_common_RJR_SS + l_lep2pt + l_VR_RJR_SS + l_SF,

    #SS regions to check fake modelling
    'CR-RJR-topSS-AF'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_top,
    'CR-RJR-tauSS-AF'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_tau,
                                                    
    'CR-RJR-topSS-ee'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_top + l_2e,
    'CR-RJR-tauSS-ee'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_tau + l_2e,
                                                      
    'CR-RJR-topSS-mm'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_top + l_2m,
    'CR-RJR-tauSS-mm'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_tau + l_2m,
                                                     
    'CR-RJR-topSS-em'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_top + l_em,
    'CR-RJR-tauSS-em'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_tau + l_em,
                                                      
    'CR-RJR-topSS-me'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_top + l_me,
    'CR-RJR-tauSS-me'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_tau + l_me,
                                                      
    'CR-RJR-topSS-ee-me'   : l_common_RJR_SS +l_slep2pt_mll +  l_CR_RJR_top + l_ee_me,
    'CR-RJR-tauSS-ee-me'   : l_common_RJR_SS +l_slep2pt_mll +  l_CR_RJR_tau + l_ee_me,
                                                      
    'CR-RJR-topSS-mm-em'   : l_common_RJR_SS +l_slep2pt_mll +  l_CR_RJR_top + l_mm_em,
    'CR-RJR-tauSS-mm-em'   : l_common_RJR_SS +l_slep2pt_mll +  l_CR_RJR_tau + l_mm_em,
                                                       
    'CR-RJR-topSS-DF'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_top + l_DF,
    'CR-RJR-tauSS-DF'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_tau + l_DF,
                                                      
    'CR-RJR-topSS-SF'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_top + l_SF,
    'CR-RJR-tauSS-SF'     : l_common_RJR_SS + l_slep2pt_mll + l_CR_RJR_tau + l_SF, 

    #VRDF
    'VRDF-RJR-iMLLg-DF'   : l_common_RJR_OS + l_slep2pt_mll + l_SR_RJR_MLL + l_iMLLg + l_DF, 
    'VRDF-RJR-iMT2h-DF'   : l_common_RJR_OS + l_slep2pt_mt2 + l_SR_RJR_MT2 + l_iMT2h + l_DF, 
  
    #RJR MLL SRs
    'SRSF-RJR-MLL-SF'     : l_common_RJR_OS + l_slep2pt_mll +  l_SR_RJR_MLL + l_SF,
    'SRSF-RJR-MLL-ee'     : l_common_RJR_OS + l_slep2pt_mll +  l_SR_RJR_MLL + l_2e,
    'SRSF-RJR-MLL-mm'     : l_common_RJR_OS + l_slep2pt_mll +  l_SR_RJR_MLL + l_2m,

    'SRSF-RJRaa-MLL-SF'   : l_common_RJR_OS + l_slep2pt_mll + l_SR_RJR_MLL + l_eMLLaa + l_SF,
    'SRSF-RJRab-MLL-SF'   : l_common_RJR_OS + l_slep2pt_mll + l_SR_RJR_MLL + l_eMLLab + l_SF,
    'SRSF-RJRb-MLL-SF'    : l_common_RJR_OS +  l_slep2pt_mll +l_SR_RJR_MLL + l_eMLLb + l_SF,
    'SRSF-RJRc-MLL-SF'    : l_common_RJR_OS +  l_slep2pt_mll +l_SR_RJR_MLL + l_eMLLc + l_SF,
    'SRSF-RJRc2-MLL-SF'   : l_common_RJR_OS +  l_slep2pt_mll +l_SR_RJR_MLL + l_eMLLc + l_SF,
    'SRSF-RJRd-MLL-SF'    : l_common_RJR_OS +  l_slep2pt_mll +l_SR_RJR_MLL + l_eMLLd + l_SF,
    'SRSF-RJRe-MLL-SF'    : l_common_RJR_OS +  l_slep2pt_mll +l_SR_RJR_MLL + l_eMLLe + l_SF,
    'SRSF-RJRf-MLL-SF'    : l_common_RJR_OS +  l_slep2pt_mll +l_SR_RJR_MLL + l_eMLLf + l_SF,
    'SRSF-RJRg-MLL-SF'    : l_common_RJR_OS +  l_slep2pt_mll +l_SR_RJR_MLL + l_eMLLg + l_SF,

    #RJR MT2 SRs
    'SRSF-RJR-MT2-SF'    : l_common_RJR_OS + l_slep2pt_mt2 +  l_SR_RJR_MT2 + l_SF,
    'SRSF-RJR-MT2-ee'    : l_common_RJR_OS + l_slep2pt_mt2 +  l_SR_RJR_MT2 + l_2e,
    'SRSF-RJR-MT2-mm'    : l_common_RJR_OS + l_slep2pt_mt2 +  l_SR_RJR_MT2 + l_2m,
    
    ####################### 
    ### LowMET Regions
    #######################     

    #by flavor
    #CR-top
    'CR-LowMET-top-AF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top,
    'CR-LowMET-top-ee'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top + l_2e,
    'CR-LowMET-top-mm'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top + l_2m,
    'CR-LowMET-top-em'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top + l_em,
    'CR-LowMET-top-me'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top + l_me,
    'CR-LowMET-top-ee-me'  : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top + l_ee_me,
    'CR-LowMET-top-mm-em'  : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top + l_mm_em,
    'CR-LowMET-top-DF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top + l_DF,
    'CR-LowMET-top-SF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_top + l_SF,
   
    #CR-tau 
    'CR-LowMET-tau-AF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau,
    'CR-LowMET-tau-ee'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau + l_2e,
    'CR-LowMET-tau-mm'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau + l_2m,
    'CR-LowMET-tau-em'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau + l_em,
    'CR-LowMET-tau-me'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau + l_me,
    'CR-LowMET-tau-ee-me'  : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau + l_ee_me,
    'CR-LowMET-tau-mm-em'  : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau + l_mm_em,
    'CR-LowMET-tau-DF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau + l_DF,
    'CR-LowMET-tau-SF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau + l_SF, 

    #CR-tau-low
    'CR-LowMET-tau-low-AF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low,
    'CR-LowMET-tau-low-ee'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low + l_2e,
    'CR-LowMET-tau-low-mm'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low + l_2m,
    'CR-LowMET-tau-low-em'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low + l_em,
    'CR-LowMET-tau-low-me'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low + l_me,
    'CR-LowMET-tau-low-ee-me'  : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low + l_ee_me,
    'CR-LowMET-tau-low-mm-em'  : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low + l_mm_em,
    'CR-LowMET-tau-low-DF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low + l_DF,
    'CR-LowMET-tau-low-SF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_tau_low + l_SF,

    #CR-VV
    'CR-LowMET-VV-AF'      : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV,
    'CR-LowMET-VV-ee'      : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV  + l_2e,
    'CR-LowMET-VV-mm'      : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV  + l_2m,
    'CR-LowMET-VV-em'      : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV  + l_em,
    'CR-LowMET-VV-me'      : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV  + l_me,
    'CR-LowMET-VV-ee-me'   : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV + l_ee_me,
    'CR-LowMET-VV-mm-em'   : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV + l_mm_em,
    'CR-LowMET-VV-DF'      : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV  + l_DF,
    'CR-LowMET-VV-SF'      : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_CR_LowMET_VV  + l_SF,
    
    #VR-SS
    'VR-LowMET-SS-AF'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS,
    'VR-LowMET-SS-ee-me'   : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS + l_ee_me,
    'VR-LowMET-SS-mm-em'   : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS + l_mm_em,
    'VR-LowMET-SS-me'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS + l_me,
    'VR-LowMET-SS-em'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS + l_em,
    'VR-LowMET-SS-ee'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS + l_2e,
    'VR-LowMET-SS-mm'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS + l_2m,
    'VR-LowMET-SS-DF'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS + l_DF,
    'VR-LowMET-SS-SF'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS + l_SF,

    #VR-SS-low
    'VR-LowMET-SS-low-AF'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low,
    'VR-LowMET-SS-low-ee-me'   : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low + l_ee_me,
    'VR-LowMET-SS-low-mm-em'   : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low + l_mm_em,
    'VR-LowMET-SS-low-me'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low + l_me,
    'VR-LowMET-SS-low-em'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low + l_em,
    'VR-LowMET-SS-low-ee'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low + l_2e,
    'VR-LowMET-SS-low-mm'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low + l_2m,
    'VR-LowMET-SS-low-DF'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low + l_DF,
    'VR-LowMET-SS-low-SF'      : l_common_RJR_SS + l_lep2pt + l_VR_LowMET_SS_low + l_SF,

    #VRDF
    'VRDF-LowMET-iMLLg-DF'   : l_common_RJR_OS + l_slep2pt_mll_lowMET + l_SR_LowMET_MLL + l_iMLLg + l_DF, 
    'VRDF-LowMET-iMT2h-DF'   : l_common_RJR_OS + l_slep2pt_mt2_lowMET + l_SR_LowMET_MT2 + l_iMT2h + l_DF, 

    #EWKino SRs
    'SRSF-LowMET-MLL-SF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET +  l_SR_LowMET_MLL + l_SF,
    'SRSF-LowMET-MLL-ee'     : l_common_RJR_OS + l_slep2pt_mll_lowMET +  l_SR_LowMET_MLL + l_2e,
    'SRSF-LowMET-MLL-mm'     : l_common_RJR_OS + l_slep2pt_mll_lowMET +  l_SR_LowMET_MLL + l_2m,
    'SRSF-LowMET-MLL-low-SF'     : l_common_RJR_OS + l_slep2pt_mll_lowMET +  l_SR_LowMET_MLL_low + l_SF,
    'SRSF-LowMET-MLL-low-ee'     : l_common_RJR_OS + l_slep2pt_mll_lowMET +  l_SR_LowMET_MLL_low + l_2e,
    'SRSF-LowMET-MLL-low-mm'     : l_common_RJR_OS + l_slep2pt_mll_lowMET +  l_SR_LowMET_MLL_low + l_2m,

    #Slepton SRs
    'SRSF-LowMET-MT2-SF'    : l_common_RJR_OS + l_slep2pt_mt2_lowMET +  l_SR_LowMET_MT2 + l_SF,
    'SRSF-LowMET-MT2-ee'    : l_common_RJR_OS + l_slep2pt_mt2_lowMET +  l_SR_LowMET_MT2 + l_2e,
    'SRSF-LowMET-MT2-mm'    : l_common_RJR_OS + l_slep2pt_mt2_lowMET +  l_SR_LowMET_MT2 + l_2m,

  }
  # From cut lists, build cut string doing N-1 appropriately
  l_cuts = d_cuts[sig_reg]

  l_cuts_nMinus1 = []
  # If a control or validation region, do not do N-1, do all N cuts
  if 'VR' in sig_reg or 'CR' in sig_reg:    
      l_cuts_nMinus1 = l_cuts
  elif doSRmll:
      l_cuts_nMinus1 = l_cuts
  else:
    # if safe to do so, exclude the cut that involves var
    #l_cuts_nMinus1 = [cut for cut in l_cuts if var not in cut]
    for cut in l_cuts:
      # Make sure METOverHTLep cuts are still applied in mll and mt2leplsp_100 N-1 plots
      if ('METOverHTLep' in cut and 'mll' in cut) and ('mll' in var):
        l_cuts_nMinus1.append(cut)
        continue
      elif ('METOverHTLep' in cut and 'mt2leplsp_100' in cut) and ('mt2leplsp_100' in var):
        l_cuts_nMinus1.append(cut)
        continue
      elif ('lep2Pt' in cut and 'mll' in cut) and ('mll' in var):
        l_cuts_nMinus1.append(cut)
        continue
      elif ('lep2Pt' in cut and 'mt2leplsp_100' in cut) and ('mt2leplsp_100' in var):
        l_cuts_nMinus1.append(cut)
        continue
      elif ('RISR' in cut and 'mll' in cut) and ('mll' in var):
        l_cuts_nMinus1.append(cut)
        continue
      elif ('RISR' in cut and 'mt2leplsp_100' in cut) and ('mt2leplsp_100' in var):
        l_cuts_nMinus1.append(cut)
        continue
      elif ('mll' in cut) and ('mll' in var):
        l_cuts_nMinus1.append(cut)
        continue
      # (N-1) if variable to be plotted is in cut, do not cut on it 
      elif ('RISR' in cut and 'RISR' in var): 
	continue
      elif var in cut: continue
      l_cuts_nMinus1.append(cut) 
  l_cuts_nMinus1.append(add_cuts)
  # join cuts with && (AND) operator
  added_cuts = ' && '.join(l_cuts_nMinus1)
  #cuts = ' && '.join(l_cuts_nMinus1)
  #added_cuts = cuts + ' && ' + add_cuts
  
  if print_cuts:
    print('===============================================')
    print('Cuts applied:')
    for x in l_cuts_nMinus1:
      print x
    print('-----------------------------------------------')
    print 'Unweighted final cut-string:', added_cuts
    print('===============================================')
 
  return added_cuts, l_cuts_nMinus1

