#!/usr/bin/env python
'''

Welcome to plot.py

* This is a script to efficiently make 1-dimensional
    plots from the SusySkimHiggsino ntuples 
* For CRs and VRs, the default is to
    makes plots of data vs MC in various variables
* For SRs, the default is to
    make blinded N-1 plots without data but with Zn significance
* Configure various aspects in other files
    - cuts.py
    - samples.py
    - variables.py

'''

# So Root ignores command line inputs so we can use argparse
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(1)
from ROOT import *

import time 
import os, sys, time, argparse

from math      import sqrt
from math      import log
from random    import gauss
from array     import array
from samples   import *
from cuts      import *
from variables import *

#--------------------------------------------------
#
# Global settings
# Labels
ATLAS_label = '#bf{#it{ATLAS}} Internal'
NTUP_status = 'SusySkimHiggsino v2.7b (R21)'

useLooseNominal = False

# When lots of samples, put leg outside plot so less crowded
legend_outside_plot = True 

# Makes repeated plots sequentially adding cuts
do_cutflow = False 

# text size as percentage
text_size = 0.045
#
#--------------------------------------------------

#____________________________________________________________________________
def main():
  
  t0 = time.time()
  
  #================================================
  # default values
  var     = 'met_Et'
  var     = 'nVtx'
  var     = 'RunNumber'
  var     = 'mSFOS_min'
  var     = 'METOverHTLep'
  var     = 'lep3Pt'
  var     = 'mll'
  sig_reg = 'preselect'
  sig_reg = 'SRSF-iMLLg-SF'
  sig_reg = 'VRDF-iMT2f-DF'
  sig_reg = 'VR3L-1ib' 
  sig_reg = 'CR-top-AF'
  
  ttbarSamp='ttbar'
  unblind  = False
  cutArrow = False
  IsLogY   = True
  DoHighMet = True
  DoHardMuons = True
  DoSRmll = False
  use_mc_fakes = True
  period   = '15_16_17_18' 
  lumi     = 78.5
  
  # -------------------------------------------------------------
  # Argument parser
  parser = argparse.ArgumentParser(description='Analyse background/signal TTrees and make plots.')
  parser.add_argument('-v', '--variable',  type=str, nargs='?', help='String name for the variable (as appearing in the TTree) to make N-1 in.', default=var)
  parser.add_argument('-s', '--sigReg',    type=str, nargs='?', help='String name of selection (signal/control) region to perform N-1 selection.', default=sig_reg)
  parser.add_argument('-l', '--lumi',      type=str, nargs='?', help='Float of integrated luminosity to normalise MC to.', default=lumi)
  parser.add_argument('-t', '--ttbarSamp', type=str, nargs='?', help='ttbar sample to use.', default=ttbarSamp)
  parser.add_argument('-u', '--unblind',   type=str, nargs='?', help='Should the SRs be unblinded?')
  parser.add_argument('-p', '--period',    type=str, nargs='?', help='Period: 15_16 (2015+16 data, mc16a), 17 (2017 data, mc16d), 15_16_17 (2015 to 2017 data, mc16a+mc16d)')
  parser.add_argument('-a', '--cutArrow', action='store_true', help='Draw arrows where cuts are placed for N-1 plots.')
  parser.add_argument('-n', '--noLogY',   action='store_true', help='Do not draw log Y axis.')
  parser.add_argument('-m', '--lowMet',   action='store_true', help='All plots made with 120 < MET < 200.')
  parser.add_argument('-f', '--withFake', action='store_true', help='All plots made with fake background & truth matching.')
  parser.add_argument('-o', '--softMuons',action='store_true', help='All plots made with muon pT > 3.0.')
  parser.add_argument('-d', '--doSRmll',action='store_true', help='Make mll plot with SR mll cuts.')

  args = parser.parse_args()
  if args.variable:
    var      = args.variable
  if args.sigReg:
    sig_reg = args.sigReg
  if args.lumi:
    lumi = args.lumi
  if args.ttbarSamp:
    ttbarSamp = args.ttbarSamp
  if args.period:
    period = args.period

  # I know we could just use a bool argument here, but maybe safer to 
  # require the string, so it's harder to unblind by mistake!
  if args.unblind == 'True':
    unblind = True
  if args.cutArrow:
    cutArrow = True
  if args.noLogY:
    IsLogY = False
  if args.lowMet:
    DoHighMet = False
  if args.softMuons:
    DoHardMuons = False
  if args.withFake:
    use_mc_fakes = False
  if args.doSRmll:
    DoSRmll = True
  
  # -------------------------------------------------------------

  if use_mc_fakes:
    ttbarSamp='alt_ttbar_PowPy8_nonallhad_hdamp258p75'
  else: 
    ttbarSamp = 'ttbar' 
  
  # Convert maths characters in variables to valid file names
  save_var = var
  if '/' in var:
    save_var = var.replace('/', 'Over', 1)
  if '(' in var:
    save_var = save_var.replace('(', '', 1)
  if ')' in var:
    save_var = save_var.replace(')', '', 1)
  
  # -------------------------------------------------
  #
  # Determine labels and lumi based on period
  #
  savedir = 'figs/v2_7b/'
  lumi     = 140.452 # Normalising luminosity. FIX ME: for v2.2b we configure this in L942 [1/fb]
  if period == '15_16':
    print_lumi = 36.2 # Luminosity to print in plot [1/fb]
    add_cut  = 'RandomRunNumber < 320000 '
    savedir += '/2015_16'
    annotate_text = '2015+16 Data, mc16a'
  elif period == '17':
    print_lumi = 43.8 # [1/fb]
    add_cut  = 'RandomRunNumber > 320000'  
    savedir += '/2017'
    annotate_text = '2017 Data, mc16c/d'
  elif period == '18':
    print_lumi = 59.9 # [1/fb]
    add_cut  = 'RandomRunNumber >= 348885 && '  
    savedir += '/2018'
    annotate_text = '2018 Data, mc16e'
  elif period == '15_16_17':
    print_lumi = 80.5 # [1/fb]
    add_cut  = 'RandomRunNumber < 348885 && '  
    savedir += '/80invfb'
    annotate_text = '15+16+17 Data, mc16a+d'
  elif period == '15_16_17_18':
    print_lumi = 140.452 # [1/fb]
    add_cut  = 'RandomRunNumber > 0 && '  
    savedir += '/140invfb'
    annotate_text = '15+16+17+18 Data, mc16a+d+e'
  #
  # Do extra plot sets 
  #
  if DoHighMet: 
    add_cut += ' met_Et > 200'
  else:  
    add_cut += ' met_Et < 200'
    savedir += '/low_met'
  if DoHardMuons:
    add_cut += ' && lep2Pt > 4'
  else:  
    savedir += '/soft_muons'
  if use_mc_fakes:
    print "Using mc fakes"
  else:
    add_cut += ' && ((lep1TruthMatched && lep2TruthMatched) || (DatasetNumber >= 407311 && DatasetNumber <= 407315))'
    savedir += '/data_fakes'
  #
  # -------------------------------------------------
  
  print( '--------------------------------------------' )
  print( '\nWelcome to plot.py\n' )
  print( 'Plotting variable: {0}'.format(var) )
  print( 'Selection region: {0}'.format(sig_reg) )
  print( 'Normalising luminosity: {0}'.format(lumi) )
  print( '--------------------------------------------\n' )

  # Do a cutflow of plots
  if do_cutflow:
    savedir += '/cutflow'
    mkdir(savedir)
    
    # list of cuts to apply sequentially 
    l_cuts = [
      'met_Et>100 && nLep_base >= 2',
      'met_Et>200',
      'nBJet20_MV2c10 >= 1', # CR-top
      'nLep_base==2',
      'nLep_signal==2',
      '(lep1Author != 16 && lep2Author != 16)',
      'lep1Charge != lep2Charge',
      'lep1Pt > 5',
      'jetPt[0]>100',
      'DPhiJ1Met > 2.0',
      'minDPhiAllJetsMet > 0.4',
      'mll > 1',
      '(mll < 3 || mll > 3.2)',
      'mll < 60',
      'Rll > 0.05',
      '(MTauTau < 0 || MTauTau > 160)',
      'METOverHTLep > 5.0',
      'lep2Pt > 4',
      #'METOverHTLep > 3.0', # VR-VV
    ]
   
    l_print_cuts = []
    
    # Make a plot per addition of cut
    for count, cut in enumerate( l_cuts ):
      l_print_cuts.append(cut)
      add_cut += ' && ' + cut

      save_name = savedir + '/hist1d_{0}_{1}_cut{2}'.format(save_var, sig_reg, count)
      calc_selections(var, add_cut, lumi, save_name, sig_reg, print_lumi, annotate_text, ttbarSamp, unblind, cutArrow, IsLogY, DoSRmll, l_print_cuts)
    
  
  else:
    mkdir(savedir)  
    save_name = savedir + '/hist1d_{0}_{1}'.format(save_var, sig_reg)
    if not IsLogY: save_name += '_noLogY'
    print('Lumi to print: {0}'.format(print_lumi))
    calc_selections(var, add_cut, lumi, save_name, sig_reg, print_lumi, annotate_text, ttbarSamp, unblind, cutArrow, IsLogY, use_mc_fakes, DoSRmll)

  tfinish = time.time()
  telapse = tfinish - t0
    
  print('\n--------------------------------------------------')
  print('Finished plotting in {0:.1f}s'.format(telapse) )
  print('Have a lovely day :) ')
  print('--------------------------------------------------')

#____________________________________________________________________________
def calc_selections(var, add_cuts, lumi, save_name, sig_reg, print_lumi, annotate_text='', ttbarSamp='ttbar', unblind=False, cutArrow=False, IsLogY=True, use_mc_fakes=True, DoSRmll=False, l_print_cuts=[]):
  '''
  Extract trees given a relevant variable
  '''

  #----------------------------------------------------
  # 
  # Get various configurations from other python files
  #
  #----------------------------------------------------
  #
  # Get the sample paths from samples.py
  bkg_path, data_path, sig_path, bkg_suffix, data_suffix, fakes_suffix, sig_all_file = get_sample_paths()
  #
  # Get samples to plot from samples.py
  l_samp_bkg, l_sampOther = get_samples_to_plot(ttbarSamp)
  #
  # Get dictionary defining sample properties from samples.py
  d_samp = configure_samples()
  #
  # Get dictionary of histogram configurations from variables.py
  d_vars = configure_vars()
  # 
  # Get the cut string to apply from cuts.py
  if do_cutflow:
    normCutsAfter, l_preselect_cuts = configure_cuts(var, add_cuts, sig_reg, DoSRmll ) 
    l_cuts = l_print_cuts
  else:
    normCutsAfter, l_cuts = configure_cuts(var, add_cuts, sig_reg, DoSRmll ) 
  #
  #----------------------------------------------------

  # Blind the SRs
  if 'SR' not in sig_reg or unblind : 
    l_sampOther = ['data'] + l_sampOther
  if use_mc_fakes: l_samp = l_samp_bkg + l_sampOther
  else:
    l_samp = l_samp_bkg + ['fakes'] + l_sampOther 
    l_samp_bkg = l_samp_bkg + ['fakes']

  print('\n----------------------------------')
  print('Samples to plot:')
  for samp in l_samp: print(samp)
  print('----------------------------------\n')
  
  #----------------------------------------------------
  # 
  # Initialise objects for analysis
  #
  #----------------------------------------------------

  # obtain the number of bins with their xmin and xmax
  hNbins = d_vars[var]['hXNbins']
  hXmin  = d_vars[var]['hXmin']
  hXmax  = d_vars[var]['hXmax']

  variable_bin = False
  doSigWeight = False
  hXarray = []
  if hNbins == 'var':
    variable_bin = True
    hXarray = d_vars[var]['binsLowE']  
    hNbins = len(hXarray) - 1

  # Initialise stacked background  
  hs = THStack('','')
  hs_bkg_frac  = THStack('','') # Another stack normalised so displays fraction of processes
  hs_intgl_low = THStack('','') # lower cut integral (for significance cut)
  hs_intgl_upp = THStack('','') # upper cut integral (for significance cut)
 
  # Initialise objects to fill in loop 
  d_files = {}
  d_hists = {}
  d_yield = {}
  d_yieldErr = {}
  d_raw = {}
  nTotBkg = 0 # yield of background
  nTotBkgRaw = 0 # raw yield of background
  nVarBkg = 0 # variance of background
 
  l_bkg = []
  l_sig = []
  
  h_dat = 0
  N_dat = 0
  
  l_styles = [1, 1, 1, 1, 1, 1, 1]
  Nsignal_count = 0

  # ----------------------------------------------------------------- 
  #
  # Loop through samples, fill histograms
  #
  # ----------------------------------------------------------------- 
 
  for samp in l_samp:
    if 'SR' in sig_reg and 'data' in samp and not unblind:
      continue
    #print( 'Processing {0}'.format(samp) )
    # obtain sample attributes 
    sample_type = d_samp[samp]['type']
  
    # Choose full path of sample by its type  
    full_path = ''
    if sample_type == 'sig':
      l_color     = d_samp[samp]['l_color']
      full_path = '{0}/AllSignals{1}'.format(sig_path, sig_all_file)
      l_sig.append(samp)
      doSigWeight = True 
    elif sample_type == 'bkg':
      f_color     = d_samp[samp]['f_color']
      full_path = '{0}/{1}{2}'.format(bkg_path, samp, bkg_suffix)
      l_bkg.append(samp)
    elif sample_type == 'data':
      full_path = '{0}/data{1}'.format(data_path, data_suffix)
    else:
      print('Please set a sample type in samples.py for sample {0}'.format(samp) )
    
    if 'fake' in samp:
      full_path = '/usatlas/groups/bnl_local/jshahini/December7_v2.7_Fakes/SusySkimHiggsino_v2.7_SUSY16_Fakes_tree_j100.root'
      #full_path = ' /usatlas/groups/bnl_local/jshahini/December7_v2.7_Fakes/SusySkimHiggsino_v2.7_SUSY16_Fakes_tree_j100_v2.root' 
      #full_path = '/usatlas/groups/bnl_local/jshahini/November27_v2.6_Fakes/SusySkimHiggsino_v2.6_SUSY16_Fakes_tree_FFs151617.root'
      #full_path = '/usatlas/groups/bnl_local/jshahini/November27_v2.6_Fakes/SusySkimHiggsino_v2.6_SUSY16_Fakes_tree.root'
      #full_path = '/usatlas/groups/bnl_local/jgonski/Sept1_2018_Higgsino_R21_v2.5_Production/skimsForHF/fakes_NoSys_v25_hack_v3_rebinnedFFs.root'
      #full_path = '/usatlas/u/jgonski/HiggsinoPlotting/SusySkimHiggsino_v2.6_SUSY16_Fakes_tree.root'

    cutsAfter = normCutsAfter 

    # assign TFile to a dictionary entry
    d_files[samp] = TFile(full_path)
    print(TFile(full_path))

    # Get TH1F histogram from the TTree in the TFile and store to dictionary entry
    d_hists[samp] = tree_get_th1f( d_files[samp], samp, var, cutsAfter, doSigWeight, hNbins, hXmin, hXmax, lumi, variable_bin, hXarray)
    # ---------------------------------------------------- 
    # Stacked histogram: construct and format
    # ---------------------------------------------------- 
    # extract key outputs of histogram 
    hist        = d_hists[samp][0]
    nYield      = d_hists[samp][1]
    h_intgl_low = d_hists[samp][2]
    h_intgl_upp = d_hists[samp][3]
    nYieldErr   = d_hists[samp][4]
    nRaw        = d_hists[samp][5]
   
    # samp : nYield number of events for each sample
    d_yield[samp]    = nYield
    d_yieldErr[samp] = nYieldErr
    d_raw[samp]      = nRaw
    
    # add background to stacked histograms
    if sample_type == 'bkg':
      hs.Add(hist)
      hs_intgl_low.Add(h_intgl_low)
      hs_intgl_upp.Add(h_intgl_upp)
      
      format_hist(hist, 1, 0, 1, f_color, 1001, 0)
      nTotBkg  += nYield
      nTotBkgRaw  += nRaw
      nVarBkg  += nYieldErr ** 2
    
    if sample_type == 'sig':
      #format_hist(hist, 2, l_color, 2, f_color)
      format_hist(hist, 3, l_color, l_styles[Nsignal_count], f_color=0)
      Nsignal_count += 1
    
    if sample_type == 'data':
      h_dat = hist
      format_hist(h_dat,  1, kBlack, 1)
      N_dat = nYield

  errStatBkg = sqrt( nVarBkg ) # treat total statistical error as sum in quadrature of sample stat errors
  errTotBkg  = sqrt( errStatBkg**2 + (0.2 * nTotBkg) ** 2 )
  errForBinomialZ = errTotBkg / nTotBkg

  print('errStatBkg: {0:.3f}, sqrtB: {1:.3f}, errTotBkg: {2:.3f}'.format(errStatBkg, sqrt(nTotBkg), errTotBkg))

  print('==============================================')
  print('{0}, Data, {1}'.format(sig_reg, N_dat))
  print('----------------------------------------------')
  #print('{0}, Total bkg, {1:.3f} +/- {2:.3f}'.format(sig_reg, nTotBkg, errTotBkg))
  print('{0}, Total bkg, {1:.3f}, {2:.3f}'.format(sig_reg, nTotBkg, errTotBkg))
  print('----------------------------------------------')
 

  # ----------------------------------------------------------------- 
  # Put the legend on the RHS outside the actual plot
  # ----------------------------------------------------------------- 
  d_bkg_leg = {}
  if legend_outside_plot:
    leg = mk_leg(0.67, 0.40, 0.85, 0.98, sig_reg, l_sampOther, d_samp, nTotBkg,errForBinomialZ, d_hists, d_yield, d_yieldErr, d_raw, sampSet_type='bkg', txt_size=0.06)
    l_bkg_leg = ['samp1']
    d_bkg_leg['samp1'] = mk_leg(0.67, 0.05, 0.85, 0.90, sig_reg, l_samp_bkg, d_samp, nTotBkg, errForBinomialZ, d_hists, d_yield, d_yieldErr, d_raw, sampSet_type='bkg', txt_size=0.04)

  # ----------------------------------------------------------------- 
  # Otherwise squeeze it inside the actual plot
  # ----------------------------------------------------------------- 
  else:
    # legend for signals, data and total bkg yield
    leg = mk_leg(0.44, 0.68, 0.63, 0.93, sig_reg, l_sampOther, d_samp, nTotBkg, errForBinomialZ, d_hists, d_yield, d_yieldErr, d_raw, sampSet_type='bkg', txt_size=0.033)
    l_bkg_leg = ['samp1'] 
    d_bkg_leg['samp1'] = mk_leg(0.69, 0.68, 0.84, 0.93, sig_reg, l_samp_bkg, d_samp, nTotBkg, errForBinomialZ, d_hists, d_yield, d_yieldErr, d_raw, sampSet_type='bkg', txt_size=0.033)
   
  print('==============================================')
  
  # ----------------------------------------------------------------- 
  #
  # Make MC error histogram (background uncertainty hatching)
  #
  pc_sys = 20 # percentage systematic uncertainty
  h_mcErr = mk_mcErr(hs, pc_sys, hNbins, hXmin, hXmax, variable_bin, hXarray)
  h_mcErr.SetFillStyle(3245) # hatching 
  h_mcErr.SetFillColor(kGray+2)
  h_mcErr.SetLineWidth(2)
  # make other markings invisible
  #h_mcErr.SetLineColorAlpha(kWhite, 0)
  h_mcErr.SetLineColorAlpha(kGray+2, 1.0)
  h_mcErr.SetMarkerColorAlpha(kWhite, 0)
  h_mcErr.SetMarkerSize(0)
  #h_mcErr.SetMarkerColorAlpha(kWhite, 0)
  if 'Pass' in sig_reg or 'preselect' in sig_reg:
    leg.AddEntry(h_mcErr, 'SM stat #oplus 20% syst ({0:.1f})'.format(nTotBkg), 'lf')
  else:
    #leg.AddEntry(h_mcErr, '#scale[0.8]{SM stat #oplus 20% syst} ' + '({0:.1f} #pm {1:.1f})'.format(nTotBkg, errTotBkg), 'lf')
    if legend_outside_plot:
      leg.AddEntry(h_mcErr, 'SM Total ' + '({0:.1f}, {1:.0f}, 100%)'.format(nTotBkg, nTotBkgRaw), 'lf')
    else: 
      leg.AddEntry(h_mcErr, '#scale[0.8]{SM stat #oplus 20% syst} ' + '({0:.1f})'.format(nTotBkg), 'lf')
  # ----------------------------------------------------------------- 
  
  # ----------------------------------------------------------------- 
  #
  # Now all background histogram and signals obtained
  # Proceed to make significance scan
  #
  # ----------------------------------------------------------------- 
 
  # Dicitonary for histograms and its significance plots
  # in format {samp_name : histogram}
  d_hsig = {}
  d_hsigZ20 = {}
  d_hsigZ05 = {}
  
  # obtain direction of cut  
  cut_dir = d_vars[var]['cut_dir'] 
  
  # calculate significances for signals only
  for samp in l_samp:
    sample_type = d_samp[samp]['type']
    if sample_type == 'sig':
      print samp
      d_hsig[samp] = d_hists[samp][0]
      h_signal_low = d_hists[samp][2]
      h_signal_upp = d_hists[samp][3]
      h_reg = d_hists[samp][0]
      
      if 'SR' in sig_reg and not unblind :
        d_hsigZ20[samp] = mk_sigZ_plot(h_reg, hs, 20, cut_dir, errForBinomialZ, hNbins, hXmin, hXmax,variable_bin,hXarray)
        d_hsigZ05[samp] = mk_sigZ_plot(h_reg, hs, 05, cut_dir, errForBinomialZ, hNbins, hXmin, hXmax,variable_bin,hXarray)


  
  # ----------------------------------------------------------------- 
  # Proceed to plot
  plot_selections(var, hs, d_hsig, h_dat, h_mcErr, d_hsigZ05, d_hsigZ20, leg, l_bkg_leg, d_bkg_leg, lumi, save_name, pc_sys, sig_reg, nTotBkg, l_sig, cutsAfter, annotate_text, variable_bin, l_cuts, print_lumi, unblind, cutArrow, IsLogY)
  # ----------------------------------------------------------------- 
  
  return nTotBkg

#____________________________________________________________________________
def plot_selections(var, h_bkg, d_hsig, h_dat, h_mcErr, d_hsigZ05, d_hsigZ20, leg, l_bkg_leg, d_bkg_leg, lumi, save_name, pc_sys, sig_reg, nTotBkg, l_sig, cutsAfter, annotate_text, variable_bin, l_cuts, print_lumi, unblind=False, cutArrow=False, IsLogY=True):
  '''
  plots the variable var given input THStack h_bkg, one signal histogram and legend built
  makes a dat / bkg panel in lower part of figure
  '''
  print('Proceeding to plot')
  
  # gPad left/right margins
  gpLeft = 0.17
  if legend_outside_plot:
    gpRight = 0.35
    can  = TCanvas('','',1250,1000)
  else:
    gpRight = 0.05
    can  = TCanvas('','',1000,1000)
    
  d_vars = configure_vars()
  
 #==========================================================
  # Build canvas
  customise_gPad() 
  pad1 = TPad('pad1', '', 0.0, 0.40, 1.0, 1.0)
  pad2 = TPad('pad2', '', 0.0, 0.00, 1.0, 0.4)
  pad1.Draw()
  pad1.cd()
  
  customise_gPad(top=0.03, bot=0.04, left=gpLeft, right=gpRight)
  if IsLogY: pad1.SetLogy()
  
  #=============================================================
  # draw and decorate
  # draw elements
  h_bkg.Draw('hist')
  # draw signal samples
  for samp in d_hsig:
    print('Drawing {0}'.format(samp))
    d_hsig[samp].Draw('hist same') #e2 = error coloured band
    #if IsLogY ==False: customise_axes(d_hsig[samp], xtitle, ytitle, 1.2, IsLogY, enlargeYaxis)
  # Clone the total background histogram to draw the line
  h_mcErr_clone = h_mcErr.Clone()
  h_mcErr_clone.SetFillColorAlpha(kWhite, 0)
  h_mcErr_clone.SetFillStyle(0)
  h_mcErr.Draw('same e2')
  h_mcErr_clone.Draw('same hist')
  
  # IMPORTANT: only draw data for control regions until unblinded
  if 'SR' not in sig_reg or unblind:
    h_dat.Draw('hist same ep')
    # Data point size 
    h_dat.SetMarkerSize(1.9)
    h_dat.SetLineWidth(2)

  if not legend_outside_plot:
    leg.Draw('same')
  for bkg_leg in l_bkg_leg:
    d_bkg_leg[bkg_leg].Draw('same')
  
  #==========================================================
  # calculate bin width 
  hNbins = d_vars[var]['hXNbins']
  hXmin  = d_vars[var]['hXmin']
  hXmax  = d_vars[var]['hXmax']
  if not variable_bin: binWidth = (hXmax - hXmin) / float(hNbins)
  
  # label axes of top pad
  xtitle = ''
  binUnits = d_vars[var]['units']
  if variable_bin:
    ytitle = 'Events / bin'
  elif 0.1 < binWidth < 1:
    ytitle = 'Events / {0:.2f} {1}'.format(binWidth, binUnits)
  elif binWidth <= 0.1:
    ytitle = 'Events / {0:.2f} {1}'.format(binWidth, binUnits)
  elif binWidth >= 1:
    ytitle = 'Events / {0:.0f} {1}'.format(binWidth, binUnits)
  enlargeYaxis = False
  if 'Pass' in sig_reg or 'preselect' in sig_reg or IsLogY==False:
    enlargeYaxis = True
  if IsLogY == False:
    ymax = 0.0
    for samp in d_hsig:
       ymaxCurr = d_hsig[samp].GetMaximum()
       if ymaxCurr > ymax: ymax = ymaxCurr
    h_bkg.SetMaximum(max(h_bkg.GetMaximum()*1.7, ymax*1.7))
  else: customise_axes(h_bkg, xtitle, ytitle, 1.5, 1.0, IsLogY, enlargeYaxis)
 
  # Add arrow in case of N-1 plots to indicate where cut would be
  if cutArrow:
    add_cut_arrow_to_plot(h_bkg, d_vars, var, hXmax, IsLogY)
  
  # Add text e.g. ATLAS Label, sqrt{s}, lumi to plot
  add_text_to_plot(sig_reg, lumi, l_cuts, annotate_text, print_lumi)

  gPad.RedrawAxis() 
  
  #---------------------------------------------------------------
  # Pad 2: lower panel for data/SM or significance
  #---------------------------------------------------------------
  
  can.cd()
  pad2.Draw()
  #pad2.SetLogy()
  pad2.cd()
  customise_gPad(top=0.05, bot=0.39, left=gpLeft, right=gpRight)
  
  varTeX = 'tlatex'
  
  Xunits = d_vars[var]['units']
  if Xunits == '':
    #xtitle = '{0}'.format( d_vars[var]['tlatex'])
    xtitle = '{0}'.format( d_vars[var][varTeX])
  else:
    xtitle = '{0} [{1}]'.format( d_vars[var][varTeX], Xunits ) 
  
  # SRs draw significance scans, CRs draw  
  if 'SR' in sig_reg and not unblind:
    cut_dir = d_vars[var]['cut_dir']
    draw_sig_scan(l_sig, d_hsigZ20, cut_dir, xtitle, hXmin, hXmax) 
    gPad.RedrawAxis() 
  if 'SR' not in sig_reg or unblind:
    #==========================================================
    # MC error ratio with MC (hatching)
    h_mcErrRatio = h_mcErr.Clone()
    h_mcErrRatio.Divide(h_bkg.GetStack().Last())
    h_mcErrRatio.SetFillStyle(3245)
    h_mcErrRatio.SetFillColor(kGray+2)
    h_mcErrRatio.SetMarkerSize(0) 
    h_mcErrRatio.Draw('e2')
    
    # Draw line for the ratios
    l = draw_line(hXmin, 1, hXmax, 1, color=kGray+2, style=1) 
    l.Draw()
    
    # Draw data on top
    hRatio = h_dat.Clone()
    hRatio.Divide(h_bkg.GetStack().Last())  
    hRatio.Draw('same ep') 
    
    # Ensure uncertainties in ratio panel are consistent with upper plot
    for ibin in xrange(0, hRatio.GetNbinsX()+1) :
      ratioContent = hRatio.GetBinContent(ibin)
      dataError = h_dat.GetBinError(ibin)
      dataContent = h_dat.GetBinContent(ibin)
      if ratioContent > 0 :
        hRatio.SetBinError(ibin, ratioContent * dataError / dataContent) 
      
    # Ensure error bars drawn even if point outside range 
    oldSize = hRatio.GetMarkerSize()
    hRatio.SetMarkerSize(0)
    hRatio.DrawCopy("same e0")
    hRatio.SetMarkerSize(oldSize)
    hRatio.Draw("PE same") 
    hRatio.GetYaxis().SetTickSize(0)
  
    ytitle = 'Data / SM'
    customise_axes(h_mcErrRatio, xtitle, ytitle, 1.2)
    gPad.RedrawAxis() 
    
    # Insert arrows indicating data point is out of range of ratio panel
    l_arrows = {} 
    for mybin in range( hRatio.GetXaxis().GetNbins()+1 ):  
      Rdat = hRatio.GetBinContent(mybin)
      Rerr = hRatio.GetBinError(mybin)
      xval = hRatio.GetBinCenter(mybin)
      if Rdat - Rerr > 2:
        l_arrows[xval] = cut_arrow( xval, 1.7, xval, 1.9, 'up', 0.008, 6, kOrange+2 )
        l_arrows[xval][1].Draw()
    #draw_data_vs_pred(pad2, h_dat, hRatio, h_mcErrRatio, xtitle, hXmin, hXmax) 

    #==========================================================
  
  if legend_outside_plot:
    print('Drawing legend in lower panel')
    leg.Draw('same')
  
    myText(0.715, 0.395, 'Stat #oplus 20% syst', text_size*0.8, kBlack) 
  
  #==========================================================
  # save everything
  can.cd()
  can.SaveAs(save_name + '.pdf')
  #can.SaveAs(save_name + '.eps')
  #can.SaveAs(save_name + '.png')
  can.Close()

#____________________________________________________________________________
def add_cut_arrow_to_plot(h_bkg, d_vars, var, hXmax, IsLogY):

  print('Drawing arrow to indicate where cut is in N-1 plot')
   
  #---------------------------------------
  #
  # Set height of arrow
  ymin_Ar = gPad.GetUymin()
  ymax_Ar = h_bkg.GetMaximum()
  if IsLogY:     ymax_Ar = 80
  if not IsLogY: ymax_Ar = 0.8*ymax_Ar
  
  # Arrow horizontal length is 6% of the maximum x-axis bin 
  arr_width = hXmax * 0.06
  
  # Case 1-sided cut 
  cut_pos = d_vars[var]['cut_pos']
  cut_dir = d_vars[var]['cut_dir']
  cutAr = cut_arrow(cut_pos, ymin_Ar, cut_pos, ymax_Ar, cut_dir, 0.012, arr_width)
  cutAr[0].Draw()
  cutAr[1].Draw()
  
  # Case 2-sided cuts, add an extra arrow
  if 'cut_pos2' in d_vars[var].keys():
    cut_pos2 = d_vars[var]['cut_pos2']
    cut_dir2 = d_vars[var]['cut_dir2']
    cutAr2   = cut_arrow(cut_pos2, ymin_Ar, cut_pos2, ymax_Ar, cut_dir2, 0.012, arr_width)
    cutAr2[0].Draw()
    cutAr2[1].Draw()
  #---------------------------------------
  
#____________________________________________________________________________
def add_text_to_plot(sel_reg, lumi, l_cuts, annotate_text, print_lumi):
  
  print('Adding ATLAS label, sqrt{s}, lumi, selection region, ntuple version to plot')
  
  #---------------------------------------------------------------
  #
  # Replace -mm suffix in selection region with mu mu etc
  if '-ee' in sel_reg:
    sel_reg = sel_reg.replace('-ee', ' ee', 1)
  if '-mm' in sel_reg:
    sel_reg = sel_reg.replace('-mm', ' #mu#mu', 1)
  if '-em' in sel_reg:
    sel_reg = sel_reg.replace('-em', ' e#mu', 1)
  if '-me' in sel_reg:
    sel_reg = sel_reg.replace('-me', ' #mue', 1)
  if '-ee-me' in sel_reg:
    sel_reg = sel_reg.replace('-ee-me', ' ee+#mue', 1)
  if '-mm-em' in sel_reg:
    sel_reg = sel_reg.replace('-mm-em', ' #mu#mu+e#mu', 1)

  if '-SF' in sel_reg:
    sel_reg = sel_reg.replace('-SF', ' ee+#mu#mu', 1)
  if '-DF' in sel_reg:
    sel_reg = sel_reg.replace('-DF', ' e#mu+#mue', 1)
  if '-AF' in sel_reg:
    sel_reg = sel_reg.replace('-AF', ' ee+#mu#mu+e#mu+#mue', 1)
  #
  #---------------------------------------------------------------

  #---------------------------------------------------------------
  #
  # Text for ATLAS, energy, lumi, region, ntuple status
  
  # ATLAS Label
  myText(0.21, 0.87, ATLAS_label, text_size*1.2, kBlack)
  
  # sqrt{s} and lumi
  print('Lumi to put on plot: {0}'.format(print_lumi))
  myText(0.21, 0.81, '#sqrt{s}' + ' = 13 TeV, {0:.1f}'.format(float(print_lumi)) + ' fb^{#minus1}', text_size, kBlack) 
  
  # Selection region label
  if sel_reg == 'preselect':
    sel_reg = 'SUSY16'
    myText(0.21, 0.77, sel_reg, text_size*0.7, kBlack) 
  else:
    myText(0.21, 0.77, sel_reg, text_size*0.7, kBlack) 
  
  # Version of SusySkimHiggsino ntuples
  myText(0.21, 0.73, NTUP_status, text_size*0.7, kGray+1) 
  
  # Additional annotations
  if not annotate_text == '':
    myText(0.21, 0.68, annotate_text, text_size*0.65, kGray+1) 
  #
  #---------------------------------------------------------------
  
  #---------------------------------------------------------------
  #
  # If we're using legend outside the plot, also print cuts applied
  if legend_outside_plot:
    
    myText(0.67, 0.92, 'Sample (Weighted, Raw, Fraction)', text_size*0.95, kBlack) 

    # List all the cuts in the plot
    for cut_number, my_cut in enumerate(l_cuts):
      if 'Truth' in my_cut:
	my_cut_new = my_cut.replace('&& ((lep1TruthMatched && lep2TruthMatched) || (DatasetNumber >= 407311 && DatasetNumber <= 407315))','')
        my_cut2 = '&& ((lep1TruthMatched && lep2TruthMatched) || '
        my_cut3= '(DatasetNumber >= 407311 && DatasetNumber <= 407315))'
	cut_txt_size = text_size * 0.37
      	myText(0.42, 0.93-float(cut_number)*0.02, '{0}'.format(my_cut_new), cut_txt_size, kGray+1) 
      	myText(0.42, 0.93-float(cut_number+1)*0.02, '{0}'.format(my_cut2), cut_txt_size, kGray+1) 
      	myText(0.42, 0.93-float(cut_number+2)*0.02, '{0}'.format(my_cut3), cut_txt_size, kGray+1) 
      else: 
	# If the cut is over 50 characters long, shrink it
      	if len(my_cut) > 50: cut_txt_size = text_size * 0.27
      	else:                cut_txt_size = text_size * 0.42
      	myText(0.42, 0.93-float(cut_number)*0.02, '{0}'.format(my_cut), cut_txt_size, kGray+1) 
  # 
  #--------------------------------------------------------------- 

#____________________________________________________________________________
def mk_mcErr(hStack, pc_sys, Nbins=100, xmin=0, xmax=100, variable_bin=False, hXarray=0):
  '''
  Make the hatched error histogram for SM prediction
  '''
  if variable_bin:
    h_mcErr = TH1D('', "", Nbins, array('d', hXarray) )
  else:
    h_mcErr = TH1D('', "", Nbins, xmin, xmax)
  
  print( 'Making MC err' )
  for my_bin in range( hStack.GetStack().Last().GetSize() ):
    yval = hStack.GetStack().Last().GetBinContent(my_bin)
    
    if yval == 0:
      yval = 0.001
    # Handle pathologies where bin yield is negative
    if yval < 0:
      yval = 0.001
      print( '\nWARNING: negative histogram value {0} in bin {1}, setting to 0.001'.format(yval, my_bin) )
  
    # get statistical variance as sum of weights squared
    yval_GetErr   = hStack.GetStack().Last().GetBinError(my_bin)
    # add stat and sys err in quadrature
    yval_err = sqrt( yval_GetErr ** 2 + ( 0.01 * pc_sys * yval ) ** 2 )
    h_mcErr.SetBinContent( my_bin, yval )
    h_mcErr.SetBinError(   my_bin, yval_err ) 
  
  return h_mcErr

#____________________________________________________________________________
def draw_sig_scan(l_signals, d_hsigZ, cut_dir, xtitle, hXmin, hXmax):
  '''
  Draw significance scan for signals in list l_signals
  using significance histograms d_hsigZ
  labelled by cut_dir, xtitle in range hXmin, hXmax
  '''
  print('Making significance scan plot in lower panel')
  #----------------------------------------------------
  # draw significances
  d_samp = configure_samples()
  ytitle = 'Significance Z'
  currentMax = 0.0
  for i, samp in enumerate(l_signals):	#find Zn max to have right maximum
    hsigZ = d_hsigZ[samp]
    if hsigZ.GetMaximum() > currentMax: currentMax = hsigZ.GetMaximum()
  for i, samp in enumerate(l_signals):	
    hsigZ = d_hsigZ[samp]
    hsigZ.Draw('hist same')
    if i < 1:
      customise_axes(hsigZ, xtitle, ytitle, 2.0, currentMax)
    l_color     = d_samp[samp]['l_color'] 
    format_hist(hsigZ, 2, l_color, 1, 0)
  
  # draw line for the ratio = 1
  l = draw_line(hXmin, 1.97, hXmax, 1.97, color=kAzure+1, style=7) 
  l.Draw()
  x_txt = 0.77
  if legend_outside_plot: x_txt = 0.50
  if 'left' in cut_dir:
    myText(x_txt, 0.83, 'Cut left',  0.07, kBlack)
  if 'right' in cut_dir:
    myText(x_txt, 0.83, 'Cut right', 0.07, kBlack)

#____________________________________________________________________________
def mk_SoverB_plot(h_intgl_sig, h_intgl_bkg, pc_syst, cut_dir, errForBinomialZ, Nbins=100, xmin=0, xmax=100):
  '''
  Takes background & signal one-sided integral histograms
  and input percentage systematic
  Returns the signal significance Z histogram
  '''
  print('Making S/B plot')
  h_pcsyst = TH1D('', "", Nbins, xmin, xmax)

  if 'right' in cut_dir: 
    for my_bin in range( h_intgl_bkg.GetStack().Last().GetSize() ): 
       sExp =  h_intgl_sig.Integral( my_bin, h_intgl_sig.GetSize() + 1 )
       bExp =  h_intgl_bkg.GetStack().Last().Integral( my_bin, h_intgl_bkg.GetStack().Last().GetSize() + 1 )
       bin_low  = h_intgl_bkg.GetStack().Last().GetBinLowEdge( my_bin ) 
       if bExp <= 0: bExp = 0.1
       if sExp < 0: sExp = 0.0
       RS_sigZ = sExp / bExp
       
       #print('{0}, {1}, {2}, {3}, {4}, {5}'.format(my_bin, bin_low, bExp, sExp, RS_sigZ,errForBinomialZ) )
       #h_pcsyst.Fill(bin_low, RS_sigZ)
       h_pcsyst.SetBinContent(my_bin, RS_sigZ)
  

  elif 'left' in cut_dir: 
    for my_bin in range( h_intgl_bkg.GetStack().Last().GetSize(),0,-1 ): 
       sExp =  h_intgl_sig.Integral( 0, my_bin )
       bExp =  h_intgl_bkg.GetStack().Last().Integral( 0, my_bin )
       bin_low  = h_intgl_bkg.GetStack().Last().GetBinLowEdge( my_bin ) 
       if bExp <= 0: bExp = 0.1
       if sExp < 0: sExp = 0.0
       RS_sigZ = sExp / bExp
       
       #print('{0}, {1}, {2}, {3}, {4}, {5}'.format(my_bin, bin_low, bExp, sExp, RS_sigZ,errForBinomialZ) )
       #h_pcsyst.Fill(bin_low, RS_sigZ)
       h_pcsyst.SetBinContent(my_bin, RS_sigZ)

  return h_pcsyst

#____________________________________________________________________________
def mk_sigZ_plot(h_intgl_sig, h_intgl_bkg, pc_syst, cut_dir, errForBinomialZ, Nbins=100, xmin=0, xmax=100,variable_bin=False, hXarray=0):
  '''
  Takes background & signal one-sided integral histograms
  and input percentage systematic
  Returns the signal significance Z histogram
  '''
  print('Making significance plot')
  if variable_bin:
    h_pcsyst   = TH1D('', "", Nbins, array('d', hXarray) )
  else:
    h_pcsyst   = TH1D('', "", Nbins, xmin, xmax)

  if 'right' in cut_dir: 
    for my_bin in range( h_intgl_bkg.GetStack().Last().GetSize() ): 
       sExp =  h_intgl_sig.Integral( my_bin, h_intgl_sig.GetSize() + 1 )
       bExp =  h_intgl_bkg.GetStack().Last().Integral( my_bin, h_intgl_bkg.GetStack().Last().GetSize() + 1 )
       bin_low  = h_intgl_bkg.GetStack().Last().GetBinLowEdge( my_bin ) 
       if bExp < 0: bExp = 0.0
       if sExp < 0: sExp = 0.0
       #RS_sigZ = RooStats.NumberCountingUtils.BinomialExpZ( sExp, bExp, errForBinomialZ )
       RS_sigZ = getZPoisson(sExp, bExp, errForBinomialZ ) 
       if RS_sigZ > 100: 
         RS_sigZ = 0.0
       
       #print('{0}, {1}, {2}, {3}, {4}, {5}'.format(my_bin, bin_low, bExp, sExp, RS_sigZ,errForBinomialZ) )
       #h_pcsyst.Fill(bin_low, RS_sigZ)
       h_pcsyst.SetBinContent(my_bin, RS_sigZ)
  

  elif 'left' in cut_dir: 
    for my_bin in range( h_intgl_bkg.GetStack().Last().GetSize(),0,-1 ): 
       sExp =  h_intgl_sig.Integral( 0, my_bin )
       bExp =  h_intgl_bkg.GetStack().Last().Integral( 0, my_bin )
       bin_low  = h_intgl_bkg.GetStack().Last().GetBinLowEdge( my_bin ) 
       if bExp < 0: bExp = 0.0
       if sExp < 0: sExp = 0.0
       #RS_sigZ = RooStats.NumberCountingUtils.BinomialExpZ( sExp, bExp, errForBinomialZ  )
       RS_sigZ = getZPoisson(sExp, bExp, errForBinomialZ ) 
       if RS_sigZ > 100: 
         RS_sigZ = 0.0
       
       #print('{0}, {1}, {2}, {3}, {4}, {5}'.format(my_bin, bin_low, bExp, sExp, RS_sigZ,errForBinomialZ) )
       h_pcsyst.Fill(bin_low, RS_sigZ)
       #h_pcsyst.SetBinContent(my_bin-1, RS_sigZ)

  return h_pcsyst

#____________________________________________________________________________
def getZPoisson(s, b, db):
    """
    The significance for optimisation.

    s: total number of signal events
    b: total number of background events
    db: MC stat uncertainty for the total bkg
    """
    # add flat 30% systematic to the total bkg uncertainty
    flatsys = 0.3
    n = s+b
    sigma = sqrt(db**2+(flatsys*b)**2)

    if s <= 0 or b <= 0:
        return 0

    factor1 = n*log( (n*(b+sigma**2))/(b**2+n*sigma**2) )
    factor2 = (b**2/sigma**2)*log( 1 + (sigma**2*(n-b))/(b*(b+sigma**2)) )

    try:
        return sqrt( 2 * (factor1 - factor2))
    except ValueError:
        return 0
#____________________________________________________________________________
def mk_leg(xmin, ymin, xmax, ymax, sig_reg, l_samp, d_samp, nTotBkg, errForBinomialZ, d_hists, d_yield, d_yieldErr, d_raw, sampSet_type='bkg', txt_size=0.05) :
  '''
  @l_samp : Constructs legend based on list of samples 
  @nTotBkg : Total background events
  @d_hists : The dictionary of histograms 
  @d_samp : May from samples to legend text
  @d_yields : The dictionary of yields 
  @d_yieldErr : Dictionary of errors on the yields
  @d_raw : Dictionary of raw yields
  @sampSet_type : The type of samples in the set of samples in the list 
  '''  

  # ---------------------------------------------------- 
  # Legend: construct and format
  # ---------------------------------------------------- 
  leg = TLegend(xmin,ymin,xmax,ymax)
  leg.SetBorderSize(0)
  leg.SetTextSize(txt_size)
  leg.SetNColumns(1)

  # legend markers 
  d_legMk = {
    'bkg'  : 'f',
    'sig'  : 'l',
    'data' : 'ep'
    }

  # Need to reverse background order so legend is filled as histogram is stacked
  if sampSet_type == 'bkg':
    l_samp = [x for x in reversed(l_samp)]
  for samp in l_samp: 
    #print( 'Processing {0}'.format(samp) )
    # obtain sample attributes 
    hist        = d_hists[samp][0]
    sample_type = d_samp[samp]['type']
    leg_entry   = d_samp[samp]['leg']
    legMk       = d_legMk[sample_type]
   
    #print('samp: {0}, type: {1}, legMk: {2}'.format(samp, sample_type, legMk) ) 
    # calculate the % of each background component and put in legend
    pc_yield   = 0
    # Add more information when legend outside plot
    if legend_outside_plot:
      if sample_type == 'bkg':
        pc_yield = 100 * ( d_yield[samp] / float(nTotBkg) )
        leg_txt = '{0} ({1:.1f}, {2:.0f}, {3:.1f}%)'.format( leg_entry, d_yield[samp], d_raw[samp], pc_yield )
      if sample_type == 'sig':
        leg_txt = '{0} ({1:.1f}, {2:.0f})'.format( leg_entry, d_yield[samp], d_raw[samp] )
    else:
      if sample_type == 'bkg':
        pc_yield = 100 * ( d_yield[samp] / float(nTotBkg) )
        leg_txt = '{0} ({1:.1f}%)'.format( leg_entry, pc_yield )
      if sample_type == 'sig':
        leg_txt = '{0} ({1:.1f})'.format(leg_entry, d_yield[samp])
    if sample_type == 'data':
      leg_txt = '{0} ({1:.0f} Events)'.format(leg_entry, d_yield['data'])  
    leg.AddEntry(hist, leg_txt, legMk)
    #print('{0}, {1}, {2:.3f}, {3:.3f}%'.format(sig_reg, samp, d_yield[samp], pc_yield) )
    print('{0}, {1}, {2:.3f} +/- {3:.3f}, {4:.0f}'.format(sig_reg, samp, d_yield[samp], d_yieldErr[samp], d_raw[samp]) )
    if(sample_type == 'sig'): 
      RS_sigZ = RooStats.NumberCountingUtils.BinomialExpZ( d_yield[samp], nTotBkg, errForBinomialZ )
      print( 'Sensitivity: nSig =  {0}, nBkg = {1}, background error = {2}, sigma: {3}'.format(d_yield[samp], nTotBkg, errForBinomialZ, RS_sigZ) )


  return leg
    
#_______________________________________________________
def tree_get_th1f(f, hname, var, cutsAfter='', doSigWeight=False, Nbins=100, xmin=0, xmax=100, lumifb=35, variable_bin=False, hXarray=0):
  '''
  from a TTree, project a leaf 'var' and return a TH1F
  '''
  print(hname)
  if variable_bin:
    h_AfterCut   = TH1D(hname + '_hist', "", Nbins, array('d', hXarray) )
    hOneBin      = TH1D(hname + '_onebin', '', 2, 0, 2)
  else:
    h_AfterCut   = TH1D(hname + '_hist', "", Nbins, xmin, xmax)
    hOneBin      = TH1D(hname + '_onebin', '', 2, 0, 2)
 
  #h_AfterCut.Sumw2()
  
  lumi     = lumifb * 10 ** 3 # convert to [pb^{-1}]
  #if doSigWeight:
  #  weights = "(genWeight * eventWeight * leptonWeight * jvtWeight * bTagWeight * pileupWeight * FFWeight * (pileupWeight < 100000))"
  #else:

  # fix for huge x-sections of higgs samples
  ggHFix_weight = "((DatasetNumber == 345073 ? 0.007776 : 1) * (DatasetNumber == 345106 ? 0.0002176 : 1) * (DatasetNumber == 345120 ? 0.007776 : 1) * (DatasetNumber == 345323 ? 0.02268 : 1) * (DatasetNumber == 345324 ? 0.02268 : 1))"
  # fix very large sherpa weights
  sherpa_weight_fix = "( (abs(eventWeight) > 100) && ( (DatasetNumber > 363000 && DatasetNumber < 365000) || (DatasetNumber > 407310 && DatasetNumber < 407320) || (DatasetNumber > 345700 && DatasetNumber < 345750) ) ? (1/eventWeight) : 1 )"
  # veto some phase space of sherpa low-mll samples as there was some bug in the filter
  sherpa_lowmll_fix = "(( ((DatasetNumber>=364358&&DatasetNumber<=364363)||(DatasetNumber>=36480&&DatasetNumber<=36482)) && (mll>10&&lep2Pt>5) ) ? 0 : 1)"
  # restrict to ee events with Rll > 0.3 due to modeling issues -> cannot go lower in mll as 3 GeV due to stats
  electron_fix = "( (lep1Flavor == 1 && lep2Flavor == 1) ? (Rll > 0.3 && mll > 3) : 1 )"
  weights = "(genWeight * eventWeight * leptonWeight * jvtWeight * bTagWeight * pileupWeight * FFWeight * {} * {} * {} * {})".format(ggHFix_weight, sherpa_weight_fix, sherpa_lowmll_fix, electron_fix)
  
  #cut_after = '({0}) * {1} * ({2})'.format(cutsAfter, weights, lumi) 
  #cut_after = '({0}) * {1} * ( RandomRunNumber > 320000 ? 43800 : 36200 )'.format(cutsAfter, weights) 
  
  #-----------------------------------------
  # FIX ME
  #-----------------------------------------
  cut_after = '({0}) * {1} * (140452)'.format(cutsAfter, weights) 
  #cut_after = '({0}) * {1} * (80000)'.format(cutsAfter, weights) 
  #cut_after = '({0}) * {1} * (78500)'.format(cutsAfter, weights) 
  #cut_after = '({0}) * {1} * (36159)'.format(cutsAfter, weights) 
  # if qualifies as mc16d, multiply by extra factor of lumi ratio squared....
  print('---------------------------------')
  print('Final weighted cut string: ')
  print(cut_after)
  print('---------------------------------')
  # ========================================================= 
  if 'data' not in hname:

    if useLooseNominal:
      t = f.Get(hname + '_LOOSE_NOMINAL') 
    else:
      t = f.Get( hname + '_NoSys') 
    t.Project( hname + '_hist',           var, cut_after )
    t.Project( hname + '_onebin', 'lep1Signal', cut_after )
  # all data no need to apply weights
  elif 'data' in hname:
    if useLooseNominal:
      t = f.Get(hname + '_LOOSE_NOMINAL') 
    else:
      t = f.Get( hname ) 
    t.Project( hname + '_hist',           var, cutsAfter )
    t.Project( hname + '_onebin', 'lep1Signal', cutsAfter )
  # =========================================================
  # perform integrals to find 
  # total yield, one-sided lower and upper cumulative histos
  nYieldErr = ROOT.Double(0)
  nYield    = h_AfterCut.IntegralAndError(0, Nbins+1, nYieldErr)
  if nYield < 0: nYield = 0.0
  if nYieldErr < 0: nYieldErr = 0.0
 
  h_intgl_lower = TH1D(hname + '_intgl_lower', "", Nbins, xmin, xmax)
  h_intgl_upper = TH1D(hname + '_intgl_upper', "", Nbins, xmin, xmax)
  
  for my_bin in range( h_AfterCut.GetXaxis().GetNbins() + 1 ):
    
    # get lower edge of bin
    bin_low = h_AfterCut.GetXaxis().GetBinLowEdge( my_bin )
    
    # set the negatively weighted values to 0.
    bin_val = h_AfterCut.GetBinContent( my_bin )
    if bin_val < 0:
      print( 'WARNING: Bin {0} of sample {1} has negative entry, setting central value to 0.'.format(my_bin, hname) )
      h_AfterCut.SetBinContent(my_bin, 0.)
    
    # do one-sided integral either side of bin
    intgl_lower = h_AfterCut.Integral( 0, my_bin )
    intgl_upper = h_AfterCut.Integral( my_bin, Nbins+1 ) 
    if intgl_lower < 0.0: intgl_lower = 0.0
    if intgl_upper < 0.0: intgl_upper = 0.0
 
    h_intgl_lower.Fill( bin_low, intgl_lower )
    h_intgl_upper.Fill( bin_low, intgl_upper )
  
  nRaw = h_AfterCut.GetEntries()
  print( 'Jesse  errors hist.IntegralAndError() : sample {0} has integral {1:.3f} +/- {2:.3f}'.format( hname, nYield, nYieldErr ) )
  print( 'Elodie errors hist.GetBinError()      : sample {0} has integral {1:.3f} +/- {2:.3f}'.format( hname, hOneBin.GetBinContent(2), hOneBin.GetBinError(2) ) )
  # =========================================================
  
  return [h_AfterCut, nYield, h_intgl_lower, h_intgl_upper, nYieldErr, nRaw]

#____________________________________________________________________________
def format_hist(hist, l_width=2, l_color=kBlue+2, l_style=1, f_color=0, f_style=1001, l_alpha=1.0):
  
  # Lines
  hist.SetLineColorAlpha(l_color, l_alpha)
  hist.SetLineStyle(l_style)
  hist.SetLineWidth(l_width)
  
  # Fills
  hist.SetFillColor(f_color)
  hist.SetFillStyle(f_style)

  # Markers
  hist.SetMarkerColor(l_color)
  hist.SetMarkerSize(1.1)
  hist.SetMarkerStyle(20)

#____________________________________________________________________________
def customise_gPad(top=0.03, bot=0.15, left=0.17, right=0.08):

  gPad.Update()
  gStyle.SetTitleFontSize(0.0)
  
  # gPad margins
  gPad.SetTopMargin(top)
  gPad.SetBottomMargin(bot)
  gPad.SetLeftMargin(left)
  gPad.SetRightMargin(right)
  
  gStyle.SetOptStat(0) # hide usual stats box 
  
  gPad.Update()
  
#____________________________________________________________________________
def customise_axes(hist, xtitle, ytitle, scaleFactor=1.1, currentMax=3.0, IsLogY=False, enlargeYaxis=False):
  
  # set a universal text size
  text_size = 45
  TGaxis.SetMaxDigits(4) 
  
  #--------------------------------------------
  # X axis
  xax = hist.GetXaxis()
  
  # precision 3 Helvetica (specify label size in pixels)
  xax.SetLabelFont(43)
  xax.SetTitleFont(43)
  
  xax.SetTitle(xtitle)
  xax.SetTitleSize(text_size)
  
  # Upper panel for Events 
  if 'Events' in ytitle:
  #if False:
    xax.SetLabelSize(0)
    xax.SetLabelOffset(0.02)
    xax.SetTitleOffset(2.0)
    xax.SetTickSize(0.04)  
  # Lower panel for data/SM or significance
  else:
    xax.SetLabelSize(text_size - 7)
    xax.SetLabelOffset(0.03)
    xax.SetTitleOffset(3.0)
    xax.SetTickSize(0.08)
 
  gPad.SetTickx() 
  #--------------------------------------------
  
  #--------------------------------------------
  # Y axis
  yax = hist.GetYaxis()
  # precision 3 Helvetica (specify label size in pixels)
  yax.SetLabelFont(43)
  yax.SetTitleFont(43) 
  
  yax.SetTitle(ytitle)
  yax.SetTitleSize(text_size)
  yax.SetTitleOffset(1.3)    
  
  yax.SetLabelOffset(0.015)
  yax.SetLabelSize(text_size - 7)
 
  ymax = hist.GetMaximum()
  ymin = hist.GetMinimum()
  
  # Upper panel for Events
  if 'Events' in ytitle:
    yax.SetNdivisions(505) 
    if IsLogY:
      if enlargeYaxis:
        ymax = 2 * 10 ** 25
        ymin = 0.02
      else:
        ymax = 2 * 10 ** 7
        ymin = 0.01
      hist.SetMaximum(ymax)
      hist.SetMinimum(ymin)
    else:
      if enlargeYaxis:
        hist.SetMaximum(ymax*scaleFactor*2)
        hist.SetMinimum(0.0)
      else:
        hist.SetMaximum(ymax*scaleFactor)
        hist.SetMinimum(0.0)
  
  # Lower panel for data/SM or significance 
  elif 'S / B' in ytitle:
    hist.SetMinimum(0.1)
    hist.SetMaximum(500) 
  elif 'Significance' in ytitle:
    hist.SetMinimum(0.0)
    hist.SetMaximum(currentMax*1.4)
  elif 'Data' in ytitle:
    hist.SetMinimum(0.0)
    hist.SetMaximum(2.0) 
   
  yax.SetNdivisions(205)
  gPad.SetTicky()
  gPad.Update()
  #--------------------------------------------

#____________________________________________________________________________
def myText(x, y, text, tsize=0.05, color=kBlack, angle=0) :
  
  l = TLatex()
  l.SetTextSize(tsize)
  l.SetNDC()
  l.SetTextColor(color)
  l.SetTextAngle(angle)
  l.DrawLatex(x,y,'#bf{' + text + '}')
  l.SetTextFont(4)

#____________________________________________________________________________
def cut_arrow(x1, y1, x2, y2, direction='right', ar_size=1.0, ar_width=10, color=kGray+3, style=1) :
  
  # Draw an arrow to indicate the cut position of a variable
  l = TLine(x1, y1, x2, y2)
  if direction == 'right':
    ar = TArrow(x1-0.02, y2, x1+ar_width, y2, ar_size, '|>')
  if direction == 'left':
    ar = TArrow(x1-ar_width+0.02, y2, x1, y2, ar_size, '<|')
  if direction == 'up':
    ar = TArrow(x1, y1, x1, y2, ar_size, '|>')
  if direction == 'down':
    ar = TArrow(x1, y1, x1, y2, ar_size, '<|')
  
  l.SetLineWidth(4)
  l.SetLineStyle(style)
  l.SetLineColor(color) 
  ar.SetLineWidth(4)
  ar.SetLineStyle(style)
  ar.SetLineColor(color) 
  ar.SetFillColor(color)  
  return [l, ar]

#____________________________________________________________________________
def draw_line(xmin, ymin, xmax, ymax, color=kGray+1, style=2) :

  # Function to draws lines given locations @xmin, ymin, xmax, ymax

  line = TLine(xmin , ymin , xmax, ymax)
  line.SetLineWidth(2)
  line.SetLineStyle(style)
  line.SetLineColor(color) # 12 = gray
  return line
 
#_________________________________________________________________________
def mkdir(dirPath):

  # Makes new directory @dirPath

  try:
    os.makedirs(dirPath)
    print 'Successfully made new directory ' + dirPath
  except OSError:
    pass
 
if __name__ == "__main__":
  main()

