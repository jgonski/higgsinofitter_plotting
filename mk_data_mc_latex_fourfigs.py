#!/usr/bin/env pytho
'''

# Getting started
Run this script as

./mk_data_mc_latex.py

This produces a LaTeX file frames.tex

Then in your beamer template, import this TeX file by

\input{frames}

to insert into the main beamer template e.g. in slides.tex.



# Things one can configure

Define signal regions to include in TeX in list sig_regs = []
Define variables to include in TeX in list my_vars = []
Map the variable name to LaTeX in make_latex() wherein lies a dictionary

mk_frame() is where we configure the LaTeX template for each frame
'''

import os

#____________________________________________________________________________
def main():

    frame_tex = ''



    sig_regs = [
    'CR-top-AF',
    'CR-top-ee',
    'CR-top-mm',
    'CR-top-em',
    'CR-top-me',


    'CR-tau-AF',
    'CR-tau-ee',
    'CR-tau-mm',
    'CR-tau-em',
    'CR-tau-me',

    'VR-VV-AF',
    'VR-VV-ee',
    'VR-VV-mm',
    'VR-VV-em',
    'VR-VV-me',

    'VR-SS-ee-me',
    'VR-SS-mm-em',

    #'VR-SS-SF',
    #'VR-SS-ee',
    #'VR-SS-mm',
    #'VR-SS-me',
    #'VR-SS-em',


    'VRDF-iMLLg-DF',
    'VRem-iMLLg-em',
    'VRme-iMLLg-me',

    'VRDF-iMT2f-DF',
    'VRem-iMT2f-em',
    'VRme-iMT2f-me',
]


    sig_regs = [
    'CR-top-AF',
    'CR-top-ee',
    'CR-top-mm',
    'CR-top-em',
    'CR-top-me',

    'VRDF-iMLLg-DF',
    #'VRem-iMLLg-em',
    #'VRme-iMLLg-me',

    'VRDF-iMT2f-DF',
    #'VRem-iMT2f-em',
    #'VRme-iMT2f-me',
]
    sig_regs = [ #regular validation plots
    'CR-RJR-top-AF',
    'CR-RJR-tau-AF',
    'CR-RJR-VV-AF',
    'VR-RJR-SS-AF',
    'VR-RJR-SS-ee-me',
    'VR-RJR-SS-mm-em',
    'SRSF-RJR-MLL-SF',
    'SRSF-RJR-MT2-SF',
    #'VR-RJR-SS-AF-loose',    
    #'VR-RJR-SS-ee-me-loose',    
    #'VR-RJR-SS-mm-em-loose',    
    'VRDF-RJR-iMLLg-DF',
    'VRDF-RJR-iMT2h-DF',
]

    my_vars = [
    'met_Et',
    #'met_track_Et',
    #'METRel',
    #'METTrackRel',
    #'fabs(dPhiMetAndMetTrack)',
    #'TST_Et',
    'jetPt[0]',
    #'jetPt[1]',
    #'jetPt[2]',
    #'jetPt[3]',
    #'jetPt[4]',
    #'jetEta[0]',
    #'jetEta[1]',
    'lep1Pt',
    #'lep1Topoetcone20',
    #'lep1Ptvarcone20',
    #'lep1Ptvarcone30',
    'lep2Pt',
    #'lep2Topoetcone20',
    #'lep2Ptvarcone20',
    #'lep2Ptvarcone30',
    'nJet30',
    #'nJet20',
    #'nJet25',
    #'nTotalJet',
    'DPhiJ1Met',
    #'DPhiJ2Met',
    #'DPhiJ3Met',
    #'DPhiJ4Met',
    #'minDPhi4JetsMet',
    'minDPhiAllJetsMet',
    #'fabs(vectorSumJetsPhi-jetPhi[0])',
    #'vectorSumJetsPt',

    'METOverHTLep',
    'Rll',
    'mll',
    'Ptll',
    #'dPhiPllMet',

    'MTauTau',
    'nBJet20_MV2c10',
    #'lep1Pt/mll',
    #'lep2Pt/mll',
    'mt_lep1',
    #'mt_lep2',
    #'mt_lep1+mt_lep2',
    #'mt2leplsp_0',
    #'mt2leplsp_25',
    #'mt2leplsp_50',
    #'mt2leplsp_75',
    #'mt2leplsp_90',
    'mt2leplsp_100',
    #'mt2leplsp_110',
    #'mt2leplsp_120',
    #'mt2leplsp_150',
    #'mt2leplsp_200',
    #'mt2leplsp_250',
    #'mt2leplsp_300',
    #'lep1Pt/lep2Pt', 
    #'(lep1Pt+lep2Pt)/mll',
    'nVtx',
     #'mu',
    'RJR_RISR',
    #'RJR_MS',
    'minRll',
    #'ptWlep_minMll',
    ]
    my_vars = [
    'met_Et',
    'jetPt[0]',
    'lep1Pt',
    'lep2Pt',
    'nJet30',
    'nBJet20_MV2c10',
    'DPhiJ1Met',
    'minDPhiAllJetsMet',
    'METOverHT',
    'METOverHTLep',
    'met_Signif',
    #'met_Et*sqrt(METOverHT/met_Et)',
    'Rll',
    'mll',
    'MTauTau',
    'actual_mu',
    'mt_lep1',
    'mt2leplsp_100',
    'RJR_RISR',
    'RJR_MS',
    ]



    for reg in sig_regs:
        print('Making title frame for {0}'.format(reg))
	reg_lowmet = reg.replace('RJR','LowMET')
        frame_tex += mk_reg_frame(reg)
        for var in my_vars:
            print('Making frame for {0}, {1}'.format(reg, var))
            frame_tex += mk_frame(reg, reg_lowmet, var)

   	#print(frame_tex)	
   	output_name = 'frames.tex'

   	# Output file
   	with open('frames.tex', 'w') as f_frame:
   		f_frame.write(frame_tex)


   	print('Saving to {0}'.format(output_name))

#____________________________________________________________________________
def make_latex(var):

  # Mapping variable to LaTeX
  d_var_latex = {
    'nVtx'                : r'Number of vertices',
    'mu'                  : r'Interactions per crossing (avg) $\mu$',
    'actual_mu'           : r'Interactions per crossing (actual) $\mu$',
    'nLep_base'           : r'$N_\text{baseline}^\ell$',
    'nLep_signal'         : r'$N_\text{signal}^\ell$',
    'met_Et'              : r'$E_\text{T}^\text{miss}$',
    'met_Signif'          : r'$E_\text{T}^\text{miss}$ Significance',
    'nJet30'              : r'$N_\text{jets}^{30}$',
    'jetPt[0]'            : r'$p_\text{T}(j_1)$',
    'DPhiJ1Met'           : r'$\Delta\phi\left(j_1, \mathbf{p}_\text{T}^\text{miss}\right)$',
    'minDPhiAllJetsMet'   : r'min($\Delta\phi\left(\text{any jet}, \mathbf{p}_\text{T}^\text{miss}\right))$',
    'nBJet20_MV2c10'      : r'$N_\text{b-jets}^{20}$',
    'MTauTau' 		  : r'$m_{\tau\tau}$',
    'lep1Pt'              : r'$p_\text{T}^{\ell_1}$',
    'lep2Pt'              : r'$p_\text{T}^{\ell_2}$',
    'lep1Eta'             : r'$\eta^{\ell_1}$',
    'lep2Eta'             : r'$\eta^{\ell_2}$',
    'METOverHTLep'   	  : r'$E_\text{T}^\text{miss} / H_\text{T}^\text{lep}$',
    'METOverHT'   	  : r'$E_\text{T}^\text{miss} / H_\text{T}$',
    'Rll'                 : r'$\Delta R_{\ell\ell}$',
    'mt_lep1'             : r'$m_\text{T}^{\ell_1}$',
    'mll'                 : r'$m_{\ell\ell}$',
    'Ptll'                : r'$p^{\ell\ell}_\text{T}$',
    'mt2leplsp_100'       : r'$m_\text{T2}^{100}$',
    'RJR_RISR'            : r'$R_{ISR}$',
    'RJR_MS'              : r'$M_{T}^{S}$',
    'minRll'              : r'min$\Delta R_{\ell\ell}$',
    'ptWlep_minMll'       : r'$p_\text{T}^{\ell_W}$',
  }

  return d_var_latex[var]

#____________________________________________________________________________
def mk_reg_frame(reg):

    frame = r'''
%--------------------------------------
\begin{frame}
\centering
'''

    frame += r'''{\huge \mdseries '''
    frame += reg
    
    frame += r'''}
\end{frame}
%--------------------------------------

'''

    return frame

#____________________________________________________________________________
def mk_frame(reg, reg_lowmet, var):
    '''
    Return a beamer frame for the region and variable specified
    '''

    fig_1 = r'\includegraphics[scale=0.2]{NEWMERGE/140invfb/soft_muons/data_fakes/hist1d_'   + var + '_' + reg + '}'
    fig_2 = r'\includegraphics[scale=0.2]{NEWMERGE/140invfb/low_met/soft_muons/data_fakes/hist1d_' + var + '_' + reg_lowmet + '}'

    fig_4 = r'\includegraphics[scale=0.2]{NEWMERGE/80invfb/soft_muons/data_fakes/hist1d_'   + var + '_' + reg + '}'
    fig_5 = r'\includegraphics[scale=0.2]{NEWMERGE/2018/soft_muons/data_fakes/hist1d_' + var + '_' + reg + '}'

    #[width=1.1\textwidth]{

    col1_head = r'''
%--------------------------------------
\begin{frame}{
    \AddToShipoutPictureFG*{
    \AtPageUpperLeft{\put(-4,-22){\makebox[\paperwidth][r]{\color{white}\tiny \textbf{v2.7 Validation} \color{white}| Gonski \emph{and Compressed EWK team} | 18 December 2018}}}  
    }%
'''
	
    col1_head += make_latex(var) + ' | ' + reg
	
    col1_head += r'''}
\centering
\begin{columns}
%
\begin{column}{0.5\textwidth}  
\centering
\begin{figure}
	'''

    col1_taila = r'''
\setbeamerfont{caption name}{size=\tiny}
\vspace*{-4mm}\caption{\tiny 140.45 $fb^{-1}$, mc16a+d+e, MET > 200}
\end{figure} 
\end{column}
	'''

    col2_head = r'''
\begin{column}{0.5\textwidth}
\centering
\begin{figure}
	'''

    col2_taila = r'''
\setbeamerfont{caption name}{size=\tiny}
\vspace*{-4mm}\caption{\tiny 140.45 $fb^{-1}$, mc16a+d+e, 120 < MET <200 (LowMET)}
\end{figure} 
\end{column}
\end{columns}
'''


    colb1_head = r'''
\centering
\begin{columns}
%
\begin{column}{0.5\textwidth}  
\centering
\begin{figure}
	'''

    colb1_taila = r'''
\setbeamerfont{caption name}{size=\tiny}
\vspace*{-4mm}\caption{\tiny $80.5 fb^{-1}$, mc16a+d, MET > 200}
\end{figure} 
\end{column}
	'''

    colb2_head = r'''
\begin{column}{0.5\textwidth}
\centering
\begin{figure}
	'''

    colb2_taila = r'''
\setbeamerfont{caption name}{size=\tiny}
\vspace*{-4mm}\caption{\tiny $59.9 fb^{-1}$, mc16e,  MET > 200}
\end{figure} 
\end{column}
\end{columns}
\end{frame}

%--------------------------------------

'''


    frame_tex = col1_head + fig_1 + col1_taila + col2_head + fig_2 + col2_taila + colb1_head + fig_4 + colb1_taila + colb2_head + fig_5 + colb2_taila 

    return frame_tex


#_______________________________
if __name__ == "__main__":
  main()
