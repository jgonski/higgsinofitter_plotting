# Plotting scripts for SusySkimHiggsino ntuples

Here we have simple plotting scripts to plot 1D distributions in variables 
from flat ntuples produced from SusySkimHiggsino campaigns.
The primary goal is efficient physics validation of ntuple productions
and provide an independent check that inputs into HiggsinoFitter are consistent.
But, it has also been used for physics analysis design and scrutiny.

After configuring paths etc, the `plot.py` script is the main script to run:
```
./plot.py 
```
To see what options with flags, do
```
./plot.py --help
```
Also, you can open up `plot.py` and there are some global settings at the top of the script.

An example to plot m(ll) spectrum in CR-top-AF, we can do
```
./plot.py -s CR-top-AF -v mll
```
Another example is to plot the METOverHTLep in SRSF-iMLLg-SF 
with an arrow indicating the position of the cut
and without log scale on the y-axis
```
./plot.py -s SRSF-iMLLg-SF -v METOverHTLep --cutArrow --noLogY 
```

The available regions are specified in `cuts.py`, 
while the available variables to plot in are in `variables.py`.
The samples which are plotted are in `samples.py`.
By default, any region with SR in its name is blinded and an
N-1 plot with a significance scan in the lower panel is presented.

## Configuring samples to present in samples.py

In `samples.py`, we locations and presentational properties for each sample.

The `get_sample_paths()` function: 
* We place the paths to the background, data and signal `.root` files.
* We specify the suffixes of the background, data, fakes and signal `.root` files.
The assumption is that the name of the sample is e.g. `ttbar`.
By default we look in the `$HISTFITTER/../higgsino_samples` soft link
to where the SusySkimHiggsino samples are placed as the `TOPPATH`.

The `get_samples_to_plot()` function:
* Here we place lists of background and signal samples we wish to plot.

The `configure_samples()` function:
* Here we define custom colours to paint our samples.
* The `d_samp` dictionary contains various properties for each sample.
The name of the TTree is the dictionary key. 

## Configuring selections to apply in cuts.py

In `cuts.py`, we configure a dictionary mapping 
a region name e.g. 'CR-top-AF' into a cut string.
These cut strings are written in a python list,
which is joined with `&&` operator.
We write the selection as lists to enable easier 
construction of N-1 plots.

## Configuring variables to plot in variables.py

In `variables.py`, we configure
* Map to TLaTeX printed as the x axis title
* The x axis lower and upper edge
* The number of bins
* The units of the variable e.g. GeV for mll

# Batch submission

One can submit a parallelised production of the plots to a batch system from `/batch`.
By default, this is one plot made per job.

```
cd batch
./send_plotting_to_batch.py [--doTorque] [--doCondor]
```
You can choose whether to submit to torque or condor batch system using the flags.
Inside this `send_plotting_to_batch.py`, please choose which regions
and which variables to plot over. 

To make any modifications to the batch script, 
specifically what options to give `./plot.py` on the batch,
open `make_batch_scripts.py` inside the `plotting/` directory make the modifications desired. 
Then rerun
```
./make_batch_scripts.py
```
This creates the `torque_plot.sh` script which is submitted to torque batch systems.
This also creates the `condor_plot.sh` which is submitted by the `condor_plot.sub`
submission script to condor batch systems. 
By default, one combination of region and variable is sent as one job.


# Cutflow maker
Todo.
There is also `make_cutflows.py` script to make tables of cutflows.
Configure the cuts inside.
