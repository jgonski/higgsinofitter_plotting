#!/usr/bin/env python
'''
Welcome to HiggsinoFitter send_plotting_to_batch.py
  - Reads in list of regions to be submitted from test files
  - Submits batch systems to torque or condor system for HistFitting
  - Default output is $HISTFITTER/results

Example submission of background only fit to torque batch system (condor todo):
./send_plotting_to_batch.py # for Torque (PBS) submission

'''

import sys, os, time, argparse
HF_dir = os.environ['HISTFITTER']

#____________________________________________________________________________
def main():

  send_bkgOnly() 

#_________________________________________________________________________
def send_bkgOnly():
  '''
  Send the background only fit to batch
  ''' 

  # Regions as configured in the doPlots section of HiggsinoConfig.py
  l_regions = [
    'CRtop',
    'CRtau',
    'VRDF_iMLLg',
    'VRDF_iMT2f',
    'VR_VV',
    'VR_SS_EEME',
    'VR_SS_MMEM',
    'VR_SS_AF',
    'SRSF_iMLLg',
    'SRSF_iMT2f',
    'SRSF_common',
  ]
  
  # Map to a list of variables for each region
  # This is as configured in the doPlots section of HiggsinoConfig.py
  d_reg_vars = {
    'CRtop' :     ['mll', 'mt2leplsp_100', 'lep2Pt', 'METOverHTLep_METOverHTLep', 'nBJet20_MV2c10_nBJet20_MV2c10'],
    'CRtau' :     ['mll', 'mt2leplsp_100', 'MTauTau_MTauTau'],
    'VRDF_iMLLg' : ['mll', 'METOverHTLep_METOverHTLep', 'lep2Pt'],
    'VRDF_iMT2f' : ['mt2leplsp_100', 'METOverHTLep_METOverHTLep', 'lep2Pt'],
    'VR_VV' :       ['mll', 'mt2leplsp_100'],
    'VR_SS_EEME' :  ['mll', 'mt2leplsp_100', 'lep2Pt'],
    'VR_SS_MMEM' :  ['mll', 'mt2leplsp_100', 'lep2Pt'],
    'VR_SS_AF' :  ['mll', 'mt2leplsp_100', 'lep2Pt'],
    'SRSF_iMLLg' : ['mll', 'METOverHTLep_METOverHTLep'],
    'SRSF_iMT2f' : ['mt2leplsp_100', 'METOverHTLep_METOverHTLep'],
    'SRSF_common' : ['jetPt_0_', 
                     'lep1Pt',
                     'lep2Pt',
                     'met_Et',
                     'mt_lep1',
                     'Rll',
                     'METOverHTLep',
                     'nBJet20_MV2c10_nBJet20_MV2c10',
                     'DPhiJ1Met_DPhiJ1Met',
                     'MTauTau_MTauTau',
                     'minDPhiAllJetsMet_minDPhiAllJetsMet',
                     ],
    
  }
  
  # We perform 3 different background-only fits and want to see how they compare 
  l_bkg_fit_types = [
    'bkg',
    'bkg_doCRplusSRMLL',
    'bkg_doCRplusSRMT2'
  ]
  

  for bkg_fit_type in l_bkg_fit_types:
    print( 'Considering background fit type: {0}'.format(bkg_fit_type) )
    mkdir( '../save_plots/{0}'.format(bkg_fit_type) )
    
    for region in l_regions:
      for var in d_reg_vars[region]:
    
        bkg_dir = 'Higgsino_{0}_{1}'.format(bkg_fit_type, region)
        sig_dir = 'Higgsino_sig_' + region
        

        # "plot_name" refers to "region" in HistPlotter
        my_region = region 
        plot_name = my_region + '_' + var
        print('Plotting {0}'.format(plot_name)) 
        
        # reg_name is the region name printed in the plot
        reg_name = region.replace('_', '-')
        if region == 'CRtop':
          reg_name = 'CR-top'
        if region == 'CRtau':
          reg_name = 'CR-tau'
        if 'VR_SS_EEME' in region:
          reg_name = 'VR-SS ee+#mue'
        elif 'VR_SS_MMEM' in region:
          reg_name = 'VR-SS #mu#mu+e#mu'
        elif 'VR_SS_AF' in region:
          reg_name = 'VR-SS ee+#mu#mu+e#mu+#mue'
        elif 'VRDF_iMLLg' in region:
          reg_name = 'VRDF-m_{#font[12]{ll}}' 
        elif 'VRDF_iMT2f' in region:
          reg_name = 'VRDF-m_{T2}^{100}' 
        elif 'SRSF_iMLLg' in region:
          reg_name = 'SR#font[12]{ll}-m_{#font[12]{ll}}' 
        elif 'SRSF_iMT2f' in region:
          reg_name = 'SR#font[12]{ll}-m_{T2}^{100}' 
        elif 'SRSF_common' in region:
          reg_name = 'Common SR' 
           
       
        if 'bkg' in bkg_fit_type:
          #fit_txt = 'CRs-only fit'
          fit_txt = ''
        if 'bkg_doCRplusSRMLL' in bkg_fit_type:
          fit_txt = 'Exclusion fit, #mu_{signal} = 0'
        if 'bkg_doCRplusSRMT2' in bkg_fit_type:
          fit_txt = 'Exclusion fit, #mu_{signal} = 0'
        
        
        extraFlags = ''
        if 'nBJet' in var:
          #extraFlags = ' --noLogY -mainYMax 28'
          extraFlags += ' --integerBins'
        if ('METOverHTLep' in var and 'SRSF_iMLLg' not in region):
          extraFlags += ' -units "NoUnitsUnity"'
        if ('METOverHTLep' in var and 'SRSF_iMLLg' in region) or 'Phi' in var or 'Rll' in var:
          extraFlags += ' -units "NoUnits"'
        if 'SRSF_common' in region:
          if 'lep1Pt' not in var or 'lep2Pt' not in var or 'met_Et' not in var:
            extraFlags += ' --increaseY' 
              
        script_name = 'batch_torque_plots.sh'
        TorqueTime = '01:59:00'
        cmd   = "qsub -l walltime={0} -l cput={0} -v bkg_dir='{1}',sig_dir='{2}',reg='{3}',regName='\"{4}\"',extraFlags='{5}',hf_dir='{6}',bkg_fit_type='{7}',fit_txt='\"{8}\"' {9}".format(TorqueTime, bkg_dir, sig_dir, plot_name, reg_name, extraFlags, HF_dir, bkg_fit_type, fit_txt, script_name)
        print(cmd)
        os.system(cmd)

      #./output.o -name $bkg_dir -sigName $sig_dir -dir ../../results -save save_plots -region $reg -regionName $regName $extraFlags 
 
  '''
  # Todo: condor submission
  # Condor multiple submission per signal handled in condor submission file
  if doCondor:
    script_name = 'condor_bkg_fit.sub'
    cmd = "cd batch/condor ; condor_submit {0}".format(script_name)
    print(cmd)
    if not no_submit:
      os.system(cmd)     
  '''


#_________________________________________________________________________
def mkdir(dirPath):
  '''
  make directory for given input path
  '''
  try:
    os.makedirs(dirPath)
    print 'Successfully made new directory ' + dirPath
  except OSError:
    pass


#_______________________________
if __name__ == "__main__":
  main()
