#!/bin/bash
#PBS -N "plot_"$bkg_dir$reg
cd $hf_dir
echo $hf_dir
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh ; lsetup "root 6.06.06-x86_64-slc6-gcc49-opt" ; source setup.sh ; cd ..
cd plotting/HistPlotter
#mkdir save_plots
echo $PWD
#ls
echo 'Available regions to plot:'
./output.o -name $bkg_dir -sigName $sig_dir -dir ../../results -save save_plots/$bkg_fit_type -region $reg -regionName "$regName" --printRegionsOnly
echo '-------------------------------------------'
echo 'Proceeding to plot'
./output.o -name $bkg_dir -sigName $sig_dir -dir ../../results -save save_plots/$bkg_fit_type -region $reg -regionName "$regName" -fitTypeText "$fit_txt" $extraFlags
#--noLogY -mainYMax 26
