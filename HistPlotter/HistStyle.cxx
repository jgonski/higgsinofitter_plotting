#include "HistStyle.h"
#include "TStyle.h"


HistStyle::HistStyle()
{
 
}
// ------------------------------------------------------------------------------------- //
void HistStyle::init()
{
  
  TStyle *atlasStyle = new TStyle("ATLAS","Atlas style");

  // use plain black on white colors
  Int_t icol=0; // WHITE
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetFrameFillColor(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetStatColor(icol);
  //atlasStyle->SetFillColor(icol); // don't use: white fill color for *all* objects

  // set the paper & margin sizes
  atlasStyle->SetPaperSize(20,26);

  // set margin sizes
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05);
  atlasStyle->SetPadBottomMargin(0.25);
  atlasStyle->SetPadLeftMargin(0.18);

  // set title offsets (for axis label)
  atlasStyle->SetTitleXOffset(1.6);
  atlasStyle->SetTitleYOffset(2.5);

  // use large fonts
  //Int_t font=72; // Helvetica italics
  Int_t font=42; // Helvetica
  Double_t tsize=0.15;
  atlasStyle->SetTextFont(font);

  atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");
  
  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");

  // use bold lines and markers
  atlasStyle->SetMarkerStyle(20);
  atlasStyle->SetMarkerSize(1.2);
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars 
  //atlasStyle->SetErrorX(0.001);
  // get rid of error bar caps
  atlasStyle->SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations
  atlasStyle->SetOptTitle(0);
  //atlasStyle->SetOptStat(1111);
  atlasStyle->SetOptStat(0);
  //atlasStyle->SetOptFit(1111);
  atlasStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();

}
// ------------------------------------------------------------------------------------- //
TCanvas* HistStyle::getCanvas()
{

  TCanvas* c1 = new TCanvas("c1","c1",685,600);
  return c1;
}
// ------------------------------------------------------------------------------------- //
TLegend* HistStyle::getLegend(Double_t x1, Double_t y1, Double_t x2, Double_t y2, Double_t size)
{

  TLegend* leg = new TLegend(x1,y1,x2,y2);
  leg->SetFillStyle(0);
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  leg->SetTextSize(size);
  leg->SetTextFont(42);

  return leg;
}
// ------------------------------------------------------------------------------------- //
TPad* HistStyle::getMainTPad()
{

  TPad *pad1 = new TPad("mainPad","mainPad",0.,0.3615,.99,1);
  pad1->SetBottomMargin(0.005);
  pad1->SetFillColor(kWhite);
  pad1->SetTickx();
  pad1->SetTicky();
    
  return pad1;
}
// ------------------------------------------------------------------------------------- //
TPad* HistStyle::getRatioTPad()
{

  TPad *pad2 = new TPad("ratioPad","ratioPad",0.,0.01,.99,0.35);
  pad2->SetTopMargin(0.005);
  pad2->SetBottomMargin(0.40);
  pad2->SetFillColor(kWhite); 

  return pad2;

}
// ------------------------------------------------------------------------------------- //
void HistStyle::setHistAxes(TAxis*& x_axis, TAxis*& y_axis,TString x_axis_label)
{

  // This can go....

  // Y-axis properties
  y_axis->SetTitle("Data/SM");
  //y_axis->SetLabelSize(0.15);
  y_axis->SetTitleOffset(0.35);
  y_axis->SetLabelOffset(0.02);
  y_axis->SetNdivisions(504);         
  y_axis->SetLabelSize(0.50);
  y_axis->SetTitleSize(0.10);
  y_axis->CenterTitle(); 

  // X-axis properties
  x_axis->SetTitleSize(0.14);
  x_axis->SetTitleOffset(0.30);
  x_axis->SetTitleOffset(1.);
  x_axis->SetLabelOffset(0.03);
  x_axis->SetTickLength(0.06);
  x_axis->SetTitle(x_axis_label);

}
// ------------------------------------------------------------------------------------- //
void HistStyle::SetMainPadStyle(RooPlot* frame)
{

  frame->SetTitle("");
  frame->GetXaxis()->SetLabelSize(0.00);
  frame->GetYaxis()->SetLabelSize(0.07);
  frame->GetYaxis()->SetTitleSize(0.08);
  frame->GetYaxis()->SetTitleOffset(1.11);
  frame->GetYaxis()->SetLabelOffset(0.015);
  
  
}
// ------------------------------------------------------------------------------------- //
void HistStyle::SetRatioPadStyle(RooPlot* frame,TString x_axis_label)
{

  // Y-axis properties
  frame->GetYaxis()->SetTitle("Data / SM");
  frame->GetYaxis()->SetTitleOffset(0.60);
  frame->GetYaxis()->SetNdivisions(303);         
  frame->GetYaxis()->SetLabelOffset(0.02);
  frame->GetYaxis()->SetLabelSize(0.14);
  frame->GetYaxis()->SetTitleSize(0.14);
  frame->GetYaxis()->CenterTitle(); 

  // X-axis properties
  frame->GetXaxis()->SetTitleSize(0.15);
  frame->GetXaxis()->SetTitleOffset(1.25);
  frame->GetXaxis()->SetLabelOffset(0.04);
  frame->GetXaxis()->SetLabelSize(0.14);
  frame->GetXaxis()->SetTickLength(0.06);
  frame->GetXaxis()->SetTitle(x_axis_label);

  frame->SetTitle("");
  frame->SetMinimum(0.0);
  frame->SetMaximum(2.0);

}
// ------------------------------------------------------------------------------------- //
HistStyle::~HistStyle()
{
}
