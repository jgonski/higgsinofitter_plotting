#ifndef HISTPLOTTER_H
#define HISTPLOTTER_H

/*
 *
 * Note that a lot of the methods and functions in this 
 * class are taken directly from HistFitter:
 *     => http://histfitter.web.cern.ch/histfitter/
 *
 * This class only acts as a wraper around these functions in a configurable way
 */


// Plotting packages
#include "HistStyle.h"

// C++
#include <vector>
#include <iostream>
#include <map>

// Root
#include "TString.h"
#include "TFile.h"
#include "TIterator.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TPad.h"
#include "TLatex.h"
#include "TAxis.h"
#include "TF1.h"
#include "TH1.h"
#include "TColor.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TArrow.h"

// Roofit

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif

#include "RooWorkspace.h"
#include "RooFitResult.h"
#include "RooSimultaneous.h"
#include "RooAbsData.h"
#include "RooArgSet.h"
#include "RooAbsArg.h"
#include "RooCatType.h"
#include "RooCategory.h"
#include "RooAbsData.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooCurve.h"
#include "RooRealVar.h"
#include "RooDataHist.h"
#include "RooRealSumPdf.h"
#include "RooProduct.h"
#include "RooDataSet.h"



using namespace std;
using namespace RooFit;

struct PlotInfo
{
  TString name;
  TString histDir;
  TString signalDir;
  TString saveDir;
  TString regions;
  TString rootFileName;
  TString workSpaceName;
};

struct ArrowInfo
{
  std::vector<TString> name;
  std::vector<TString> text;
  std::vector<TString> type;   // upper,lower,or box
  std::vector<float>   height;
  std::vector<float>   lowerCut;
  std::vector<float>   lowerCutWidth;
  std::vector<float>   upperCut;
};

class HistPlotter: public HistStyle
{

 private:
  vector<PlotInfo*>   m_plots;             
  TFile*              m_HEPDATA;

 public:
  HistPlotter();
  ~HistPlotter();

  void Process();
  void init();

  void addPlots(TString histDir,
                TString name,
                TString sigName,
		TString saveDir,
		TString regions,
                TString rootFileName,
		TString workSpaceName);

  void addProcess(TString process,TString legendName,Int_t color, int nLeg=1);
  void addSigProcess(TString process,TString legendName,Int_t color);


  void makeCentralPlot(RooDataSet* regionData,RooAbsPdf* regionPdf, RooRealVar* regionVar,
		       RooWorkspace* w,RooFitResult* rFit,TString regionCatLabel,TPad*& pad,TString signalDir);

  void AddSignalToPlot(RooPlot*& frame,RooRealVar* obsRegion,TString regionCatLabel,TLegend* leg,TString signalDir);

  void makeRatioPlot(RooAbsData* data,RooDataSet* regionData,RooAbsPdf* regionPdf,RooRealVar* regionVar,
		     RooWorkspace* w,RooFitResult* rFit,TString dataCatLabel,TString regionCatLabel);

  RooCurve* MakePdfErrorRatioHist(RooWorkspace* w, RooDataSet* regionData, RooAbsPdf* regionPdf, 
				  RooRealVar* regionVar, RooFitResult* rFit, Double_t Nsigma=1.0);

  RooHist* makeRatioHist(RooDataSet* regionData,/*RooAbsData* data,*/ RooAbsPdf* regionPdf,RooRealVar* regionVar,
			 TString dataCatLabel);


  void doHEPData(RooDataSet* regionData,RooAbsPdf* regionPdf,RooFitResult* rFit,RooRealVar* regionVar,TString regionCatLabel);

  // Plotting functions
  void AddComponentsToPlot(RooWorkspace* w, RooPlot* frame,RooAbsPdf* regionPdf, 
			   RooRealVar* obsRegion,TString regionCatLabel,
			   TLegend* leg1,TLegend* leg2,bool stack=true);

  Double_t GetComponentFrac(RooWorkspace* w, const char* Component, const char* RRSPdfName, 
			    RooRealVar* observable, RooRealVar* binWidth);

  void RemoveEmptyDataBins(RooWorkspace* w, RooPlot* frame);
  void SetFrameStyle(RooPlot*& frame,TPad*& pad,TString region);
  void AddText(TString myText, bool NDC,double xMin, double yMin, double textSize=0.045,int textFont=42);

  // Helper functions
  RooWorkspace* getWorkspace(TString rootFileName,TString workSpaceName="w");
  void addToLegend(RooPlot* frame,TLegend* leg,TString name,bool add_map_only=true,bool is_curve=true,bool totalSM=false,bool signal=false);
  TString getXaxisLabel(TString regionCatLabel);
  TString getRegionName(TString regionCatLabel);
  void reverseLegendMap();
 
  void addRegionToCanvas(RooPlot* frame,RooRealVar* regionVar,TString regionCatLabel);
  vector<TString> getRegions(TString regions,RooCategory* cat);

  // Misc functions
  void clear(){m_plots.clear();}
  void setLogY(bool logY){m_logY=logY;}
  void setIncreaseY(bool increaseY){m_increaseY=increaseY;}
  void setHistStatus(TString histStatus){m_histStatus=histStatus;}
  void showRegions(bool showRegions){m_showRegions=showRegions;}

  void addRegionToPlot(TString region);


  // Map functions
  void addXaxisLabelToMap(TString oldLabel,TString newLabel);
  void addRegionToMap(TString oldLabel,TString newLabel);
  void addArrowToCanvas(TString name,TString text,TString type,float height,float lowerCut,float lowerCutWidth,float upperCut=-1);
  void addDataCut(TString regionName,TString cut);

  void groupProcessInMap(int nLeg,TString process);
  int getNLegend(TString process);

  TString getDataCut(TString region);
 
  // Scales
  void setRatioYMax(double ratio_yMax){m_ratio_yMax=ratio_yMax;}
  void setRatioYMin(double ratio_yMin){m_ratio_yMin=ratio_yMin;}
  void setMainYMax(double main_yMax){m_main_yMax=main_yMax;}
  void setMainYMin(double main_yMin){m_main_yMin=main_yMin;}

  // Misc
  void printRegionsOnly(bool printRegionsOnly){m_printRegionsOnly=printRegionsOnly;}
  void integerBins(bool integerBins){m_integerBins=integerBins;}
  void setRegionName(TString regionName){m_regionName=regionName;}
  void setFitTypeText(TString fitTypeText){m_fitTypeText=fitTypeText;}

  void setErrorRange(float min,float max){
    m_minRange=min;
    m_maxRange=max;
  }

  void setUnits(TString units){m_units=units;};
 protected:

  HistStyle* m_histStyle;

  bool m_removeEmptyBins;
  bool m_showRegions;
  bool m_logY;
  bool m_increaseY;
  bool m_showRegionName;

  double m_ratio_yMax;
  double m_ratio_yMin;
  double m_main_yMax;
  double m_main_yMin;

  float m_minRange;
  float m_maxRange;

  TString m_histStatus;
  TString m_units;
  TString m_regionsToPlot;
  TString m_regionName;
  TString m_fitTypeText;
  bool  m_printRegionsOnly;
  bool  m_integerBins;

  std::map<TString,Int_t>        m_colorMap;
  std::map<TString,Int_t>        m_colorMapSig;
  std::map<TString,TString>      m_legendMap;
  std::map<TString,TString>      m_legendMapSig;
  std::map<TString,TString>      m_regionMap;
  std::map<TString,TString>      m_xAxisMap;
  std::map<TString,TString>      m_dataCuts;
  std::map<TString,ArrowInfo*>   m_arrowMap;
  std::map<TString,int>          m_legendGrouping;


};


#endif
