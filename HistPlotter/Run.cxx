#include "HistPlotter.h"
int main(int argc, char** argv)
{

  TString name;
  TString sigName;
  TString dir;
  TString save;
  TString regionName = "";
  TString fitTypeText = "";
  TString region = "";
  TString units = "GeV";
  double ratioYmin = -1.0;
  double ratioYmax = -1.0;
  double mainYmin = -1.0;
  double mainYmax = -1.0;

  float min = 0;
  float max = 0;

  bool printRegionsOnly=false;
  bool integerBins=false;
  bool logY=true;
  bool increaseY=false;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-name") == 0){
      name = argv[++i];
    }
    else if (strcmp(argv[i], "-sigName") == 0){
      sigName = argv[++i];
    }
    else if (strcmp(argv[i], "-dir") == 0){
      dir = argv[++i];
    }
    else if (strcmp(argv[i], "-save") == 0){
      save = argv[++i];
    }
    else if (strcmp(argv[i], "-region") == 0){
      region = argv[++i];
    }
    else if (strcmp(argv[i], "-units") == 0){
      units = argv[++i];
    }
   else if (strcmp(argv[i], "-regionName") == 0){
      regionName = argv[++i];
    }
   else if (strcmp(argv[i], "-fitTypeText") == 0){
      fitTypeText = argv[++i];
    }
    else if (strcmp(argv[i], "-ratioYMin") == 0){
      ratioYmin = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-ratioYMax") == 0){
      ratioYmax = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-mainYMin") == 0){
      mainYmin = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-mainYMax") == 0){
      mainYmax = atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-minRange") == 0){
      min= atof(argv[++i]);
    }
    else if (strcmp(argv[i], "-maxRange") == 0){
      max= atof(argv[++i]);
    }
    else if (strcmp(argv[i], "--printRegionsOnly") == 0){
      printRegionsOnly=true; 
    }
    else if (strcmp(argv[i], "--integerBins") == 0){
      integerBins=true;
    }
    else if (strcmp(argv[i], "--noLogY") == 0){
      logY=false;
    }
    else if (strcmp(argv[i], "--increaseY") == 0){
      increaseY=true;
    }

    else{
      cout << "Error: invalid argument!" << endl;
      exit(0);
    }
  }

  cout << "Name:   " << name << endl;
  cout << "Save:   " << save << endl;
  cout << "Fit type text:   " << fitTypeText << endl;

  HistPlotter* plotter = new HistPlotter();

  plotter->printRegionsOnly(printRegionsOnly);
  plotter->integerBins(integerBins);
  plotter->setLogY(logY);
  plotter->setIncreaseY(increaseY);
  plotter->setRegionName(regionName);
  plotter->setFitTypeText(fitTypeText);
  //plotter->setHistStatus("Internal");
  //plotter->setHistStatus("Preliminary");
  plotter->setHistStatus(" ");
  plotter->setRatioYMin(ratioYmin);
  plotter->setRatioYMax(ratioYmax);
  plotter->setMainYMin(mainYmin);
  plotter->setMainYMax(mainYmax);
  plotter->setErrorRange(min,max);

  TColor *my_color = new TColor();

  int myLightBlue    = my_color->GetColor("#9ecae1");
  int myMediumBlue   = my_color->GetColor("#6baed6");
  
  int myMediumGreen  = my_color->GetColor("#41ab5d");
  
  int myLighterOrange = my_color->GetColor("#ffeda0");
  int myMediumOrange  = my_color->GetColor("#fe9929");
  
  int myLighterGrey = my_color->GetColor("#e3e3e3");

  // These processes need to be specified to be drawn
  plotter->addProcess("fakes",              "Fake/nonprompt",              myLighterGrey,  1);
  plotter->addProcess("topoverallSyst",     "t#bar{t}, single top",        myLightBlue,    2);
  plotter->addProcess("ZttjetsoverallSyst", "Z(#rightarrow#tau#tau)+jets", myMediumGreen,  2);
  plotter->addProcess("dibosonoverallSyst", "Diboson",                     myMediumOrange, 2);
  plotter->addProcess("otheroverallSyst",   "Others",                      myLighterOrange,2);

  
  // Overlay the signal
  std::cout << "sigName: " << sigName << std::endl;
  if(sigName.Contains("SR") && sigName.Contains("MT2")){
    std::cout << "Putting slepton signals" << std::endl;
    plotter->addSigProcess("Slepton_105_100", "#tilde{#font[12]{l}}: m(#tilde{#font[12]{l}}, #tilde{#chi}^{0}_{1}) = (105, 100) GeV",kRed+1); // 
    plotter->addSigProcess("Slepton_110_100", "#tilde{#font[12]{l}}: m(#tilde{#font[12]{l}}, #tilde{#chi}^{0}_{1}) = (110, 100) GeV",kViolet+3); // 
  }
  if(sigName.Contains("SR") && sigName.Contains("MLL")){
    std::cout << "Putting Higgsino signals" << std::endl;
    plotter->addSigProcess("Higgsino_105_100","#tilde{H}: m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{0}_{1}) = (105, 100) GeV",kRed+1); //
    plotter->addSigProcess("Higgsino_110_100","#tilde{H}: m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{0}_{1}) = (110, 100) GeV",kViolet+3); //
  }
  if(sigName.Contains("SR") && sigName.Contains("common")){
    std::cout << "Putting a Higgsino and slepton signal" << std::endl;
    plotter->addSigProcess("Slepton_110_100", "#tilde{#font[12]{l}}: m(#tilde{#font[12]{l}}, #tilde{#chi}^{0}_{1}) = (110, 100) GeV",kRed+1); //
    plotter->addSigProcess("Higgsino_110_100","#tilde{H}: m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{0}_{1}) = (110, 100) GeV",kViolet+3); //
  }
  
  //plotter->addXaxisLabelToMap("mct","m_{CT} [GeV]");
  plotter->addXaxisLabelToMap("mt_lep1","m_{T}(#font[12]{l}_{1}) [GeV]");
  plotter->addXaxisLabelToMap("mll","m_{#font[12]{ll}} [GeV]");
  plotter->addXaxisLabelToMap("Rll","#DeltaR_{#font[12]{ll}}");
  plotter->addXaxisLabelToMap("lep1Pt","Leading lepton p_{T} [GeV]");
  plotter->addXaxisLabelToMap("lep2Pt","Subleading lepton p_{T} [GeV]");
  plotter->addXaxisLabelToMap("mt2leplsp_100","m_{T2}^{100} [GeV]");
  plotter->addXaxisLabelToMap("METOverHTLep","E_{T}^{miss} / H_{T}^{lep}");
  plotter->addXaxisLabelToMap("MTauTau","m_{#tau#tau} [GeV]");
  plotter->addXaxisLabelToMap("met_Et","E_{T}^{miss} [GeV]");
  plotter->addXaxisLabelToMap("nBJet20_MV2c10","Number of b-tagged jets");
  plotter->addXaxisLabelToMap("DPhiJ1Met","#Delta#phi(j_{1}, #bf{p}_{T}^{miss})");
  plotter->addXaxisLabelToMap("minDPhiAllJetsMet","min(#Delta#phi(any jet, #bf{p}_{T}^{miss}))");

  plotter->addXaxisLabelToMap("jet1Pt","Leading jet p_{T} [GeV]");
  plotter->addXaxisLabelToMap("jet2Pt","Sub-leading jet p_{T} [GeV]");
  plotter->addXaxisLabelToMap("nJet30","Number of jets (p_{T}>30 GeV)");


  // Arrows showing where cut in the N-1 variable will be placed
  plotter->addArrowToCanvas("SRSF_iMLLg_METOverHTLep_METOverHTLep","SRSF_iMLLg","lower",50,5,8);
  plotter->addArrowToCanvas("SRSF_iMT2f_METOverHTLep_METOverHTLep","SRSF_iMT2f","lower",120,3,6);
  plotter->addArrowToCanvas("VRDF_iMLLg_METOverHTLep_METOverHTLep","VRDF_iMLLg","lower",20,5,8);
  plotter->addArrowToCanvas("VRDF_iMT2f_METOverHTLep_METOverHTLep","VRDF_iMT2f","lower",120,3,6);
  plotter->addArrowToCanvas("CRtop_METOverHTLep_METOverHTLep","CRtop","lower",800,5,8);
  plotter->addArrowToCanvas("CRtop_nBJet20_MV2c10_nBJet20_MV2c10","CRtop","lower",1100,0.5,1.5);
  plotter->addArrowToCanvas("CRtau_MTauTau_MTauTau","CRtau","lower",100,60,80);
  plotter->addArrowToCanvas("CRtau_MTauTau_MTauTau","CRtau","upper",100,120,100);
  
  plotter->addArrowToCanvas("SRSF_common_MTauTau_MTauTau","SRSF_common","upper",108,160,400);
  plotter->addArrowToCanvas("SRSF_common_MTauTau_MTauTau","SRSF_common","lower",108,0,-200);
  plotter->addArrowToCanvas("SRSF_common_nBJet20_MV2c10_nBJet20_MV2c10","SRSF_common","lower",3000,0.5,-0.2);
  plotter->addArrowToCanvas("SRSF_common_DPhiJ1Met_DPhiJ1Met","SRSF_common","lower",100,2.0,2.5);
  plotter->addArrowToCanvas("SRSF_common_minDPhiAllJetsMet_minDPhiAllJetsMet","SRSF_common","lower",100,0.4,0.8);

  plotter->addRegionToPlot(region);
  plotter->setUnits(units);

  plotter->addPlots(dir,name,sigName,save,"ALL","BkgOnly_combined_NormalMeasurement_model_afterFit.root","w");
  plotter->Process();

  return 0;
}
