'''

Welcome to variables.py

Here we configure how the variables are 
binned and labelled in the ROOT plots
using a dictionary d_vars = {}

'''

#____________________________________________________________________________
def configure_vars():
  
   
  # ---------------------------------------------
  #
  # Format for each variable entry
  #
  # 'variable name as in TTree ntuple of SusySkimHiggsino' 
  #    : {
  #       'tlatex'  : ROOT TLaTeX axis entry to display in plot
  #       'units'   : Units of variable to print on plot
  #       'hXNbins' : Number of bins
  #       'hXmin',  : Lower x axis edge
  #       'hXmax'   : Upper x axis edge 
  #       'cut_pos' : Arrow position (for N-1 plots)
  #       'cut_dir' : Cut direction for N-1 Zn significance scan and arrow direction
  #      }
  #
  # To do variable bin widths, place 'var' as value of 'hXNbins' 
  # and specify lower bin edges as 'binsLowE':[0,4,5,11,15,20,40,60]
  # e.g. 
  # 'lep2Pt':{'tlatex':'p_{T}(#font[12]{l}_{2})','units':'GeV','hXNbins':'var','hXmin':0,'hXmax':60,'binsLowE':[0,4,5,11,15,20,40,60],'cut_pos':200,'cut_dir':'left'}, 
  #
  # ---------------------------------------------
  
  # ---------------------------------------------
  #
  # Contents
  #
  # Jets & MET
  #  - MET variables
  #  - Jet multiplicities
  #  - b-tagged multiplicities
  #  - Jet kinematics
  #  - Jets and MET
  #  - Recursive jigsaw reconstruction
  #
  # Leptons 
  #  - multiplicities
  #  - kinematics
  #  - MCTruthClassifer
  #  - impact parameter variables
  #  - Lepton isolation cone variables
  #  - Higher level lepton variables
  #  - Lepton variables involving MET
  #  - mT2 by various trial invisible masses
  #
  # Pileup 
  #
  # ---------------------------------------------

  d_vars = {
    
   
    # -------------------//------------------------
   
    
    # ---------------------------------------------
    #
    # Jets & MET
    #
    # ---------------------------------------------

    # ---------------------------------------------
    # MET variables
    # ---------------------------------------------
    #'met_Et'       :{'tlatex':'E_{T}^{miss}','units':'GeV','hXNbins':30,'hXmin':0,'hXmax':600,'cut_pos':200,'cut_dir':'right'},
    #'met_Et'       :{'tlatex':'E_{T}^{miss}','units':'GeV','hXNbins':50,'hXmin':0,'hXmax':1000,'cut_pos':200,'cut_dir':'right'},
    #'met_Et'       :{'tlatex':'E_{T}^{miss}','units':'GeV','hXNbins':'var','hXmin':0,'hXmax':1000,'binsLowE':[0,200,250,300,350,400,450,500,600,700,800,900,1000],'cut_pos':200,'cut_dir':'right'},
    #'met_Et'       :{'tlatex':'E_{T}^{miss}',          'units':'GeV','hXNbins':20,'hXmin':0,'hXmax':1000,'cut_pos':200,'cut_dir':'right'},
    'met_Et'       :{'tlatex':'E_{T}^{miss}','units':'GeV','hXNbins':'var','hXmin':0,'hXmax':500,'binsLowE':[0,100,120,140,160,180,200,220,240,260,280,300,350,400,450,500],'cut_pos':200,'cut_dir':'right'},
    'met_Signif'   :{'tlatex':'E_{T}^{miss} Significance','units':'GeV','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':200,'cut_dir':'right'},
    'METRel'       :{'tlatex':'E_{T}^{miss,rel}',      'units':'GeV','hXNbins':30,'hXmin':0,'hXmax':600, 'cut_pos':900,'cut_dir':'right'},
    'TST_Et'       :{'tlatex':'TracksofttermE_{T}',    'units':'GeV','hXNbins':20,'hXmin':0,'hXmax':100, 'cut_pos':200,'cut_dir':'right'},
    'met_track_Et' :{'tlatex':'Track E_{T}^{miss}',    'units':'GeV','hXNbins':15,'hXmin':0,'hXmax':300, 'cut_pos':900,'cut_dir':'right'},
    'METTrackRel'  :{'tlatex':'Track E_{T}^{miss,rel}','units':'GeV','hXNbins':30,'hXmin':0,'hXmax':600, 'cut_pos':900,'cut_dir':'right'},
    'dPhiMetAndMetTrack':{'tlatex':'|#Delta#phi(#bf{p}_{T}^{miss},Track#bf{p}_{T}^{miss})|','units':'','hXNbins':20,'hXmin':0,'hXmax':4,'cut_pos':200,'cut_dir':'left'},
    'fabs(dPhiMetAndMetTrack)':{'tlatex':'|#Delta#phi(#bf{p}_{T}^{miss}, Track #bf{p}_{T}^{miss})|','units':'','hXNbins':25,'hXmin':0,'hXmax':4,'cut_pos':200,'cut_dir':'left'},
    
    # ---------------------------------------------
    # Jet multiplicity
    # ---------------------------------------------
    'nJet30'     :{'tlatex':'N_{|#eta|<2.8Jets}^{pT>30GeV}','units':'','hXNbins':10,'hXmin':0,'hXmax':10,'cut_pos':15,'cut_dir':'left'},
    'nJet25'     :{'tlatex':'N_{|#eta|<2.8Jets}^{pT>25GeV}','units':'','hXNbins':10,'hXmin':0,'hXmax':10,'cut_pos':15,'cut_dir':'left'},
    'nJet20'     :{'tlatex':'N_{|#eta|<2.8Jets}^{pT>20GeV}','units':'','hXNbins':10,'hXmin':0,'hXmax':10,'cut_pos':15,'cut_dir':'left'},
    'nTotalJet'  :{'tlatex':'N_{|#eta|<4.5Jets}^{pT>25GeV}','units':'','hXNbins':10,'hXmin':0,'hXmax':10,'cut_pos':15,'cut_dir':'left'},
    'nTotalJet20':{'tlatex':'N_{|#eta|<4.5Jets}^{pT>20GeV}','units':'','hXNbins':10,'hXmin':0,'hXmax':10,'cut_pos':15,'cut_dir':'left'},

    # ---------------------------------------------
    # b-tagged multiplicity
    # ---------------------------------------------
    'nBJet20_MV2c10'                 :{'tlatex':'N_{b-jet}^{pT>20GeV}, MV2c10 FixedCutBEff 85%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet30_MV2c10'                 :{'tlatex':'N_{b-jet}^{pT>30GeV}, MV2c10 FixedCutBEff 85%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet20_MV2c10_FixedCutBEff_77' :{'tlatex':'N_{b-jet}^{pT>20GeV}, MV2c10 FixedCutBEff 77%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet30_MV2c10_FixedCutBEff_77' :{'tlatex':'N_{b-jet}^{pT>30GeV}, MV2c10 FixedCutBEff 77%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet20_MV2c10rnn_FixedCutBEff_85' :{'tlatex':'N_{b-jet}^{pT>20GeV}, MV2c10 RNN FixedCutBEff 85%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet30_MV2c10rnn_FixedCutBEff_85' :{'tlatex':'N_{b-jet}^{pT>30GeV}, MV2c10 RNN FixedCutBEff 85%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet20_DL1_FixedCutBEff_77'    :{'tlatex':'N_{b-jet}^{pT>20GeV}, DL1 FixedCutBEff 77%',    'units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet30_DL1_FixedCutBEff_77'    :{'tlatex':'N_{b-jet}^{pT>30GeV}, DL1 FixedCutBEff 77%',    'units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet20_DL1_FixedCutBEff_85'    :{'tlatex':'N_{b-jet}^{pT>20GeV}, DL1 FixedCutBEff 85%',    'units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet30_DL1_FixedCutBEff_85'    :{'tlatex':'N_{b-jet}^{pT>30GeV}, DL1 FixedCutBEff 85%',    'units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet20_DL1rnn_FixedCutBEff_77' :{'tlatex':'N_{b-jet}^{pT>20GeV}, DL1 RNN FixedCutBEff 77%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet30_DL1rnn_FixedCutBEff_77' :{'tlatex':'N_{b-jet}^{pT>30GeV}, DL1 RNN FixedCutBEff 77%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet20_DL1rnn_FixedCutBEff_85' :{'tlatex':'N_{b-jet}^{pT>20GeV}, DL1 RNN FixedCutBEff 85%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    'nBJet30_DL1rnn_FixedCutBEff_85' :{'tlatex':'N_{b-jet}^{pT>30GeV}, DL1 RNN FixedCutBEff 85%','units':'','hXNbins':7,'hXmin':0,'hXmax':7,'cut_pos':1,'cut_dir':'left'},
    
    # ---------------------------------------------
    # Jet kinematics
    # ---------------------------------------------
    'jetPt[0]' :{'tlatex':'p_{T}(j_{1})','units':'GeV','hXNbins':30,'hXmin':0, 'hXmax':600,'cut_pos':100,'cut_dir':'right'},
    'jetPt[1]' :{'tlatex':'p_{T}(j_{2})','units':'GeV','hXNbins':30,'hXmin':0, 'hXmax':300,'cut_pos':900,'cut_dir':'right'},
    'jetPt[2]' :{'tlatex':'p_{T}(j_{3})','units':'GeV','hXNbins':10,'hXmin':0, 'hXmax':200,'cut_pos':30, 'cut_dir':'right'},
    'jetPt[3]' :{'tlatex':'p_{T}(j_{4})','units':'GeV','hXNbins':10,'hXmin':0, 'hXmax':200,'cut_pos':30, 'cut_dir':'right'},
    'jetPt[4]' :{'tlatex':'p_{T}(j_{5})','units':'GeV','hXNbins':10,'hXmin':0, 'hXmax':200,'cut_pos':400,'cut_dir':'left'},
    'jetEta[0]':{'tlatex':'#eta(j_{1})', 'units':'',   'hXNbins':50,'hXmin':-5,'hXmax':5,  'cut_pos':10, 'cut_dir':'left'},
    'jetEta[1]':{'tlatex':'#eta(j_{2})', 'units':'',   'hXNbins':50,'hXmin':-5,'hXmax':5,  'cut_pos':10, 'cut_dir':'left'},

    # ---------------------------------------------
    # Jets and MET
    # ---------------------------------------------
    'DPhiJ1Met':{'tlatex':'|#Delta#phi(#bf{p}_{T}^{j1}, #bf{p}_{T}^{miss})|','units':'','hXNbins':20,'hXmin':0,'hXmax':4,'cut_pos':2.0,'cut_dir':'right'},
    'DPhiJ2Met':{'tlatex':'|#Delta#phi(#bf{p}_{T}^{j2}, #bf{p}_{T}^{miss})|','units':'','hXNbins':20,'hXmin':0,'hXmax':4,'cut_pos':2.5,'cut_dir':'right'},
    'DPhiJ3Met':{'tlatex':'|#Delta#phi(#bf{p}_{T}^{j3}, #bf{p}_{T}^{miss})|','units':'','hXNbins':20,'hXmin':0,'hXmax':4,'cut_pos':2.5,'cut_dir':'right'},
    'DPhiJ4Met':{'tlatex':'|#Delta#phi(#bf{p}_{T}^{j4}, #bf{p}_{T}^{miss})|','units':'','hXNbins':20,'hXmin':0,'hXmax':4,'cut_pos':2.5,'cut_dir':'right'},
    'vectorSumJetsPt'      :{'tlatex':'p_{T} of vectorial sum of all jets',  'units':'','hXNbins':20,'hXmin':0,'hXmax':600,'cut_pos':2.5,'cut_dir':'right'},
    'METOverJ1pT'          :{'tlatex':'E_{T}^{miss}/p_{T}(j_{1})',           'units':'','hXNbins':40,'hXmin':0,'hXmax':6.0,'cut_pos':10,'cut_dir':'right'},
    'METOverHT'            :{'tlatex':'E_{T}^{miss}/H_{T}^{jets}',           'units':'','hXNbins':40,'hXmin':0,'hXmax':6.0,'cut_pos':10,'cut_dir':'right'},
    'dPhiVectorSumJetsMET' :{'tlatex':'|#Delta#phi(#bf{H}_{T}^{jets}, #bf{p}_{T}^{miss})|]','units':'','hXNbins':16,'hXmin':0,'hXmax':4,'cut_pos':2.5,'cut_dir':'right'},
    'dPhiVectorSumJetsJ1'  :{'tlatex':'|#Delta#phi(#bf{H}_{T}^{jets}, #bf{p}_{T}^{j1})|]','units':'','hXNbins':16,'hXmin':0,'hXmax':4,'cut_pos':2.5,'cut_dir':'right'},
    'minDPhi4JetsMet'      :{'tlatex':'min[|#Delta#phi(#bf{p}_{T}^{leading four jets}, #bf{p}_{T}^{miss})|]','units':'','hXNbins':20,'hXmin':0,'hXmax':4,'cut_pos':2.5,'cut_dir':'right'},
    'minDPhiAllJetsMet'    :{'tlatex':'min[|#Delta#phi(#bf{p}_{T}^{All jets}, #bf{p}_{T}^{miss})|]','units':'','hXNbins':20,'hXmin':0,'hXmax':4,'cut_pos':0.4,'cut_dir':'right'},
   
    # ---------------------------------------------
    # Recursive jigsaw reconstruction
    # ---------------------------------------------
      
    'RJR_PTISR'    :{'tlatex':'p_{T}^{ISR}',        'units':'GeV','hXNbins':25,'hXmin':0,'hXmax':500,'cut_pos':10,'cut_dir':'right'},
    #'RJR_RISR'     :{'tlatex':'R_{ISR}',            'units':'',   'hXNbins':10,'hXmin':0.85,'hXmax':1.0,'cut_pos':0.9,'cut_dir':'right'},
    'RJR_RISR'     :{'tlatex':'R_{ISR}',            'units':'',   'hXNbins':40,'hXmin':0.7,'hXmax':1.1,'cut_pos':0.9,'cut_dir':'right'},
    #'RJR_RISR'     :{'tlatex':'R_{ISR}',            'units':'',   'hXNbins':20,'hXmin':0.0,'hXmax':1.2,'cut_pos':0.9,'cut_dir':'right'},
    'RJR_MS'       :{'tlatex':'M_{S}',              'units':'GeV','hXNbins':20,'hXmin':0,'hXmax':200,'cut_pos':60,'cut_dir':'left'},
    'RJR_dphiISRI' :{'tlatex':'#Delta#phi(ISR, I)', 'units':'',   'hXNbins':20,'hXmin':0,'hXmax':4.0,'cut_pos':10,'cut_dir':'right'},
    'RJR_NjV'      :{'tlatex':'N_{jets}^{V}',       'units':'',   'hXNbins':10,'hXmin':0,'hXmax':10, 'cut_pos':10,'cut_dir':'right'},
    'RJR_NjISR'    :{'tlatex':'N_{jets}^{ISR}',     'units':'',   'hXNbins':10,'hXmin':0,'hXmax':10, 'cut_pos':10,'cut_dir':'right'},
    'met_Et*sqrt(METOverHT/met_Et)':{'tlatex':'ME_{T}/#sqrt{HT}',     'units':'','hXNbins':12,'hXmin':0,'hXmax':24, 'cut_pos':11,'cut_dir':'right'},


    # -------------------//------------------------
   
    
    # ---------------------------------------------
    #
    # Leptons
    #
    # ---------------------------------------------

    # ---------------------------------------------
    # Lepton multiplicities
    # ---------------------------------------------
    'nLep_base'   :{'tlatex':'N(baseline leptons)','units':'','hXNbins':10,'hXmin':0,'hXmax':10,'cut_pos':120,'cut_dir':'left'},
    'nLep_signal' :{'tlatex':'N(signal leptons)',  'units':'','hXNbins':10,'hXmin':0,'hXmax':10,'cut_pos':123,'cut_dir':'left'},

    # ---------------------------------------------
    # Lepton kinematics
    # ---------------------------------------------
    #'lep1Pt':{'tlatex':'p_{T}(#font[12]{l}_{1})','units':'GeV','hXNbins':100,'hXmin':0,'hXmax':1000,'cut_pos':200,'cut_dir':'left'},
    #'lep2Pt':{'tlatex':'p_{T}(#font[12]{l}_{2})','units':'GeV','hXNbins':100,'hXmin':0,'hXmax':1000,'cut_pos':200,'cut_dir':'left'},
    #'lep3Pt':{'tlatex':'p_{T}(#font[12]{l}_{3})','units':'GeV','hXNbins':100,'hXmin':0,'hXmax':1000,'cut_pos':200,'cut_dir':'left'},
    'lep1Pt':{'tlatex':'p_{T}(#font[12]{l}_{1})','units':'GeV','hXNbins':40,'hXmin':0,'hXmax':40,'cut_pos':200,'cut_dir':'right'},
    'lep2Pt':{'tlatex':'p_{T}(#font[12]{l}_{2})','units':'GeV','hXNbins':40,'hXmin':0,'hXmax':40,'cut_pos':200,'cut_dir':'right'},
    'lep3Pt':{'tlatex':'p_{T}(#font[12]{l}_{3})','units':'GeV','hXNbins':40,'hXmin':0,'hXmax':40,'cut_pos':200,'cut_dir':'right'},
    #'lep1Pt':{'tlatex':'p_{T}(#font[12]{l}_{1})','units':'GeV','hXNbins':'var','hXmin':0,'hXmax':60,'binsLowE':[0,4,5,11,15,20,40,60],'cut_pos':200,'cut_dir':'left'},
    #'lep2Pt':{'tlatex':'p_{T}(#font[12]{l}_{2})','units':'GeV','hXNbins':'var','hXmin':0,'hXmax':60,'binsLowE':[0,4,5,11,15,20,40,60],'cut_pos':200,'cut_dir':'left'},
    #'lep1Pt':{'tlatex':'p_{T}(#font[12]{l}_{1})','units':'GeV','hXNbins':10,'hXmin':0,'hXmax':10,'cut_pos':200,'cut_dir':'left'},
    'lep1Eta':{'tlatex':'#eta(#font[12]{l}_{1})','units':'','hXNbins':50,'hXmin':-5,'hXmax':5,'cut_pos':120,'cut_dir':'left'},
    'lep2Eta':{'tlatex':'#eta(#font[12]{l}_{2})','units':'','hXNbins':50,'hXmin':-5,'hXmax':5,'cut_pos':120,'cut_dir':'left'},
    'lep3Eta':{'tlatex':'#eta(#font[12]{l}_{3})','units':'','hXNbins':50,'hXmin':-5,'hXmax':5,'cut_pos':120,'cut_dir':'left'},
    'lep1Phi':{'tlatex':'#phi(#font[12]{l}_{1})','units':'','hXNbins':40,'hXmin':-4,'hXmax':4,'cut_pos':120,'cut_dir':'left'},
    'lep2Phi':{'tlatex':'#phi(#font[12]{l}_{2})','units':'','hXNbins':40,'hXmin':-4,'hXmax':4,'cut_pos':120,'cut_dir':'left'},
    'lep3Phi':{'tlatex':'#phi(#font[12]{l}_{3})','units':'','hXNbins':40,'hXmin':-4,'hXmax':4,'cut_pos':120,'cut_dir':'left'},
    'ptWlep_minMll':{'tlatex':'p_{T}(#font[12]{l}_{W})','units':'GeV','hXNbins':50,'hXmin':0,'hXmax':500,'cut_pos':200,'cut_dir':'left'},

    # ---------------------------------------------
    # MCTruthClassifer
    # ---------------------------------------------
    'lep1Type'   :{'tlatex':'Lep 1 MC Truth Type',  'units':'','hXNbins':39,'hXmin':0,'hXmax':39,'cut_pos':50,'cut_dir':'left'},
    'lep2Type'   :{'tlatex':'Lep 2 MCTruth Type',   'units':'','hXNbins':39,'hXmin':0,'hXmax':39,'cut_pos':50,'cut_dir':'left'},
    'lep1Origin' :{'tlatex':'Lep 1 MC Truth Origin','units':'','hXNbins':46,'hXmin':0,'hXmax':46,'cut_pos':70,'cut_dir':'left'},
    'lep2Origin' :{'tlatex':'Lep 2 MC Truth Origin','units':'','hXNbins':46,'hXmin':0,'hXmax':46,'cut_pos':70,'cut_dir':'left'},
 
    # ---------------------------------------------
    # Lepton impact parameter variables
    # ---------------------------------------------
    # Longitudinal
    'lep1Z0'        :{'tlatex':'z_{0}(#font[12]{l}_{1})','units':'mm','hXNbins':80,'hXmin':-20,'hXmax':20,'cut_pos':50,'cut_dir':'left'},
    'lep2Z0'        :{'tlatex':'z_{0}(#font[12]{l}_{2})','units':'mm','hXNbins':80,'hXmin':-20,'hXmax':20,'cut_pos':50,'cut_dir':'left'},
    'lep1Z0SinTheta':{'tlatex':'z_{0}(#font[12]{l}_{1})','units':'mm','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':50,'cut_dir':'left'},
    'lep2Z0SinTheta':{'tlatex':'z_{0}(#font[12]{l}_{2})','units':'mm','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':50,'cut_dir':'left'},
    # Transverse
    'lep1D0'        :{'tlatex':'d_{0}(#font[12]{l}_{1})','units':'mm','hXNbins':80,'hXmin':-20,'hXmax':20,'cut_pos':50,'cut_dir':'left'},
    'lep2D0'        :{'tlatex':'d_{0}(#font[12]{l}_{2})','units':'mm','hXNbins':80,'hXmin':-20,'hXmax':20,'cut_pos':50,'cut_dir':'left'},
    'lep1D0Sig'     :{'tlatex':'d_{0}(#font[12]{l}_{1}) / #sigma(d_{0}(#font[12]{l}_{1}))','units':'','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':50,'cut_dir':'left'},
    'lep2D0Sig'     :{'tlatex':'d_{0}(#font[12]{l}_{2}) / #sigma(d_{0}(#font[12]{l}_{1}))','units':'','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':50,'cut_dir':'left'},
 
    # ---------------------------------------------
    # Lepton isolation cone variables
    # ---------------------------------------------
    'lep1Ptvarcone20'  :{'tlatex':'p_{T} Var Cone 20',       'units':'','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':999,'cut_dir':'left'},
    'lep1Ptvarcone30'  :{'tlatex':'p_{T} Var Cone 30',       'units':'','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':999,'cut_dir':'left'},
    'lep1Topoetcone20' :{'tlatex':'p_{T} Topo E_{T} Cone 30','units':'','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':999,'cut_dir':'left'},
    'lep2Ptvarcone20'  :{'tlatex':'p_{T} Var Cone 20',       'units':'','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':999,'cut_dir':'left'},
    'lep2Ptvarcone30'  :{'tlatex':'p_{T} Var Cone 30',       'units':'','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':999,'cut_dir':'left'},
    'lep2Topoetcone20' :{'tlatex':'p_{T} Topo E_{T} Cone 30','units':'','hXNbins':40,'hXmin':0,'hXmax':20,'cut_pos':999,'cut_dir':'left'},

    # ---------------------------------------------
    # Higher level lepton variables 
    # ---------------------------------------------
    'Ptll' :{'tlatex':'p_{T}(#font[12]{ll})',  'units':'',   'hXNbins':30,'hXmin':0,  'hXmax':150,'cut_pos':200,'cut_dir':'right'},
    'Rll'  :{'tlatex':'#DeltaR(#font[12]{ll})','units':'',   'hXNbins':25,'hXmin':0,  'hXmax':5,  'cut_pos':2.0,'cut_dir':'left'},
    'minRll'  :{'tlatex':'min #DeltaR(#font[12]{ll})','units':'',   'hXNbins':25,'hXmin':0,  'hXmax':5,  'cut_pos':2.0,'cut_dir':'left'},
    'mll' :{'tlatex':'m(#font[12]{ll})','units':'GeV','hXNbins':'var','hXmin':0.0,'hXmax':60,'binsLowE':[0,1,2,3,5,10,15,20,25,30,40,50,60],'cut_pos':900,'cut_dir':'left'},
    #'mll' :{'tlatex':'m(#font[12]{ll})','units':'GeV','hXNbins':50,'hXmin':0.0,'hXmax':200,'cut_pos':230,'cut_dir':'left'},
    #'mll' :{'tlatex':'m(#font[12]{ll})','units':'GeV','hXNbins':5,'hXmin':0.0,'hXmax':5,'cut_pos':230,'cut_dir':'left'},
    'mSFOS_min':{'tlatex':'m_{SFOS}^{min}','units':'GeV','hXNbins':50,'hXmin':0.0,'hXmax':200,'cut_pos':230,'cut_dir':'left'},
    'mSFOS_max':{'tlatex':'m_{SFOS}^{max}','units':'GeV','hXNbins':50,'hXmin':0.0,'hXmax':200,'cut_pos':230,'cut_dir':'left'},
   
    'lep1PtOverlep2Pt':{'tlatex':'p_{T}^{#font[12]{l}_{1}}/p_{T}^{#font[12]{l}_{2}}','units':'','hXNbins':25,'hXmin':0,'hXmax':10,'cut_pos':120,'cut_dir':'left'},
    'lep1PtOverMll'   :{'tlatex':'p_{T}^{#font[12]{l}_{1}}/m(#font[12]{ll})',        'units':'','hXNbins':30,'hXmin':0,'hXmax':6, 'cut_pos':120,'cut_dir':'right'},
    'lep2PtOverMll'   :{'tlatex':'p_{T}^{#font[12]{l}2}/m(#font[12]{ll})',           'units':'','hXNbins':30,'hXmin':0,'hXmax':6, 'cut_pos':120,'cut_dir':'right'},
    'lep12PtOverMll'  :{'tlatex':'H_{T}^{leptons}/m(#font[12]{ll})',                 'units':'','hXNbins':50,'hXmin':0,'hXmax':10,'cut_pos':120,'cut_dir':'right'},
    'lep12PtOverMt2'  :{'tlatex':'H_{T}^{leptons}/m_{T2}^{#chi=100GeV}',             'units':'','hXNbins':20,'hXmin':0,'hXmax':5, 'cut_pos':120,'cut_dir':'left'},
    'LepCosThetaCoM'  :{'tlatex':'|cos#theta_{#font[12]{ll}}^{Melia}|',              'units':'','hXNbins':10,'hXmin':0,'hXmax':1,'cut_pos':1.0,'cut_dir':'left'},
    'LepCosThetaLab'  :{'tlatex':'|cos#theta_{#font[12]{ll}}^{Barr}|',               'units':'','hXNbins':10,'hXmin':0,'hXmax':1,'cut_pos':1.0,'cut_dir':'left'},
    'RjlOverEl'       :{'tlatex':'min(R_{j#font[12]{l}})/E_{#font[12]{l}}',          'units':'GeV^{#minus1}','hXNbins':15,'hXmin':0,'hXmax':0.15,'cut_pos':0.02,'cut_dir':'right'},
    
    # ---------------------------------------------
    # Lepton variables involving MET
    # ---------------------------------------------
    'mt_lep1'         :{'tlatex':'m_{T}(#font[12]{l}_{1})',                        'units':'GeV','hXNbins':20,'hXmin':0,'hXmax':200,'cut_pos':70,'cut_dir':'left'},
    'mt_lep2'         :{'tlatex':'m_{T}(#font[12]{l}_{2})',                        'units':'GeV','hXNbins':40,'hXmin':0,'hXmax':200,'cut_pos':320,'cut_dir':'left'},
    'mt_lep3'         :{'tlatex':'m_{T}(#font[12]{l}_{3})',                        'units':'GeV','hXNbins':40,'hXmin':0,'hXmax':200,'cut_pos':320,'cut_dir':'left'},
    'mt_lep1+mt_lep2' :{'tlatex':'m_{T}(#font[12]{l}_{1})+m_{T}(#font[12]{l}_{2})','units':'GeV','hXNbins':30,'hXmin':0,'hXmax':300,'cut_pos':820,'cut_dir':'left'},
    'METOverHTLep'    :{'tlatex':'E_{T}^{miss}/H_{T}^{leptons}',                   'units':'',   'hXNbins':30,'hXmin':0,'hXmax':30, 'cut_pos':5.0,'cut_dir':'right'},
    'METOverHTLep/max(5,15-2*mll)'    :{'tlatex':'(E_{T}^{miss}/H_{T}^{leptons}) / max(5,15 - 2*mll)', 'units':'','hXNbins':50,'hXmin':0.0,'hXmax':5.0, 'cut_pos':0.0,'cut_dir':'right'},
    #'METOverHTLep'    :{'tlatex':'E_{T}^{miss}/H_{T}^{leptons}','units':'','hXNbins':'var','hXmin':0,'hXmax':30.0,'binsLowE':[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,17,20,25,30],'cut_pos':5.0,'cut_dir':'right'},
    'MTauTau'         :{'tlatex':'m(#tau#tau)','units':'GeV','hXNbins':40,'hXmin':-2000,'hXmax':2000,'cut_pos':0,'cut_dir':'left','cut_pos2':160,'cut_dir2':'right'},
    'dPhiPllMet'      :{'tlatex':'|#Delta#phi(#bf{p}_{#font[12]{ll}},#bf{p}_{T}^{miss})|','units':'','hXNbins':25,'hXmin':0,'hXmax':5,'cut_pos':170,'cut_dir':'left'},

    # ---------------------------------------------
    # mT2 by various trial invisible masses
    # ---------------------------------------------
    'mt2leplsp_0'  :{'tlatex':'m_{T2}^{0}',  'units':'GeV','hXNbins':50,'hXmin':0,  'hXmax':100,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_25' :{'tlatex':'m_{T2}^{25}', 'units':'GeV','hXNbins':50,'hXmin':15, 'hXmax':115,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_50' :{'tlatex':'m_{T2}^{50}', 'units':'GeV','hXNbins':50,'hXmin':40, 'hXmax':140,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_75' :{'tlatex':'m_{T2}^{75}', 'units':'GeV','hXNbins':50,'hXmin':65, 'hXmax':165,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_90' :{'tlatex':'m_{T2}^{90}', 'units':'GeV','hXNbins':50,'hXmin':80, 'hXmax':180,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_100':{'tlatex':'m_{T2}^{100}','units':'GeV','hXNbins':'var','hXmin':100,'hXmax':140,'binsLowE':[100,100.5,101,102,105,110,120,130,140],'cut_pos':920,'cut_dir':'left'},
    #'mt2leplsp_100':{'tlatex':'m_{T2}^{100}','units':'GeV','hXNbins':50,'hXmin':90, 'hXmax':190,'cut_pos':1120,'cut_dir':'left'},
    'mt2leplsp_110':{'tlatex':'m_{T2}^{110}','units':'GeV','hXNbins':50,'hXmin':100,'hXmax':200,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_120':{'tlatex':'m_{T2}^{120}','units':'GeV','hXNbins':50,'hXmin':110,'hXmax':210,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_150':{'tlatex':'m_{T2}^{150}','units':'GeV','hXNbins':50,'hXmin':140,'hXmax':240,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_175':{'tlatex':'m_{T2}^{175}','units':'GeV','hXNbins':50,'hXmin':165,'hXmax':265,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_200':{'tlatex':'m_{T2}^{200}','units':'GeV','hXNbins':50,'hXmin':190,'hXmax':290,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_250':{'tlatex':'m_{T2}^{250}','units':'GeV','hXNbins':50,'hXmin':240,'hXmax':340,'cut_pos':120,'cut_dir':'left'},
    'mt2leplsp_300':{'tlatex':'m_{T2}^{300}','units':'GeV','hXNbins':50,'hXmin':290,'hXmax':390,'cut_pos':120,'cut_dir':'left'},
    
    
    # -------------------//------------------------
   

    # ---------------------------------------------
    # Pileup 
    # ---------------------------------------------
    'nVtx' :{'tlatex':'Number of vertices',           'units':'','hXNbins':20,'hXmin':0,'hXmax':60,'cut_pos':170,'cut_dir':'left'},
    'mu'   :{'tlatex':'Interactions per crossing (avg) #mu','units':'','hXNbins':35,'hXmin':0,'hXmax':70,'cut_pos':170,'cut_dir':'left'},
    'avg_mu'   :{'tlatex':'Interactions per crossing avg #mu','units':'','hXNbins':35,'hXmin':0,'hXmax':70,'cut_pos':170,'cut_dir':'left'},
    'actual_mu'   :{'tlatex':'Interactions per crossing actual #mu','units':'','hXNbins':35,'hXmin':0,'hXmax':70,'cut_pos':170,'cut_dir':'left'},

  } # end of d_vars = {} dictionary

  return d_vars

