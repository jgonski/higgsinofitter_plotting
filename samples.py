'''

Welcome to samples.py

This has a few functions to
- Define the paths to the ntuples in get_sample_paths()
- Specify which background and signal samples to plot in get_samples_to_plot()
- Configure sample type, legend entry and colours in configure_samples() 

'''

from ROOT import TColor
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange

#____________________________________________________________________________
def get_sample_paths():

  # -------------------------------------
  #
  # Set paths to ntuples
  #
  # -------------------------------------
  
  # Paths to ntuples
  # Joey's configuration
  #TOPPATH = '/usatlas/groups/bnl_local2/jgonski/SusySkimHiggsinov19'
  #bkg_path = TOPPATH
  #sig_path = TOPPATH
  #data_path = TOPPATH

  # Jesse's configuration
  #TOPPATH = '/home/jesseliu/atlas/susy/compressed_ew/data/atlas-susy-ew/atlas-susy-ew-softlepton/ew-git/submitdir/out_ntup/SusySkimHiggsino_v2.1' 
  #bkg_path  = TOPPATH + '/SusySkimHiggsino_v2.1_SUSY16_Bkgs_tree/add3L'
  #data_path = TOPPATH + '/SusySkimHiggsino_v2.1_SUSY16_Bkgs_tree/add3L'
  #sig_path  = TOPPATH + '/SusySkimHiggsino_v2.1_SUSY16_Signal_tree/add3L'
  
  # Default configuration: use HiggsinoFitter soft link to SusySkimHiggsino ntuples
  #TOPPATH = '$HISTFITTER/../higgsino_samples'
  
  # Path to background ntuples 
  # Julia's configuration
  bkg_path = '/usatlas/groups/bnl_local2/jgonski/December5_2018_Higgsino_R21_v2.7_Production/R21_SUSY16_repro_Bkgs_Combined_mc16ade_FINAL/outputs/'
  #bkg_path = '/usatlas/groups/bnl_local/jgonski/Sept1_2018_Higgsino_R21_v2.5_Production/R21_SUSY16_Bkgs_mc16e_Skims/R21_SUSY16_Bkgs_mc16e/v2.5/merged/'
  ## Path to data ntuple
  data_path = '/usatlas/groups/bnl_local2/jgonski/December5_2018_Higgsino_R21_v2.7_Production/R21_SUSY16_repro_Data_Skims_FINAL/R21_SUSY16_repro_Data/v2.7/merged/'
  #data_path = '/usatlas/groups/bnl_local/jgonski/Sept1_2018_Higgsino_R21_v2.5_Production/DATA18_Skims/DATA18/v2.5/merged'
  ## Path to signal ntuples
  #sig_path  = '/usatlas/groups/bnl_local/jgonski/Sept1_2018_Higgsino_R21_v2.5_Production/R21_SUSY16_Signal_Combined_mc16ad/outputs_sleptons/'
  sig_path  = '/usatlas/groups/bnl_local2/jgonski/December5_2018_Higgsino_R21_v2.7_Production/R21_SUSY16_repro_Signal_Combined_mc16ad_FINAL/outputs/'

  
  # -------------------------------------
  #
  # Suffix of the sample file names
  #
  # -------------------------------------
  
  # background path bkg_suffix
  # Joey's configuration
  #bkg_suffix = '_SusySkimHiggsino_v1.9_hadded_tree.root'
  #sig_suffix = '_SusySkimHiggsino_v1.9_SUSY16_tree_NoSys.root'
  #fakes_suffix = '_SusySkimHiggsino_v1.9e_SUSY16_tree_NoSys.root'
  #data_suffix = '_SusySkimHiggsino_v1.9e_SUSY16_tree_NoSys.root'  

  # Jesse's configuration
  #bkg_suffix   = '_SusySkimHiggsino_v2.5_mc16e_SUSY16_tree_NoSys.root'
  bkg_suffix   = '_SusySkimHiggsino_v2.7_SUSY16_tree_NoSys.root'
  #data_suffix  = '_SusySkimHiggsino_v2.5_mc16e_SUSY16_tree_NoSys.root'
  data_suffix  = '_SusySkimHiggsino_v2.7_SUSY16_tree_NoSys.root'
  fakes_suffix = '_SusySkimHiggsino_v2.7_SUSY16_tree_NoSys.root'
  sig_suffix   = '_SusySkimHiggsino_v2.7_SUSY16_tree_NoSys.root'
  #sig_suffix   = '_NoSys_v25_skimmed.root'

  return bkg_path, data_path, sig_path, bkg_suffix, data_suffix, fakes_suffix, sig_suffix


#____________________________________________________________________________
def get_samples_to_plot(ttbarSamp='ttbar'):
  '''
  List of background and signal samples to analyse in plot.py 
  '''
  
  #------------------------------------
  # Backgrounds
  #------------------------------------
  l_samp_bkg = [
    'triboson',
    #'higgs',
    #'diboson0L',
    #'diboson1L',
    #'diboson2L',
    #'diboson3L',
    #'diboson4L',
    'diboson1L',
    'diboson2L',
    'diboson3L',
    'diboson4L',
    'Zjets_all',
    'Zttjets_new',
    #'Zjets',
    #'Zttjets',
    'topOther',
    'singletop',
    ttbarSamp,
    #'ttbar',
    #'alt_ttbar_PowPy8_nonallhad_hdamp258p75',
    'Wjets',
    #'other',
    #'diboson',
    #'Zttjets', 
    #'top', 
    #'fakes', 
    ]
  #For quicker testing use fewer backgrounds
  #l_samp_bkg = [
  #  #'diboson2L',
  #  #'diboson3L',
  #  'triboson',
  #  #'diboson1L',
  #  #'Wjets',
  #] 
#
  #------------------------------------
  # Signals
  #------------------------------------
  l_samp_sig = [
    #'MGPy8EG_A14N23LO_WZ_100p0_80p0_3L_3L3_MadSpin',
    #'MGPy8EG_A14N23LO_WZ_100p0_90p0_3L_3L3_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_103_100_2LMET50_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_105_100_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_110_100_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_140_125_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_205_200_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_160_150_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_152_150_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_153_150_2L2MET75_MadSpin',
    #for regular plots
    #'MGPy8EG_A14N23LO_SM_Higgsino_145_125_2L2MET75_MadSpin',
    'MGPy8EG_A14N23LO_SM_Higgsino_165_150_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_210_200_2L2MET75_MadSpin',
    'MGPy8EG_A14N23LO_SM_Higgsino_205_200_2L2MET75_MadSpin',
    'MGPy8EG_A14N23LO_SM_Higgsino_128_125_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_127_125_2L2MET75_MadSpin',
    'MGPy8EG_A14N23LO_SM_Higgsino_102_100_2L2MET75_MadSpin',
    #for diagnostic plots
    #'MGPy8EG_A14N23LO_SM_Higgsino_103_100_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_127_125_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_155_150_2L2MET75_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_90_80_2LMET50_MadSpin',
    #'MGPy8EG_A14N23LO_SM_Higgsino_103_100_2LMET50_MadSpin',

    #'MGPy8EG_A14N23LO_SlepSlep_direct_102p0_100p0_MET50',
    #'MGPy8EG_A14N23LO_SlepSlep_direct_100p0_99p5_2L2MET75',
    'MGPy8EG_A14N23LO_SlepSlep_direct_125p0_120p0_2L2MET75',
    'MGPy8EG_A14N23LO_SlepSlep_direct_150p0_124p3_2L2MET75',
    #'MGPy8EG_A14N23LO_SlepSlep_direct_200p0_190p0_2L2MET75', 
  ]

  return l_samp_bkg, l_samp_sig

#____________________________________________________________________________
def configure_samples():
  
  # -------------------------------------
  #
  # Custom colours (in hexadecimal RGB)
  # Taken from http://colorbrewer2.org/
  #
  # -------------------------------------

  
  # Blues
  myLighterBlue   = TColor.GetColor('#deebf7')
  myLightBlue     = TColor.GetColor('#9ecae1')
  myMediumBlue    = TColor.GetColor('#0868ac')
  myDarkBlue      = TColor.GetColor('#08306b')

  # Greens
  myLightGreen    = TColor.GetColor('#c7e9c0')
  myMediumGreen   = TColor.GetColor('#41ab5d')
  myDarkGreen     = TColor.GetColor('#006d2c')

  # Oranges
  myLighterOrange = TColor.GetColor('#ffeda0')
  myLightOrange   = TColor.GetColor('#fec49f')
  myMediumOrange  = TColor.GetColor('#fe9929')
  myDarkOrange    = TColor.GetColor('#ec7014')
  myDarkerOrange  = TColor.GetColor('#cc4c02')

  # Greys
  myLightestGrey  = TColor.GetColor('#f0f0f0')
  myLighterGrey   = TColor.GetColor('#e3e3e3')
  myLightGrey     = TColor.GetColor('#969696')

  # Pinks
  myLightPink     = TColor.GetColor('#fde0dd')
  myMediumPink    = TColor.GetColor('#fcc5c0')
  myDarkPink      = TColor.GetColor('#dd3497')
  
  # Purples
  myLightPurple   = TColor.GetColor('#dadaeb')
  myMediumPurple  = TColor.GetColor('#9e9ac8')
  myDarkPurple    = TColor.GetColor('#6a51a3')

  # -------------------------------------
  #
  # Samples dictionary
  #
  # 'TTree name for sample' : 
  #                 { 'type'    : set as data, background or signal
  #                   'leg'     : label that appears in the plot legend
  #                   'f_color' : fill colour of background sample
  #                   'l_color' : line colour of signal sample 
  #                   }
  #
  # -------------------------------------
  d_samp = {
    
    # Data 
    'data'      :{'type':'data', 'leg':'Data',            'f_color':0              },
    'fakes'     :{'type':'bkg',  'leg':'Fake/nonprompt',  'f_color': myLightGrey },
    
    # Top quark
    'top'       :{'type':'bkg', 'leg':'Top',                         'f_color':myLightBlue },
    'ttbar'     :{'type':'bkg', 'leg':'t#bar{t} #geq 2#font[12]{l}', 'f_color':myLightBlue  },
    'singletop' :{'type':'bkg', 'leg':'t, tW',                       'f_color':myMediumBlue },
    'topOther'  :{'type':'bkg', 'leg':'Top other',                   'f_color':myDarkBlue   },
    'alt_ttbar_PowPy8_nonallhad_hdamp258p75' :{'type':'bkg', 'leg':'t#bar{t} #geq 1#font[12]{l}', 'f_color':myLightBlue },

    # V+jets
    'Wjets'     :{'type':'bkg', 'leg':'W+jets',                       'f_color':myLighterGrey },
    'Zjets_all'     :{'type':'bkg', 'leg':'Z(#rightarrowee/#mu#mu)+jets', 'f_color':myLightGreen  },
    'Zttjets_new'   :{'type':'bkg', 'leg':'Z(#rightarrow#tau#tau)+jets',  'f_color':myMediumGreen },
    'Zjets'     :{'type':'bkg', 'leg':'Z(#rightarrowee/#mu#mu)+jets', 'f_color':myLightGreen  },
    'Zttjets'   :{'type':'bkg', 'leg':'Z(#rightarrow#tau#tau)+jets',  'f_color':myMediumGreen },

    # VV
    'alt_diboson'   :{'type':'bkg', 'leg':'alt_VV',               'f_color':myMediumOrange  },
    'alt_diboson0L_PowPy' :{'type':'bkg', 'leg':'alt_VV 0#font[12]{l}', 'f_color':myLighterOrange },
    'alt_diboson1L_PowPy' :{'type':'bkg', 'leg':'alt_VV 1#font[12]{l}', 'f_color':myLightOrange   },
    'alt_diboson2L_PowPy' :{'type':'bkg', 'leg':'alt_VV 2#font[12]{l}', 'f_color':myMediumOrange  },
    'alt_diboson3L_PowPy' :{'type':'bkg', 'leg':'alt_VV 3#font[12]{l}', 'f_color':myDarkOrange    },
    'alt_diboson4L_PowPy' :{'type':'bkg', 'leg':'alt_VV 4#font[12]{l}', 'f_color':myDarkerOrange  },
    'diboson'   :{'type':'bkg', 'leg':'VV',               'f_color':myMediumOrange  },
    'diboson0L' :{'type':'bkg', 'leg':'VV 0#font[12]{l}', 'f_color':myLighterOrange },
    'diboson1L' :{'type':'bkg', 'leg':'VV 1#font[12]{l}', 'f_color':myLightOrange   },
    'diboson2L' :{'type':'bkg', 'leg':'VV 2#font[12]{l}', 'f_color':myMediumOrange  },
    'diboson3L' :{'type':'bkg', 'leg':'VV 3#font[12]{l}', 'f_color':myDarkOrange    },
    'diboson4L' :{'type':'bkg', 'leg':'VV 4#font[12]{l}', 'f_color':myDarkerOrange  },
    
    # Other samples
    'other'     :{'type':'bkg', 'leg':'Others',           'f_color':myLighterOrange },
    'higgs'     :{'type':'bkg', 'leg':'Higgs',            'f_color':myLightPink    },
    'triboson'  :{'type':'bkg', 'leg':'VVV',              'f_color':myLighterOrange     },

    # Signals
    #2LMET50
    #'MGPy8EG_A14N23LO_SM_Higgsino_103_100_2LMET50_MadSpin':{'type':'sig','leg':'#tilde{H} [103, 100]',           'l_color':kViolet+3 },
    #'MGPy8EG_A14N23LO_SM_Higgsino_155_150_2LMET50_MadSpin':{'type':'sig','leg':'#tilde{H} [155, 150]',           'l_color':kBlue+3 },
    #2L2MET75
    'MGPy8EG_A14N23LO_SM_Higgsino_102_100_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [102, 100]',           'l_color':kPink+9},
    'MGPy8EG_A14N23LO_SM_Higgsino_103_100_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [103, 100]',           'l_color':kSpring-2 },
    'MGPy8EG_A14N23LO_SM_Higgsino_105_100_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [105, 100]',           'l_color':kCyan+1 },
    'MGPy8EG_A14N23LO_SM_Higgsino_110_100_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [110, 100]',           'l_color':kViolet-2 },
    'MGPy8EG_A14N23LO_SM_Higgsino_127_125_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [127, 125]',           'l_color':kRed },
    'MGPy8EG_A14N23LO_SM_Higgsino_128_125_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [128, 125]',           'l_color':kTeal+10 },
    'MGPy8EG_A14N23LO_SM_Higgsino_152_150_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [152, 150]',           'l_color':kRed+2 },
    'MGPy8EG_A14N23LO_SM_Higgsino_153_150_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [153, 150]',           'l_color':kViolet+4 },
    'MGPy8EG_A14N23LO_SM_Higgsino_155_150_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [155, 150]',           'l_color':kCyan+1 },
    'MGPy8EG_A14N23LO_SM_Higgsino_115_100_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [115, 100]',           'l_color':kTeal },
    'MGPy8EG_A14N23LO_SM_Higgsino_120_100_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [120, 100]',           'l_color':kSpring+4 },
    'MGPy8EG_A14N23LO_SM_Higgsino_205_200_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [205, 200]',           'l_color':kTeal-7 },
    'MGPy8EG_A14N23LO_SM_Higgsino_210_200_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [210, 200]',           'l_color':kAzure+10 },
    'MGPy8EG_A14N23LO_SM_Higgsino_130_125_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [130, 125]',           'l_color':kBlue+8 },
    'MGPy8EG_A14N23LO_SM_Higgsino_160_150_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [160, 150]',           'l_color':kMagenta },
    'MGPy8EG_A14N23LO_SM_Higgsino_165_150_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [165, 150]',           'l_color':kAzure }, 
    'MGPy8EG_A14N23LO_SM_Higgsino_140_125_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [140, 125]',           'l_color':kMagenta+5 },
    'MGPy8EG_A14N23LO_SM_Higgsino_145_125_2L2MET75_MadSpin':{'type':'sig','leg':'#tilde{H} [145, 125]',           'l_color':kViolet+1 },
   
    #'MGPy8EG_A14N23LO_WZ_100p0_80p0_3L_3L3_MadSpin'       :{'type':'sig','leg':'#tilde{W} [100, 80]',            'l_color':kViolet+3 },
    #'MGPy8EG_A14N23LO_WZ_100p0_90p0_3L_3L3_MadSpin'       :{'type':'sig','leg':'#tilde{W} [100, 90]',            'l_color':kRed+2    },
    
    'MGPy8EG_A14N23LO_SlepSlep_direct_100p0_99p5_2L2MET75':{'type':'sig','leg':'#tilde{l} [100, 99.5]',           'l_color':kPink+9},    
    'MGPy8EG_A14N23LO_SlepSlep_direct_125p0_120p0_2L2MET75':{'type':'sig','leg':'#tilde{l} [125, 120]',           'l_color':kSpring-2 },
    'MGPy8EG_A14N23LO_SlepSlep_direct_150p0_124p3_2L2MET75':{'type':'sig','leg':'#tilde{l} [150, 124.3]',           'l_color':kCyan+1 },
    'MGPy8EG_A14N23LO_SlepSlep_direct_200p0_190p0_2L2MET75':{'type':'sig','leg':'#tilde{l} [200, 190]',           'l_color':kViolet-2 },
  
    
    }

  return d_samp


