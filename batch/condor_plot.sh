#!/bin/bash
# Batch script for condor submission created by HiggsinoFitter plotting/batch/make_batch_scripts.py
#cd /usatlas/u/jgonski/HiggsinoFitter/HistFitter
#source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh ; lsetup "root 6.06.06-x86_64-slc6-gcc49-opt" ; source setup.sh ; 
cd ../
#./plot.py -s $1 -v $2 -p 15_16_17 -m
./plot.py -s $1 -v $2 -o -f
#./plot.py -s $1 -v $2 -o -f -m
#./plot.py -s $1 -v $2 -p 15_16_17 -o -f
#./plot.py -s $1 -v $2 -p 18 -o -f 
