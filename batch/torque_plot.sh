#!/bin/bash
#PBS -m n
#PBS -N "plot_susy"
# Batch script for torque submission created by HiggsinoFitter plotting/batch/make_batch_scripts.py
cd /usatlas/u/jgonski/HiggsinoFitter/HistFitter
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh ; lsetup "root 6.06.06-x86_64-slc6-gcc49-opt" ; source setup.sh ; cd ../plotting
./plot.py -s $sig_reg -v $var -p 15_16
./plot.py -s $sig_reg -v $var -p 17
./plot.py -s $sig_reg -v $var -p 15_16_17