#!/usr/bin/env python
'''
Welcome to HiggsinoFitter make_batch_scripts.py
  - This is called on the first setup to make the torque and condor batch submission scripts
  - These are made to live in torque/ and condor/ directories
  - If they are unsatisfactory, modify the content of this script and rerun it to regenerate those batch scripts
  - A helpful guide for condor batch submission is http://batchdocs.web.cern.ch/batchdocs/local/submit.html
'''

import sys, os, time, argparse

# Extract the present HISTFITTER working directory (ensure you've done HiggsinoFitter's source setup)
HF_dir = os.environ['HISTFITTER']

#____________________________________________________________________________
def main():

  mk_batch_script( 'torque' )
  mk_batch_script( 'condor' )


#_________________________________________________________________________
def mk_batch_script( batch_type ):
  '''
  Make the batch script program that the cluster computer core runs
  '''


  # Name batch submission script
  script_name = '{0}/../plotting/batch/{1}_plot.sh'.format( HF_dir, batch_type )
  # Write the following lines to the batch submission script
  with open(script_name, 'w') as f_script:
    f_script.write( '#!/bin/bash\n' )

    if 'torque' in batch_type:
      f_script.write('#PBS -m n\n#PBS -N "plot_susy"\n' )

    f_script.write( '# Batch script for {0} submission created by HiggsinoFitter plotting/batch/make_batch_scripts.py\n'.format(batch_type) )
    f_script.write( 'cd {0}\n'.format(HF_dir) )
    f_script.write( 'source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh ; lsetup "root 6.06.06-x86_64-slc6-gcc49-opt" ; source setup.sh ; cd ../plotting\n' )
    
    if 'torque' in batch_type:
      f_script.write( './plot.py -s $sig_reg -v $var -p 15_16\n' )
      f_script.write( './plot.py -s $sig_reg -v $var -p 17\n' )
      f_script.write( './plot.py -s $sig_reg -v $var -p 15_16_17' )
    if 'condor' in batch_type:
      #f_script.write( './plot.py -s $1 -v $2 -p 15_16\n' )
      #f_script.write( './plot.py -s $1 -v $2 -p 17\n' )
      f_script.write( './plot.py -s $1 -v $2 -p 15_16_17 -n' )
      #f_script.write( './plot.py -s $1 -v $2 -p 15_16_17 -m' ) #low met 
      f_script.write( './plot.py -s $1 -v $2 -p 15_16_17 -n -f' ) #data fakes 
      #f_script.write( './plot.py -s $1 -v $2 -p 15_16_17 -o' ) #soft muons


#_______________________________
if __name__ == "__main__":
  main()
