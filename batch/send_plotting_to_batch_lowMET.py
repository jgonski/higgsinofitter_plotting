#!/usr/bin/env python
'''
Welcome. This script:
  - submits plot.py with various variables and regions to pplxint batch system
'''

import sys, os, time, argparse

#____________________________________________________________________________
def main():

  doTorque = False
  doCondor = False

  # Parse in arguments
  parser = argparse.ArgumentParser(description='Submit plots to batch system.')
  #parser.add_argument('-f', '--fitType',  type=str, nargs='?', help='Fit type. Choose from (bkg, excl, disc, simp)', default='bkg')
  parser.add_argument('--doTorque', action='store_true', help='Submit to torque batch system.')
  parser.add_argument('--doCondor', action='store_true', help='Submit to condor batch system.')

  args = parser.parse_args()
  if args.doTorque:
    print('Submitting to torque batch system...')
    doTorque = True
  if args.doCondor:
    print('Submitting to condor batch system...')
    doCondor = True

  if (not doTorque) and (not doCondor):
    print('Not submitting any jobs as no batch system was specified.')
    print('Please use flag --doTorque or --doCondor to select batch system to submit to.')
    sys.exit()

  mk_batch_scripts(doTorque, doCondor)

#_________________________________________________________________________
def mk_batch_scripts(doTorque=False, doCondor=False):
  '''
  configure f_in, f_out, selector and batch script name to send to batch
  '''

  print 'making batch scripts...'

  submit = True
  
  #my_vars = [
  #  'met_Et',
  #  #'met_track_Et',
  #  #'METRel',
  #  #'METTrackRel',
  #  #'fabs(dPhiMetAndMetTrack)',
  #  #'TST_Et',
  #  'jetPt[0]',
  #  #'jetPt[1]',
  #  #'jetPt[2]',
  #  #'jetPt[3]',
  #  #'jetPt[4]',
  #  #'jetEta[0]',
  #  #'jetEta[1]',
  #  'lep1Pt',
  #  'lep1Eta',
  #  #'lep1Topoetcone20',
  #  #'lep1Ptvarcone20',
  #  #'lep1Ptvarcone30',
  #  'lep2Pt',
  #  'lep2Eta',
  #  #'lep2Topoetcone20',
  #  #'lep2Ptvarcone20',
  #  #'lep2Ptvarcone30',
  #  'nJet30',
  #  #'nJet20',
  #  #'nJet25',
  #  #'nTotalJet',
  #  'DPhiJ1Met',
  #  #'DPhiJ2Met',
  #  #'DPhiJ3Met',
  #  #'DPhiJ4Met',
  #  #'minDPhi4JetsMet',
  #  'minDPhiAllJetsMet',
  #  #'fabs(vectorSumJetsPhi-jetPhi[0])',
  #  #'vectorSumJetsPt',

  #  'METOverHTLep',
  #  'Rll',
  #  'mll',
  #  'Ptll',
  #  'dPhiPllMet',

  #  'MTauTau',
  #  'nBJet20_MV2c10',
  #  #'lep1Pt/mll',
  #  #'lep2Pt/mll',
  #  'mt_lep1',
  #  #'mt_lep2',
  #  'mt_lep1+mt_lep2',
  #  #'mt2leplsp_0',
  #  #'mt2leplsp_25',
  #  #'mt2leplsp_50',
  #  #'mt2leplsp_75',
  #  #'mt2leplsp_90',
  #  'mt2leplsp_100',
  #  #'mt2leplsp_110',
  #  #'mt2leplsp_120',
  #  #'mt2leplsp_150',
  #  #'mt2leplsp_200',
  #  #'mt2leplsp_250',
  #  #'mt2leplsp_300',
  #  #'lep1Pt/lep2Pt', 
  #  #'(lep1Pt+lep2Pt)/mll',
  #  #'nVtx',
  #  #'actual_mu',
  #  #'mu',
  #  'RJR_RISR',
  #  'RJR_MS',
  #  'met_Et*sqrt(METOverHT/met_Et)',

  #  'minRll', 
  #  'ptWlep_minMll'
  #  ]

  my_vars = [
    'nBJet20_MV2c10',
    'met_Et',
    'jetPt[0]',
    'lep1Pt',
    'lep2Pt',
    'nJet30',
    'DPhiJ1Met',
    'minDPhiAllJetsMet',
    'METOverHT',
    'METOverHTLep',
    'met_Et*sqrt(METOverHT/met_Et)',
    'Rll',
    'mll',
    'MTauTau',
    'actual_mu',
    'mt_lep1',
    'mt2leplsp_100',
    'RJR_RISR',
    'RJR_MS',
    'met_Signif',
    ]
  #my_vars = [ #INT note N-1 plots
  ##'RJR_RISR',
  #'mt_lep1',
  ##'lep2Pt',
  #  ]
  #my_vars = [
  #'mll',
  #'mt2leplsp_100',
  ##'met_Signif',
  #]
  
  #sig_regs = [ #data18 validation
  #  'CR-top-ee',
  #  'CR-tau-ee',
  #  'VR-VV-ee',
  #  'VR-SS-ee-me',
  #  'CR-top-mm',
  #  'CR-tau-mm',
  #  'VR-VV-mm',
  #  'VR-SS-mm-em',
  #] 
  #sig_regs = [ #INT Note
  #  #'CR-RJR-top-AF',
  #  #'CR-RJR-tau-AF',
  #  #'CR-RJR-VV-AF',
  #  #'VR-RJR-SS-ee-me',    
  #  #'VR-RJR-SS-mm-em',    
  #  #'VRDF-RJR-iMLLg-DF',
  #  #'VRDF-iMT2f-DF',
  #  'SRSF-RJR-MLL-SF',
  #  'SRSF-RJR-MLL-ee',
  #  'SRSF-RJR-MLL-mm',
  #  #'SRSF-RJR-MT2-SF',
  #  #'SRSF-RJR-MT2-ee',
  #  #'SRSF-RJR-MT2-mm',
  #] 
  sig_regs = [ #regular validation plots
    #'CR-RJR-top-AF',
    #'CR-RJR-tau-AF',
    #'CR-RJR-VV-AF',
    #'VR-RJR-SS-AF',    
    #'VR-RJR-SS-ee-me',    
    #'VR-RJR-SS-mm-em',    
    #'SRSF-RJR-MLL-SF',
    #'SRSF-RJR-MT2-SF',
    #'VR-RJR-SS-AF-loose',    
    #'VR-RJR-SS-ee-me-loose',    
    #'VR-RJR-SS-mm-em-loose',    
    #'VRDF-RJR-iMLLg-DF',
    #'VRDF-RJR-iMT2h-DF',
    #'CR-LowMET-top-AF',
    #'CR-LowMET-tau-AF',
    #'CR-LowMET-VV-AF',
    #'VR-LowMET-SS-AF',    
    #'VR-LowMET-SS-ee-me',    
    #'VR-LowMET-SS-mm-em',    
    #'SRSF-LowMET-MLL-SF',
    #'SRSF-LowMET-MT2-SF',
    ##'VR-RJR-SS-AF-loose',    
    ##'VR-RJR-SS-ee-me-loose',    
    ##'VR-RJR-SS-mm-em-loose',    
    'VRDF-LowMET-iMLLg-DF',
    'VRDF-LowMET-iMT2h-DF',
  ] 
  #sig_regs = [ #regular validation plots
  #  'SRSF-RJR-MLL-SF',
  #  'SRSF-RJR-MT2-SF',
  #] 
  #sig_regs = [  # RJR CRs

  #'CR-RJRa-top-AF',  
  #'CR-RJRaa-top-AF',  
  #'CR-RJRab-top-AF',  
  #'CR-RJRb-top-AF',  
  #'CR-RJRc-top-AF',  
  #'CR-RJRc2-top-AF',  
  #'CR-RJRd-top-AF',  
  #'CR-RJRe-top-AF',  
  #'CR-RJRf-top-AF',  
  #'CR-RJRg-top-AF',  
                   
  #'CR-RJRa-tau-AF',  
  #'CR-RJRaa-tau-AF',  
  #'CR-RJRab-tau-AF',  
  #'CR-RJRb-tau-AF',  
  #'CR-RJRc-tau-AF',  
  #'CR-RJRc2-tau-AF',  
  #'CR-RJRd-tau-AF',  
  #'CR-RJRe-tau-AF',  
  #'CR-RJRf-tau-AF',  
  #'CR-RJRg-tau-AF',  
                    
  #'VR-RJRa-SS-AF',  
  #'VR-RJRaa-SS-AF',  
  #'VR-RJRab-SS-AF',  
  #'VR-RJRb-SS-AF',  
  #'VR-RJRc-SS-AF',  
  #'VR-RJRc2-SS-AF',  
  #'VR-RJRd-SS-AF',  
  #'VR-RJRe-SS-AF',  
  #'VR-RJRf-SS-AF',  
  #'VR-RJRg-SS-AF',  
  
  #'VR-RJR-SS-AF',
  #'VR-RJR-SS-ee',
  #'VR-RJR-SS-mm',
  #'VR-RJR-SS-em',
  #'VR-RJR-SS-me',
  #'VR-RJR-SS-ee-me',
  #'VR-RJR-SS-mm-em',
  #'VR-RJR-SS-SF',
  #'VR-RJR-SS-DF',
                  
  #'VR-RJR-VV-DF',  
  #'VR-RJRa-VV-DF',  
  #'VR-RJRaa-VV-DF',  
  #'VR-RJRab-VV-DF',  
  #'VR-RJRb-VV-DF',  
  #'VR-RJRc-VV-DF',  
  #'VR-RJRc2-VV-DF',  
  #'VR-RJRd-VV-DF',  
  #'VR-RJRe-VV-DF',  
  #'VR-RJRe2-VV-DF',  
  #'VR-RJRf-VV-DF',  
  #'VR-RJRg-VV-DF',  
  #'VR-RJRh-VV-DF',  

  #'VR-RJR-VV-AF',
  #'VR-RJR-VV-ee',
  #'VR-RJR-VV-mm',
  #'VR-RJR-VV-em',
  #'VR-RJR-VV-me',
  #'VR-RJR-VV-SF',
  #'VR-RJR-VV-DF',

  #] 
  #sig_regs = [ #INT Note N-1 Plots
  #'SRSF-RJRaa-MLL-SF',  
  #'SRSF-RJRab-MLL-SF',  
  #'SRSF-RJRb-MLL-SF',  
  #'SRSF-RJRc-MLL-SF',  
  #'SRSF-RJRc2-MLL-SF',  
  #'SRSF-RJRd-MLL-SF',  
  #'SRSF-RJRe-MLL-SF',  
  #'SRSF-RJRf-MLL-SF',  
  #'SRSF-RJRg-MLL-SF',  
  #] 
  #sig_regs = [ 
  #  'VRDF-RJR-iMT2h-DF',
  #]
  
  script_name = 'torque_plot.sh'
  
  if doCondor:
    print('Writing submission arguments to a file for condor')
    with open('sig_reg_var.txt', 'w') as f_sr_var:
      for var in my_vars:
        for sigReg in sig_regs:
          f_sr_var.write( '{0} {1}\n'.format(sigReg, var) )

    if submit:
      cmd = 'condor_submit condor_plot_lowMET.sub accounting_group=group_atlas.harvard'
      os.system(cmd)

  if doTorque:
    for var in my_vars:
      for sigReg in sig_regs:    
        print('sig_reg: {0}, var: {1}'.format(sigReg, var) )
        cmd   = "qsub -l walltime=01:59:00 -l cput=01:59:00 -v var='{0}',sig_reg='{1}' {2}".format(var, sigReg, script_name)
        if submit:
          os.system(cmd)

#_______________________________
if __name__ == "__main__":
  main()


